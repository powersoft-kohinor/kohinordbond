﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace kdbs_prjDBOND
{
    public class clsSecuencia
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
        public string Numfac { get; set; }
        public clsError Error { get; set; }

        public clsSecuencia f_CalcularSecuencia(string s_codemp, string s_sersec, string codsec)
        {
            //ANTES DE LLENAR ENCABEZADO EGRESOS SE CALCULA EL NUMFAC
            clsSecuencia objSecuencia = new clsSecuencia();
            objSecuencia.Error = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_ACTUALIZAR_SECUENCIA]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codemp", s_codemp);
            cmd.Parameters.AddWithValue("@codsec", codsec);
            cmd.Parameters.AddWithValue("@sersec", s_sersec); //del usuario en Login

            cmd.Parameters.Add("@sp_NumCom", SqlDbType.VarChar, 20).Direction = ParameterDirection.Output;
            try
            {
                cmd.ExecuteNonQuery();
                objSecuencia.Numfac = cmd.Parameters["@sp_NumCom"].Value.ToString();
            }
            catch (Exception ex)
            {
                objSecuencia.Error = objSecuencia.Error.f_ErrorControlado(ex);
                //lblError.Text = "*16. " + DateTime.Now + " " + ex.Message;
            }
            conn.Close();

            //Session.Add("gs_NumFac", s_numfac);
            return objSecuencia;
        }

        public clsError f_ActualizarSecuencia(string s_codemp, string s_sersec, string codsec)
        {
            //ACTUALIZA LA SECUENCIA CUANDO EL USUARIO SELECCIONA GUARDAR.
            clsError objError = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_ACTUALIZAR_SEC2]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codemp", s_codemp);
            cmd.Parameters.AddWithValue("@codsec", codsec);
            cmd.Parameters.AddWithValue("@sersec", s_sersec); //del usuario en Login

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
                //lblError.Text = "*12. " + DateTime.Now + " " + ex.Message;
            }
            conn.Close();
            return objError;
        }
    }
}