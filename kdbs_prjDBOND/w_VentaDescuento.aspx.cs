﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace kdbs_prjDBOND
{
    public partial class w_VentaDescuento : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["gs_CodUs1"] != null)
                {
                    lblUsuario.Text = Session["gs_CodUs1"].ToString();
                    lblUsuarioDrop.Text = Session["gs_CodUs1"].ToString();
                }
                else
                {
                    Response.Redirect("w_Login.aspx");
                }

                DateTime d_fechaFin = DateTime.Now;  //fehca del dia
                string s_fechaFin = d_fechaFin.ToString("yyyy-MM-dd");
                //string s_fechaFin = "2020-01-10";
                //Session.Add("gs_FecFin", s_fechaFin);
                Session.Add("gs_FecFin", d_fechaFin.Date);
                txtFecFin.Text = s_fechaFin;

                DateTime d_fechaInicio = DateTime.Now;
                string s_fechaInicio = d_fechaInicio.ToString("yyyy-MM-dd");
                //string s_fechaInicio = "2020-01-01";
                //Session.Add("gs_FecIni", s_fechaInicio);
                Session.Add("gs_FecIni", d_fechaInicio.Date);
                txtFecIni.Text = s_fechaInicio;

                ddlCodalm.DataSource = sqldsAlmacen;
                ddlCodalm.DataBind();
                ddlCodalm.Items.Insert(0, new ListItem("--Seleccionar--", "%"));

                ddlCodcla.DataSource = sqldsArticulosClases;
                ddlCodcla.DataBind();
                ddlCodcla.Items.Insert(0, new ListItem("--Seleccionar--", "%"));

                f_ShowData();

            }
            if (IsPostBack)
            {
                Session["gs_FecIni"] = DateTime.Parse(txtFecIni.Text).Date;
                Session["gs_FecFin"] = DateTime.Parse(txtFecFin.Text).Date;
                f_ShowData();
            }
        }

        protected void grvGridview_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //check if the row is the header row
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //add the thead and tbody section programatically
                e.Row.TableSection = TableRowSection.TableHeader;
            }
        }

        private void f_ShowData()
        {
            String myConnection = ConfigurationManager.ConnectionStrings["ReportesBI"].ToString();
            SqlConnection con = new SqlConnection(myConnection);

            SqlCommand cmd = new SqlCommand("[dbo].[W_S_ALMACENES]", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CODALM", ddlCodalm.SelectedValue.ToString());
            cmd.Parameters.AddWithValue("@CODCLA", ddlCodcla.SelectedValue.ToString());
            cmd.Parameters.AddWithValue("@FECINI", DateTime.Parse(txtFecIni.Text).Date);
            cmd.Parameters.AddWithValue("@FECFIN", DateTime.Parse(txtFecFin.Text).Date);
            DataTable tb = new DataTable();

            try
            {
                con.Open();
                SqlDataAdapter sda = new SqlDataAdapter(cmd);                
                sda.Fill(tb);
                con.Close();
            }
            catch (Exception ex) 
            {

            }

            if (tb != null)
            {
                if (tb.Rows.Count > 0)
                {
                    String chart = "";
                    // You can change your chart height by modify height value
                    chart = "<canvas id=\"line-chart\" width =\"100%\" height=\"50\"></canvas>";
                    chart += "<script>";
                    chart += "new Chart(document.getElementById(\"line-chart\"), { type: 'bar', data: { labels: [";

                    // more details in x-axis
                    int rowindex = 1;
                    foreach (DataRow row in tb.Rows)
                    {
                        //chart += rowindex.ToString() + ",";
                        chart += "\"" + row["nomalm"].ToString().Trim() + "\"" + ",";
                        rowindex++;
                    }

                    chart = chart.Substring(0, chart.Length - 1);

                    chart += "],datasets: [{ data: [";

                    // put data from database to chart
                    String value = "";
                    for (int i = 0; i < tb.Rows.Count; i++)
                        value += tb.Rows[i]["totren"].ToString() + ",";
                    value = value.Substring(0, value.Length - 1);

                    chart += value;

                    chart += "],label: \"Ventas por almacén\", borderColor: \"#3e95cd\",fill: true, backgroundColor: \"rgba(0, 129, 197, 0.5)\"}"; // Chart color
                    chart += "]},options: { title: { display: true,text: 'VENTAS'} } "; // Chart title
                    chart += "});";
                    chart += "</script>";

                    ltChart.Text = chart;
                }
                else
                {
                    ltChart.Text = "<span>Datos no encontrados</span>";
                }                
            }
        }
    }
}