﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace kdbs_prjDBOND
{
    public partial class WebMenuUserControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void lnbtnMenuRedirect_Click(object sender, EventArgs e)
        {
            string s_Ret = System.IO.Path.GetFileName(Request.Url.AbsolutePath); //pag actual
            LinkButton lkbtn = (LinkButton)sender;
            clsMenu objMenu = new clsMenu();

            string s_pag = objMenu.f_Menu_Redirect(lkbtn.CommandArgument, lkbtn.CommandName, Session["gs_MenuPK"], s_Ret);
            if (s_pag.StartsWith("w"))
                Response.Redirect(s_pag + ".aspx");
            else
                Response.Write(s_pag);
        }
    }
}