﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="w_ExistenciaAlmacenes.aspx.cs" Inherits="kdbs_prjDBOND.w_ExistenciaAlmacenes" %>

<%@ Register Src="~/WebMenuUserControl.ascx" TagPrefix="uc1" TagName="WebMenuUserControl" %>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Kohinor Móvil - DBOND</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">
<form id="form1" runat="server">
  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
      <uc1:WebMenuUserControl runat="server" id="WebMenuUserControl1" />
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button onclick="return false;" id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Search -->
          <div class="d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
              <div class="row">
                  <div class="col">
                      <h6 class="m-0 font-weight-bold text-primary">Existencia Almacenes</h6>
                  </div>
              </div>
            <%--<div class="input-group">
              <input type="text" class="form-control bg-light border-0 small" placeholder="Buscar..." aria-label="Search" aria-describedby="basic-addon2">
              <div class="input-group-append">
                <button class="btn btn-primary" type="button">
                  <i class="fas fa-search fa-sm"></i>
                </button>
              </div>
            </div>--%>
          </div>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <%--<!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>--%>

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">
                    <asp:Label ID="lblUsuario" runat="server"></asp:Label>
                </span>
                <img class="img-profile rounded-circle" src="Imagenes/catUser.png">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">                     
                  <asp:HyperLink ID="hpkUsuario" class="dropdown-item" runat="server">                      
                      <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                      <asp:Label ID="lblUsuarioDrop" runat="server"></asp:Label>
                  </asp:HyperLink>
                
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid" style="display:none">
          <!-- BAJO CONSTRUCCION Error Text -->
          <div class="text-center">
            <div class="error mx-auto" data-text="000">! ! !</div>
            <p class="lead text-gray-800 mb-5">Página En Construcción</p>
            <p class="text-gray-500 mb-0">Parece que la página que busca no esta lista...</p>
          </div>
        </div>


        <div class="container-fluid" >
          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-2">
              <asp:LinkButton ID="lkbtnVolver" runat="server" class="btn btn-primary shadow-sm btn-sm" PostBackUrl="~/w_ListadoProductos.aspx">
                <i class="fas fa-arrow-left"></i> Volver</asp:LinkButton>
              <asp:Label ID="lblError" runat="server" Text="" ForeColor="Maroon"></asp:Label>
            <%--<h1 class="h3 mb-0 text-gray-800"><strong>Despacho</strong></h1>--%>
            <%--<a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>--%>
          </div>          

          <div class="row">

            <!-- Area Chart -->
            <div class="col">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
            <%--    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Abrir Ingreso</h6>
                    <div class="right">
                        <asp:Button ID="btnCalcular" runat="server" CssClass="btn btn-primary btn-sm" Text="Buscar" />
                    </div>
                </div>--%>
                <!-- Card Body -->
                
                  <div class="card-body">
                      <div class="chart-area" style="width: 100%; height: 100%; overflow: scroll">
                          <%--K<canvas id="">--%>

                          <asp:GridView ID="grvExistenciaAlm" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="True" class="table table-sm table-dark table-hover" OnSelectedIndexChanged="grvCrmTarea_SelectedIndexChanged" DataSourceID="sqldsDashboardAlmacenes">

                              <Columns>
                                  <%--<asp:CommandField ButtonType="Image" SelectImageUrl="~/Imagenes/Select-Hand.png" ShowSelectButton="True">
                                  <ControlStyle Height="30px" />
                                  <HeaderStyle Height="30px" />
                                  <ItemStyle Height="30px" />
                              </asp:CommandField>--%>
                                  <%-- <asp:TemplateField HeaderText="N°" ItemStyle-Width="1%">
                                  <ItemTemplate>
                                      <%# Container.DataItemIndex + 1 %>
                                  </ItemTemplate>
                                  <ItemStyle Width="1%"></ItemStyle>
                              </asp:TemplateField>--%>
                                  <%--<asp:BoundField DataField="codalm" HeaderText="Código" SortExpression="codalm" />--%>
                                  <asp:BoundField DataField="nomalm" HeaderText="Almacen" SortExpression="nomalm" />
                                  <asp:BoundField DataField="exiact" HeaderText="Stock" SortExpression="exiact" DataFormatString="{0:0.00}"/>
                                  <asp:BoundField DataField="ultcom" HeaderText="Ult Compra" SortExpression="ultcom" DataFormatString="{0:d}" />
                                  <asp:BoundField DataField="ultven" HeaderText="Ult Venta" SortExpression="ultven" DataFormatString="{0:d}" />
                                  <asp:BoundField DataField="coddes" HeaderText="Desc" SortExpression="coddes" />
                                  <%--<asp:BoundField DataField="alerta" HeaderText="Alerta" SortExpression="alerta"/>--%>
                                  <%--<asp:BoundField DataField="nomart" HeaderText="Articulo" SortExpression="nomart" />--%>
                              </Columns>

                              <EditRowStyle BackColor="#999999" />
                              <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />

                              <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                              <RowStyle BackColor="#BBD9EF" ForeColor="#333333" />
                              <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                              <SortedAscendingCellStyle BackColor="#E9E7E2" />
                              <SortedAscendingHeaderStyle BackColor="#506C8C" />
                              <SortedDescendingCellStyle BackColor="#FFFDF8" />
                              <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                              <AlternatingRowStyle BackColor="#F8FAFA" ForeColor="#284775" />
                          </asp:GridView>

                          <%--</canvas>--%>
                      </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; KOHINOR 2021 Todos Los Derechos Reservados</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">¿Desea salir?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Seleccionar "Logout" abajo para salir de la sesión actual.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
          <a class="btn btn-primary" href="w_Login.aspx">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/chart-area-demo.js"></script>
  <script src="js/demo/chart-pie-demo.js"></script>

  <!--i SQL DATASOURCES-->
    <asp:SqlDataSource ID="sqldsDashboardAlmacenes" runat="server" ConnectionString="<%$ ConnectionStrings:kdbs_EsperanzaConnectionString %>"
        SelectCommand="PVT_S_DASHBOARD_EXISTENCIA_alm" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="CODEMP" SessionField="gs_CodEmp" Type="String" />
            <asp:SessionParameter Name="codart" SessionField="gs_Codart" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
  <!--f SQL DATASOURCES-->

</form>
</body>

</html>

