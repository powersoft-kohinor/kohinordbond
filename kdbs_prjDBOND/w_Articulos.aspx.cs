﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace kdbs_prjDBOND
{
    public partial class w_Articulos : System.Web.UI.Page
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
        protected clsArticulo objArticulo = new clsArticulo();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["gs_CodUs1"] != null)
                {
                    lblUsuario.Text = Session["gs_CodUs1"].ToString();
                    lblUsuarioDrop.Text = Session["gs_CodUs1"].ToString();
                }
                else
                {
                    Response.Redirect("w_Login.aspx");
                }
                //lblArtSeleccionado.Text = "";
                f_BindGridVacio();
            }
        }

        protected void grvAlmacenes_RowDataBound(object sender, GridViewRowEventArgs e) //only fires when the GridView's data changes during the postback
        {
            //check if the row is the header row
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //add the thead and tbody section programatically
                e.Row.TableSection = TableRowSection.TableHeader;
            }
        }
        
        protected void grvAlmacenes_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow fila = grvAlmacenes.SelectedRow;
            Label lblCodart = (Label)fila.FindControl("lblgrvCodmon");
            string s_codart = lblCodart.Text.Trim();
            lblArtSeleccionado.Text = s_codart;
            divGridAlmacen.Attributes.Add("style", "display: normal;");
            f_BindGrid_Almacen(s_codart);
            f_ExistenciaTotal();
        }        

        protected void f_ExistenciaTotal()
        {
            DataTable dt = (DataTable)Session["gdt_Articulo_Almacen"];
            int cantot = 0;
            if (dt!=null)
            {
                if (dt.Rows.Count>0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        cantot += row["exiact"] != DBNull.Value? Convert.ToInt32(row["exiact"]) : 0;
                    }
                }
            }            
            txtExistencia.Text = cantot.ToString();
        }

        private void f_BindGrid_Almacen(string s_codart)
        {
            objArticulo = objArticulo.f_Articulo_Almacen_Buscar(Session["gs_CodEmp"].ToString(), s_codart);
            if (String.IsNullOrEmpty(objArticulo.ErrorReal.Mensaje))
            {
                DataTable dt = objArticulo.dtArticulo;
                Session.Add("gdt_Articulo_Almacen", dt);
                if (dt.Rows.Count > 0)
                {
                    grvArticulo_Almacen.DataSource = dt;
                    grvArticulo_Almacen.DataBind();
                }
                else
                {
                    // add new row when the dataset is having zero record
                    dt.Rows.Add(dt.NewRow());
                    grvArticulo_Almacen.DataSource = dt;
                    grvArticulo_Almacen.DataBind();
                    grvArticulo_Almacen.Rows[0].Visible = false;
                }
            }
            else
            {
                lblError.Text = objArticulo.ErrorReal.f_ErrorNuevo("*02. ", objArticulo.ErrorReal);
            }
        }

        private void f_BindGrid(string s_codart = "%", string s_codalt = "%", string s_desart = "%", string s_nomart = "%", int recursive = 0)
        {
            objArticulo = objArticulo.f_Articulo_Buscar(Session["gs_CodEmp"].ToString(), s_codart, s_codalt, s_desart, s_nomart);
            if (String.IsNullOrEmpty(objArticulo.ErrorReal.Mensaje))
            {
                DataTable dt = objArticulo.dtArticulo;
                Session.Add("gdt_Articulo", dt);
                if (dt.Rows.Count > 0)
                {
                    grvAlmacenes.DataSource = dt;
                    grvAlmacenes.DataBind();

                    if (recursive == 1)
                    {
                        txtCodart.Text = txtDesart.Text = "";
                        txtCodalt.Text = s_codalt.Replace("%", string.Empty);
                    }
                    else if (recursive == 2)
                    {
                        txtCodart.Text = txtCodalt.Text = "";
                        txtDesart.Text = s_desart.Replace("%", string.Empty);
                    }
                }
                else
                {
                    switch (recursive)
                    {
                        case 0:
                            f_BindGrid(s_codalt: s_codart, s_nomart: s_nomart, recursive: 1);
                            break;
                        case 1:
                            f_BindGrid(s_desart: s_codalt, s_nomart: s_nomart, recursive: 2);
                            break;
                        case 2:
                            // add new row when the dataset is having zero record
                            dt.Rows.Add(dt.NewRow());
                            grvAlmacenes.DataSource = dt;
                            grvAlmacenes.DataBind();
                            grvAlmacenes.Rows[0].Visible = false;
                            break;
                        default:
                            // add new row when the dataset is having zero record
                            dt.Rows.Add(dt.NewRow());
                            grvAlmacenes.DataSource = dt;
                            grvAlmacenes.DataBind();
                            grvAlmacenes.Rows[0].Visible = false;
                            break;
                    }                    
                }
            }
            else
            {
                lblError.Text = objArticulo.ErrorReal.f_ErrorNuevo("*01. ", objArticulo.ErrorReal);
            }            
        }

        private void f_BindGridVacio()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("nomart", typeof(string)));
            dt.Columns.Add(new DataColumn("codart", typeof(string)));
            dt.Columns.Add(new DataColumn("codalt", typeof(string)));
            dt.Columns.Add(new DataColumn("desart", typeof(string)));
            dt.Columns.Add(new DataColumn("prec01", typeof(string)));
            dt.Columns.Add(new DataColumn("codmod", typeof(string)));
            dt.Columns.Add(new DataColumn("desc01", typeof(string)));

            // add new row when the dataset is having zero record
            //dt.Rows.Add(dt.NewRow());
            grvAlmacenes.DataSource = dt;
            grvAlmacenes.DataBind();
            //grvAlmacenes.Rows[0].Visible = false;
        }

        protected void f_GridBuscar(object sender, EventArgs e)
        {
            divGridAlmacen.Attributes.Add("style", "display: none;");
            f_BindGrid("%" + txtCodart.Text + "%", "%" + txtCodalt.Text + "%", "%" + txtDesart.Text + "%", "%" + txtNomart.Text + "%");
        }

        protected void f_GridBuscarLimpio(object sender, EventArgs e)
        {
            txtCodart.Text = "";
            txtCodalt.Text = "";
            txtDesart.Text = "";
            txtNomart.Text = "";
            txtExistencia.Text = "";
            divGridAlmacen.Attributes.Add("style", "display: none;");
            f_BindGridVacio();
        }
        
        protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvAlmacenes.PageIndex = e.NewPageIndex;
            f_BindGrid("%"+txtCodart.Text+"%", "%" + txtCodalt.Text + "%", "%" + txtDesart.Text + "%", "%" + txtNomart.Text + "%");
        }
    }
}