﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace kdbs_prjDBOND
{
    public partial class w_ListadoProductos : System.Web.UI.Page
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
        protected clsArticulo objArticulo = new clsArticulo();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session.Add("gs_Codart", "a%");
                Session.Add("gs_Desart", "%");
                Session.Add("gs_Nomart", "%");
                f_RepeaterProductos();
            }
        }

        protected void f_RepeaterProductos()
        {
            DataSourceSelectArguments args = new DataSourceSelectArguments();
            DataView view = (DataView)sqldsDASHBOARD_EXISTENCIA.Select(args);
            DataTable dt = view.ToTable();
            //DataTable dt = f_Articulo_Buscar(Session["gs_CodEmp"].ToString(), Session["gs_Codart"].ToString(), Session["gs_Desart"].ToString(), Session["gs_Nomart"].ToString());

            rptProductos.DataSource = dt;
            rptProductos.DataBind();
        }

        public DataTable f_Articulo_Buscar(string codemp, string codart, string desart, string nomart)
        {
            DataTable dt = new DataTable();
            clsError objError = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[PVT_S_DASHBOARD_EXISTENCIA]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the select task to get all data
            cmd.Parameters.AddWithValue("@CODEMP", codemp);
            cmd.Parameters.AddWithValue("@codart", codart);
            cmd.Parameters.AddWithValue("@nomart", nomart);
            cmd.Parameters.AddWithValue("@desart", desart);

            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }

            conn.Close();
            return dt;
        }

        protected void lkbtnDetalle_Click(object sender, EventArgs e)
        {
            LinkButton lkbtn = (LinkButton)sender;
            Session["gs_Codart"] = lkbtn.CommandArgument;
            Response.Redirect("w_DatosAdicionales.aspx");
        }

        protected void lkbtnAlmacenes_Click(object sender, EventArgs e)
        {
            LinkButton lkbtn = (LinkButton)sender;
            Session["gs_Codart"] = lkbtn.CommandArgument;
            Response.Redirect("w_ExistenciaAlmacenes.aspx");
        }
        
        protected void lkbtnBuscar_Click(object sender, EventArgs e)
        {
            string s_filtro = ddlFiltro.SelectedValue.ToString();
            switch (s_filtro)
            {
                case "1":
                    Session["gs_Codart"] = txtBuscar.Text.Trim();
                    Session["gs_Desart"] = "%";
                    Session["gs_Nomart"] = "%";
                    break;
                case "2":
                    Session["gs_Codart"] = "%"; 
                    Session["gs_Desart"] = txtBuscar.Text.Trim();
                    Session["gs_Nomart"] = "%";
                    break;
                case "3":
                    Session["gs_Codart"] = "%";
                    Session["gs_Desart"] = "%";
                    Session["gs_Nomart"] = txtBuscar.Text.Trim();
                    break;

                default:
                    break;
            }
            f_RepeaterProductos();
        }
    }
}