﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace kdbs_prjDBOND
{ 
    public class clsCharts
    {
        
        public static int Get_All_Categories()
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ReportesBI"].ConnectionString);
            int Count_Categories;
            SqlCommand cmd = new SqlCommand("[dbo].[W_S_ALMACENES]", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            conn.Open();
            Count_Categories = int.Parse(cmd.ExecuteScalar().ToString());
            conn.Close();
            return Count_Categories;

        }

        public static string[] Get_Categories()
        {
            string[] arr_category = new string[5];
            int[] arr_values = new int[5];
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ReportesBI"].ConnectionString);
            SqlCommand cmd = new SqlCommand("[dbo].[BORRAR]", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            int x = 0;
            while (reader.Read())
            {
                arr_category[x] = reader[0].ToString();
                arr_values[x++] = int.Parse(reader[1].ToString());
            }
            conn.Close();

            JavaScriptSerializer ser = new JavaScriptSerializer();
            string Category_Names = ser.Serialize(arr_category);
            string Values = ser.Serialize(arr_values);
            string[] arr = { Category_Names, Values };
            return arr;
        }
    }    
}