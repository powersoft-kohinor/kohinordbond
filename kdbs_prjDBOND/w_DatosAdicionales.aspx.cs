﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace kdbs_prjDBOND
{
    public partial class w_DatosAdicionales : System.Web.UI.Page
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
        protected clsIngreso objIngreso = new clsIngreso();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session.Add("gs_Exiact", "0.0");
                DataSourceSelectArguments args = new DataSourceSelectArguments();
                DataView view = (DataView)sqldsDASHBOARD_DETALLE.Select(args);
                DataTable dt = view.ToTable();

                lblCompras.Text = Convert.ToString(dt.Rows[0].Field<Decimal>("compras"));
                lblVentas.Text = Convert.ToString(dt.Rows[0].Field<Decimal>("ventas"));
                lblSaldo.Text = Convert.ToString(dt.Rows[0].Field<Decimal>("saldo"));
                lblPrec01.Text = Convert.ToString(dt.Rows[0].Field<Decimal>("saldo"));
                lblNomart.Text = dt.Rows[0].Field<string>("nomart");
                lblCodart.Text = dt.Rows[0].Field<string>("codart");
                lblDesart.Text = dt.Rows[0].Field<string>("desart");
                lblNomcla.Text = dt.Rows[0].Field<string>("nomcla");
                lblNomfam.Text = dt.Rows[0].Field<string>("nomfam");
                lblModbas.Text = dt.Rows[0].Field<string>("modbas");
                lblCodalt.Text = dt.Rows[0].Field<string>("codalt");
                //lblGalmat.Text = dt.Rows[0].Field<string>("galmat");
                lblNumheb.Text = dt.Rows[0].Field<string>("numheb");
                lblDesmat.Text = dt.Rows[0].Field<string>("desmat");
                lblDescri.Text = dt.Rows[0].Field<string>("descri");
            }
        }

    }
}