﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace kdbs_prjDBOND
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
        protected clsIngreso objIngreso = new clsIngreso();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session.Add("gs_Codart", "0082216AL");
                Session.Add("gs_CodEmp", "01");

            }
        }

        protected void lnbtnMenuRedirect_Click(object sender, EventArgs e)
        {
            LinkButton lkbtn = (LinkButton)sender;
            clsMenu objMenu = new clsMenu();
            string s_pag = objMenu.f_Menu_Redirect(lkbtn.CommandArgument, lkbtn.CommandName, Session["gs_Tittar"]);
            if (s_pag.StartsWith("w"))
                Response.Redirect(s_pag + ".aspx");
            else
                Response.Write(s_pag);
        }

     
    }
}