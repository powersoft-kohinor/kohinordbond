﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm2.aspx.cs" Inherits="kdbs_prjDBOND.WebForm2" %>



<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Kohinor Móvil - DBOND</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">
<form id="form1" runat="server">
  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion toggled" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="">
        <div class="sidebar-brand-icon">
            <img src="Imagenes/Logo-Kohinor2.png" width="50px"/>
          <%--<i class="fas fa-laugh-wink"></i>--%>
        </div>
        <div class="sidebar-brand-text mx-2">KOHINOR <sup>V3.0</sup></div>
      </a>        

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

        <!-- Nav Item - Abrir -->
      <li class="nav-item active">
        <asp:LinkButton ID="lkbtnAbrir" class="nav-link" runat="server" OnClick="lnbtnMenuRedirect_Click" CommandArgument="8-1" CommandName="w_Abrir">
           <i class="fas fa-folder-open"></i>
           <span>Abrir</span></asp:LinkButton>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

        <!-- Nav Item - Toma Manual -->
      <li class="nav-item active">
        <asp:LinkButton ID="lnbtnTomaManual" class="nav-link" runat="server" OnClick="lnbtnMenuRedirect_Click" CommandArgument="3-1" CommandName="w_TomaManual">
           <i class="fas fa-fw fal fa-user-edit"></i>
          <span>Toma Manual</span>
        </asp:LinkButton>

          <!-- Divider -->
      <hr class="sidebar-divider my-0">

          <!-- Nav Item - Artículos -->
      <li class="nav-item active">
        <asp:LinkButton ID="lnbtnArticulos" class="nav-link" runat="server" OnClick="lnbtnMenuRedirect_Click" CommandArgument="7-1" CommandName="w_Articulos">
           <i class="fas fa-box-open"></i>
           <span>Artículos</span></asp:LinkButton>
      </li>

        <!-- Divider -->
      <%--<hr class="sidebar-divider my-0">--%>       

        <!-- Nav Item - Informe Venta Descuento -->
      <li class="nav-item active bg-gradient-dark">
        <asp:LinkButton ID="lkbtnInformeVentaDesc" class="nav-link" runat="server" OnClick="lnbtnMenuRedirect_Click" CommandArgument="9-1" CommandName="w_VentaDescuento">
           <i class="fas fa-file-invoice-dollar"></i>
           <span>Ventas Desc</span></asp:LinkButton>
      </li>

       
      <hr class="sidebar-divider my-0">--%>

        <!-- Nav Item - Salida -->
      <li class="nav-item active">
        <asp:LinkButton ID="lnbtnSalida" class="nav-link" runat="server" OnClick="lnbtnMenuRedirect_Click" CommandArgument="6-1" CommandName="w_Login">
           <i class="fas fa-fw far fa-sign-out-alt"></i>
           <span>Salida</span></asp:LinkButton>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">      

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button onclick="return false;" class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button onclick="return false;" id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Search -->
          <div class="d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
            <%--<div class="input-group">
              <input type="text" class="form-control bg-light border-0 small" placeholder="Buscar..." aria-label="Search" aria-describedby="basic-addon2">
              <div class="input-group-append">
                <button class="btn btn-primary" type="button">
                  <i class="fas fa-search fa-sm"></i>
                </button>
              </div>
            </div>--%>
          </div>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">


            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">
                    <asp:Label ID="lblUsuario" runat="server"></asp:Label>
                </span>
                <img class="img-profile rounded-circle" src="Imagenes/catUser.png">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">                     
                  <asp:HyperLink ID="hpkUsuario" class="dropdown-item" runat="server">                      
                      <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                      <asp:Label ID="lblUsuarioDrop" runat="server"></asp:Label>
                  </asp:HyperLink>
                
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid" style="display:none">

          <!-- BAJO CONSTRUCCION Error Text -->
          <div class="text-center">
            <div class="error mx-auto" data-text="000">! ! !</div>
            <p class="lead text-gray-800 mb-5">Página En Construcción</p>
            <p class="text-gray-500 mb-0">Parece que la página que busca no esta lista...</p>
          </div>
        </div>


        <div class="container-fluid" >

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
              <asp:Label ID="lblError" runat="server" Text="" ForeColor="Maroon"></asp:Label>
            <%--<h1 class="h3 mb-0 text-gray-800"><strong>Despacho</strong></h1>--%>
            <%--<a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>--%>
          </div>

          

          <div class="row">

            <!-- Area Chart -->
            <div class="col">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
            <%--    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Abrir Ingreso</h6>
                    <div class="right">
                        <asp:Button ID="btnCalcular" runat="server" CssClass="btn btn-primary btn-sm" Text="Buscar" />
                    </div>
                </div>--%>
                <!-- Card Body -->
                
                  <div class="card-body">
                      
                  <div class="row">

            <!-- Area Chart -->
            <div class="col">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Toma Física</h6>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Acciones:</div>


                        <%--<asp:Button ID="btnDespachar" class="dropdown-item" runat="server" Text="&#9997; Despachar" OnClick="btnDespachar_Click" />
                        <asp:Button ID="btnPendiente" class="dropdown-item" runat="server" Text="💾 Grabar Pendiente" OnClick="btnPendiente_Click"/>--%>
                        <%--<asp:Button ID="btnConsolidar" class="dropdown-item" runat="server" Text="&#9195; Consolidar" OnClick="btnConsolidar_Click" />--%>     


                      <div class="dropdown-divider"></div>
                        <asp:Button ID="btnGuardar" class="dropdown-item" runat="server" Text="&#9899; Guardar" />
                    </div>
                  </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">        
                    <div style="width: 100%; overflow: scroll">
                       <%-- <div class="row">--%>
                        <div class="row">
                            <div class="col-lg">
                                <b><center>-- Artículo --</center></b>
                                <asp:TextBox ID="txtCodart" class="form-control form-control-user" runat="server" placeholder="Código"></asp:TextBox>
                                <asp:TextBox ID="txtNomart" class="form-control form-control-user" runat="server" placeholder="Nombre"></asp:TextBox>
                                <div class="row">
                                    <div class="col-6">
                                        <asp:TextBox ID="txtCantid" class="form-control form-control-user" runat="server" TextMode="Number" placeholder="Cantidad"></asp:TextBox>                                        
                                    </div>
                                    <div class="col-6">
                                        <asp:TextBox ID="txtSaldo" class="form-control form-control-user" runat="server" TextMode="Number" placeholder="Saldo" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>                                
                            </div>                            
                            <div class="col-lg">
                                <b><center>-- Tarea --</center></b>
                                <asp:TextBox ID="txtNumegr" class="form-control form-control-user" runat="server" placeholder="N° Egreso" ReadOnly="true"></asp:TextBox>
                                <asp:TextBox ID="txtFecegr" class="form-control form-control-user" runat="server" ReadOnly="true"></asp:TextBox>
                                <%--<asp:TextBox ID="txtTittar" class="form-control form-control-user" runat="server" placeholder="Tittar" ReadOnly="true"></asp:TextBox>--%>
                                <asp:TextBox ID="txtNumtar" class="form-control form-control-user" runat="server" placeholder="Numtar" ReadOnly="true"></asp:TextBox>
                            </div>
                            <%--<div class="col-lg">
                                <b><center>-- Proveedor --</center></b>
                                <asp:TextBox ID="txtCodpro" class="form-control form-control-user" runat="server" placeholder="Codpro" ReadOnly="true"></asp:TextBox>
                                <asp:TextBox ID="txtNompro" class="form-control form-control-user" runat="server" placeholder="Nompro" ReadOnly="true"></asp:TextBox>
                                <asp:Button ID="btnProveedores" class="btn btn-primary" runat="server" Text="Buscar Proveedor" OnClick="btnProveedores_Click" />
                            </div>--%>
                        </div>
                        <%--</div>--%>
                        <%--<div class="form-row">
                            <div class="form-group col-md-6">
                                
                            </div>
                            <div class="form-group col-md-2">
                                
                            </div>
                        </div>
                        --%>
                    </div>
                  </div>
              </div>
            </div>

          </div>
                </div>


              </div>
            </div>

          </div>


        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; KOHINOR 2020 Todos Los Derechos Reservados</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">¿Desea salir?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Seleccionar "Logout" abajo para salir de la sesión actual.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
          <a class="btn btn-primary" href="w_Login.aspx">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/chart-area-demo.js"></script>
  <script src="js/demo/chart-pie-demo.js"></script>

  <!--i SQL DATASOURCES-->
    <asp:SqlDataSource ID="sqldsDashboardAlmacenes" runat="server" ConnectionString="<%$ ConnectionStrings:kdbs_EsperanzaConnectionString %>"
        SelectCommand="PVT_S_DASHBOARD_EXISTENCIA_alm" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="CODEMP" SessionField="gs_CodEmp" Type="String" />
            <asp:SessionParameter Name="codart" SessionField="gs_Codart" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>

  

  <!--f SQL DATASOURCES-->

</form>
</body>

</html>






