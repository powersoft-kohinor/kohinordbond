﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="w_ListadoProductos.aspx.cs" Inherits="kdbs_prjDBOND.w_ListadoProductos" %>

<%@ Register Src="~/WebMenuUserControl.ascx" TagPrefix="uc1" TagName="WebMenuUserControl" %>


<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
    <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />
  <title>Kohinor Móvil - DBOND</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">
    <link href="css/StyleSheet1.css" rel="stylesheet" />
    <%--<style>
        img {
            max-width: 100%;
            /*max-height: 50%;*/
            height: auto;
            width: auto\9;
        }
    </style>--%>

</head>

<body id="page-top">
<form id="form1" runat="server">
    
  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <uc1:WebMenuUserControl runat="server" id="WebMenuUserControl" />
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
          <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

              <!-- Sidebar Toggle (Topbar) -->
              <button onclick="return false;" id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                  <i class="fa fa-bars"></i>
              </button>

              <!-- Topbar Search -->
              <div class="d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                  <div class="row">
                      <div class="col">
                          <h6 class="m-0 font-weight-bold text-primary">Listado</h6>
                      </div>
                  </div>
                  <%--<div class="input-group">
              <input type="text" class="form-control bg-light border-0 small" placeholder="Buscar..." aria-label="Search" aria-describedby="basic-addon2">
              <div class="input-group-append">
                <button class="btn btn-primary" type="button">
                  <i class="fas fa-search fa-sm"></i>
                </button>
              </div>
            </div>--%>
              </div>

              <!-- Topbar Navbar -->
              <ul class="navbar-nav ml-auto">

                  <%--<!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>--%>

                  <div class="topbar-divider d-none d-sm-block"></div>

                  <!-- Nav Item - User Information -->
                  <li class="nav-item dropdown no-arrow">
                      <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <span class="mr-2 d-none d-lg-inline text-gray-600 small">
                              <asp:Label ID="lblUsuario" runat="server"></asp:Label>
                          </span>
                          <img class="img-profile rounded-circle" src="Imagenes/catUser.png">
                      </a>
                      <!-- Dropdown - User Information -->
                      <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                          <asp:HyperLink ID="hpkUsuario" class="dropdown-item" runat="server">
                              <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                              <asp:Label ID="lblUsuarioDrop" runat="server"></asp:Label>
                          </asp:HyperLink>

                          <div class="dropdown-divider"></div>
                          <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                              <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                              Logout
                          </a>
                      </div>
                  </li>

              </ul>

          </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid" style="display:none">

          <!-- BAJO CONSTRUCCION Error Text -->
          <div class="text-center">
            <div class="error mx-auto" data-text="000">! ! !</div>
            <p class="lead text-gray-800 mb-5">Página En Construcción</p>
            <p class="text-gray-500 mb-0">Parece que la página que busca no esta lista...</p>
          </div>
        </div>


        <div class="container-fluid" >

            <!-- Filtros -->
          <div class="card shadow mb-0 bg-gradient-primary">
              <div class="card-body">
                  <div class="row">
                      <div class="col">
                          <div class="input-group mb-0">
                              <div class="input-group-prepend">
                                      <asp:DropDownList ID="ddlFiltro" runat="server" CssClass="form-control input-group-text">
                                          <asp:ListItem Value="1">Código</asp:ListItem>
                                          <asp:ListItem Value="2">SKU</asp:ListItem>
                                          <asp:ListItem Value="3">Nombre</asp:ListItem>
                                      </asp:DropDownList>
                              </div>
                              <asp:TextBox ID="txtBuscar" runat="server" placeholder=" Buscar..." class="form-control"></asp:TextBox>
                              <div class="input-group-append">
                                   <asp:LinkButton ID="lkbtnBuscar" runat="server" class="btn btn-facebook" OnClick="lkbtnBuscar_Click"><i class="fas fa-search fa-sm"></i></asp:LinkButton>
                              </div>
                          </div>
                      </div>                    
                  </div>
              </div>
          </div>
            <div class="card shadow mb-4">
                <div class="card-body">
                    <asp:Repeater ID="rptProductos" runat="server">
                        <ItemTemplate>
                            <div class="row">
                                <div class="col-3">
                                    <%--<asp:Image ID="imgProducto" runat="server" ImageUrl="~/Imagenes/SacoABMariela748.png"  CssClass="img-thumbnail img"/>--%>
                                    <img src="Imagenes/SacoABMariela748.png" class="img-thumbnail img-fluid" style="width: 200px"/>
                                </div>
                                <div class="col-5">
                                    <div class="row">
                                        <div class="col">
                                            <label style="color: black"><strong>
                                                <asp:Label ID="lblNomart" runat="server" Text='<%# Eval("nomart") %>'></asp:Label></strong></label></div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <label style="color: darkblue">Código
                                                <asp:Label ID="lblCodart" runat="server" Text='<%# Eval("codart") %>'></asp:Label>
                                            </label>
                                        </div>
                                        <div class="col">
                                            <label style="color: darkblue">SKU
                                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("desart") %>' ForeColor="DarkBlue" ></asp:Label>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">Existencia
                                            <asp:Label ID="lblExiact" runat="server" Text='<%# Eval("exiact","{0:0}") %>'></asp:Label></div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <label style="color: maroon"><strong>USD $<asp:Label ID="lblPrepvp" runat="server" Text='<%# Eval("prepvp","{0:0.00}") %>' DataFormatString="{0:0.00}"></asp:Label></strong> </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="row">
                                        <div class="col">USD $<asp:Label ID="lblPrec01" runat="server" Text='<%# Eval("prec01","{0:0.00}") %>' DataFormatString="{0:0.00}"></asp:Label></div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <asp:LinkButton ID="lkbtnDetalle" runat="server" class="btn btn-success shadow-sm mb-2 btn-sm" CommandArgument='<%# Eval("codart") %>' OnClick="lkbtnDetalle_Click">
                                        <i class="fas fa-file-alt"></i> Detalle</asp:LinkButton>
                                            <asp:LinkButton ID="lkbtnAlmacenes" runat="server" class="btn btn-success shadow-sm mb-2 btn-sm" CommandArgument='<%# Eval("codart") %>' OnClick="lkbtnAlmacenes_Click">
                                        <i class="fas fa-boxes"></i> Almacenes</asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr />
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; KOHINOR 2021 Todos Los Derechos Reservados</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">¿Desea salir?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Seleccionar "Logout" abajo para salir de la sesión actual.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
          <a class="btn btn-primary" href="w_Login.aspx">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/chart-area-demo.js"></script>
  <script src="js/demo/chart-pie-demo.js"></script>

  <!--i SQL DATASOURCES-->
    <asp:SqlDataSource ID="sqldsDASHBOARD_EXISTENCIA" runat="server" ConnectionString="<%$ ConnectionStrings:kdbs_EsperanzaConnectionString %>" SelectCommand="PVT_S_DASHBOARD_EXISTENCIA" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="CODEMP" SessionField="gs_CodEmp" Type="String" />
            <asp:SessionParameter DefaultValue="a%" Name="codart" SessionField="gs_Codart" Type="String" />
            <asp:SessionParameter DefaultValue="%" Name="desart" SessionField="gs_Desart" Type="String" />
            <asp:SessionParameter DefaultValue="%" Name="nomart" SessionField="gs_Nomart" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
  <!--f SQL DATASOURCES-->

</form>
</body>

</html>
