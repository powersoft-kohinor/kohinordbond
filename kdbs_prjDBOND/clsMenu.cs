﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace kdbs_prjDBOND
{
    public class clsMenu
    {
        public string f_Menu_Redirect(string level, string name, object numtra, string pag_actual)
        {
            //EJM LEVEL> 1-2-1  ...los 2 primeros son el nivel, el tercero ya no se usa

            string s_pagina = name;

            switch (level.Substring(0, 3))
            {
                case "1-2":
                    if (pag_actual.Equals("w_DespachoListado.aspx"))
                    {
                        if (numtra == null)
                            s_pagina = "<script>alert('Debe seleccionar un pedido.');</script>";
                    }
                    else
                    {
                        s_pagina = "<script>alert('Acceso Denegado. Ir a Listado Despacho primero.');</script>";
                    }

                    break;

                case "2-2":
                    if (pag_actual.Equals("w_TomaFisicaListado.aspx"))
                    {
                        if (numtra == null)
                            s_pagina = "<script>alert('Debe seleccionar una tarea.');</script>";
                    }
                    else
                    {
                        s_pagina = "<script>alert('Acceso Denegado. Ir a Listado Toma Física primero.');</script>";
                    }
                    break;

                case "4-2":
                    if (pag_actual.Equals("w_ImportListado.aspx"))
                    {
                        if (numtra == null)
                            s_pagina = "<script>alert('Debe seleccionar una tarea.');</script>";
                    }
                    else
                    {
                        s_pagina = "<script>alert('Acceso Denegado. Ir a Listado Importaciones primero.');</script>";
                    }
                    break;

                case "5-2":
                    if (pag_actual.Equals("w_TransferListado.aspx"))
                    {
                        if (numtra == null)
                            s_pagina = "<script>alert('Debe seleccionar una tarea.');</script>";
                    }
                    else
                    {
                        s_pagina = "<script>alert('Acceso Denegado. Ir a Listado Transferencias primero.');</script>";
                    }
                    break;

                    //default:
                    //    s_pagina = "<script>alert('Error en redirección.');</script>";
                    //    break;
            }
            return s_pagina;
        }
    }
}