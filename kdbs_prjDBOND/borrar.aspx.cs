﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Web.Services;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Script.Serialization;

namespace kdbs_prjDBOND
{
    public partial class borrar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ShowData();
        }

        private void ShowData()
        {
            String myConnection = ConfigurationManager.ConnectionStrings["ReportesBI"].ToString();
            SqlConnection con = new SqlConnection(myConnection);
            String query = "select * from vw_BI_Almacenes";
            SqlCommand cmd = new SqlCommand(query, con);
            DataTable tb = new DataTable();
            try
            {
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                tb.Load(dr, LoadOption.OverwriteChanges);
                con.Close();
            }
            catch { }

            if (tb != null)
            {
                String chart = "";
                // You can change your chart height by modify height value
                chart = "<canvas id=\"line-chart\" width =\"100%\" height=\"40\"></canvas>";
                chart += "<script>";
                chart += "new Chart(document.getElementById(\"line-chart\"), { type: 'bar', data: { labels: [";

                // more details in x-axis
                for (int i = 0; i < 30; i++)
                    //chart += i.ToString() + ",";
                    chart += "\"A\"" + ",";
                chart = chart.Substring(0, chart.Length - 1);

                chart += "],datasets: [{ data: [";

                // put data from database to chart
                String value = "";
                for (int i = 0; i < tb.Rows.Count; i++)
                    value += tb.Rows[i]["codtip"].ToString() + ",";
                value = value.Substring(0, value.Length - 1);

                chart += value;

                chart += "],label: \"Air Temperature\", borderColor: \"#3e95cd\",fill: true, backgroundColor: \"#3e95cd\"}"; // Chart color
                chart += "]},options: { title: { display: true,text: 'Air Temperature (oC)'} } "; // Chart title
                chart += "});";
                chart += "</script>";

                ltChart.Text = chart;
            }
        }
    }

}