﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace kdbs_prjDBOND
{
    public partial class w_Error404 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //SE COMENTO !IPSPOSTBACK POR QUE SE ESTABA CARGANDO MAS DE UNA VEZ POR RAZONES DESCONOCIDAS
            if (!IsPostBack)
            {
                Session["gs_PagPre"] = Request.UrlReferrer.ToString(); //PARA OBTENER PAGINA PREVIA
            }
        }

        protected void lkbtnVolver_Click(object sender, EventArgs e)
        {
            if (!Session["gs_PagPre"].ToString().Equals(""))            
                Response.Redirect(Session["gs_PagPre"].ToString());
        }
    }
}