﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace kdbs_prjDBOND
{
    public class clsError
    {
        public string NoError { get; set; }
        public string Mensaje { get; set; }
        public string Donde { get; set; }
        public string Objeto { get; set; }
        public string Evento { get; set; }
        public string Linea { get; set; }
        public DateTime Fecha { get; set; }
        public Exception Exception { get; set; }

        public clsError f_ErrorControlado(Exception ex)
        {
            clsError objError = new clsError();
            objError = new clsError();
            objError.Exception = ex;
            //objError.NoError = ex.HResult.ToString(); //HResult no vale con framework V4.0
            objError.Mensaje = "❗ " + ex.Message;
            StackTrace st = new StackTrace(ex, true);
            //StackFrame frame = st.GetFrame(0);
            HttpContext con = HttpContext.Current;
            //objError.Donde = con.Request.Url.ToString();
            //objError.Objeto = ex.Source;
            //objError.Evento = frame.GetMethod().DeclaringType.FullName + ":" + frame.GetMethod().Name;
            ////var LineNumber = new StackTrace(ex, true).GetFrame(0).GetFileLineNumber();
            //objError.Linea = frame.GetFileLineNumber().ToString();
            //objError.Fecha = DateTime.Now;

            StackFrame frame = null;
            for (int i = 0; i < st.FrameCount; i++) //Para los cuando StackTrace daba 0 en line number.... ver https://stackoverflow.com/questions/59246357/line-number-is-not-included-in-exception-stacktrace-code-dynamically-compiled-w
            {
                frame = st.GetFrame(i);
                if (frame.GetFileLineNumber() > 0) break;
            }
            objError.Donde = frame.GetFileName();
            objError.Objeto = ex.Source;
            objError.Evento = frame.GetMethod().DeclaringType.FullName + " : " + frame.GetMethod().Name;
            //var LineNumber = new StackTrace(ex, true).GetFrame(0).GetFileLineNumber();
            objError.Linea = frame.GetFileLineNumber().ToString();
            objError.Fecha = DateTime.Now;
            return objError;
        }

        public string f_ErrorNuevo(string s_num, clsError objError)
        {
            return s_num +" "+ objError.Fecha.ToString() + "<br/> Mensaje: " + objError.Mensaje +
                    "<br/> Objeto: " + objError.Objeto + "<br/> Evento: " + objError.Evento +
                    "<br/> Línea: " + objError.Linea;
        }
    }
}