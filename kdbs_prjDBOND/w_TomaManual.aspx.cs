﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace kdbs_prjDBOND
{
    public partial class w_TomaManual : System.Web.UI.Page
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
        protected clsSecuencia objSecuencia = new clsSecuencia();
        protected clsIngreso objIngreso = new clsIngreso();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["gs_CodUs1"] != null)
                {
                    lblUsuario.Text = Session["gs_CodUs1"].ToString();
                    lblUsuarioDrop.Text = Session["gs_CodUs1"].ToString();
                }
                else
                {
                    Response.Redirect("w_Login.aspx");
                }

                Session.Add("gs_ContVal", "0"); //para que al aplastar muchas veces solo se haga 1
                txtFecegr.Text = DateTime.Now.ToString("yyyy-MM-dd");
                ddlCodalm.DataSource = sqldsAlmacen;
                ddlCodalm.DataBind();
                ddlCodalm.Items.Insert(0, new ListItem("--Seleccionar--", "%"));
                txtCodpro.Text = "1791354419001       ";
                txtNompro.Text = "DAVILA BOND";
                DataTable dt = new DataTable();

                if (Session["gs_Numfac"]==null)
                {
                    //SECUENCIA N° EGR                
                    objSecuencia = objSecuencia.f_CalcularSecuencia(Session["gs_CodEmp"].ToString(), Session["gs_SerSec"].ToString(), "CP_ING");
                    if (String.IsNullOrEmpty(objSecuencia.Error.Mensaje))
                    {
                        Session.Add("gs_NumFac", objSecuencia.Numfac);
                        Session.Add("gs_Observ", "");
                        Session.Add("gs_Codpro", "1791354419001       ");
                        Session.Add("gs_Nompro", "DAVILA BOND");
                        txtNumegr.Text = objSecuencia.Numfac;
                        if (String.IsNullOrEmpty(objSecuencia.Numfac))
                        {
                            lblError.Text = DateTime.Now + " *07. Error al calcular Secuencia.";
                        }
                    }
                    else
                    {
                        lblError.Text = objSecuencia.Error.f_ErrorNuevo("*16.", objSecuencia.Error);
                    }

                    dt.Columns.Add("codart", typeof(string));
                    dt.Columns.Add("nomart", typeof(string));
                    dt.Columns.Add("cantid", typeof(decimal));
                    dt.Columns.Add("preuni", typeof(decimal));
                    dt.Columns.Add("coduni", typeof(string));
                    //dt.Columns.Add("desart", typeof(string));
                }
                else
                {
                    txtNumegr.Text = Session["gs_Numfac"].ToString();
                    txtObserv.Text = Session["gs_Observ"].ToString();
                    txtCodpro.Text = Session["gs_Codpro"].ToString();
                    txtNompro.Text = Session["gs_Nompro"].ToString();
                    //PARA LLENAR grvRenglonespedpro
                    DataSourceSelectArguments args = new DataSourceSelectArguments(); //para pasar del SqlDataSource1 a una DataTable
                    DataView view = (DataView)sqldsRenglonesIng.Select(args);            //para pasar del SqlDataSource1 a una DataTable
                    dt = view.ToTable();
                }

                alertExiste.Visible = true;
                if (Session["gs_Existe"] != null)
                {                    
                    alertExiste.Attributes.Add("class", "alert alert-warning alert-dismissible fade show");
                    alertIngresoText.InnerHtml = "<strong> Modificando</strong> ingreso.";                    
                }
                else
                {
                    alertExiste.Attributes.Add("class", "alert alert-primary alert-dismissible fade show");
                    alertIngresoText.InnerHtml = "<strong> Nuevo</strong> ingreso.";
                }

                Session.Add("gdt_Articulos", dt);
                grvRenglonesIng.DataSource = dt;
                grvRenglonesIng.DataBind();
                f_CantidadTotal();
            }

            if (IsPostBack)
            {
                alertExiste.Visible = false;
                lblExito.Text = "";
                if (!String.IsNullOrEmpty(txtBuscar.Text))
                {
                    string codart = txtBuscar.Text.Trim();
                    Session.Add("gs_Codart", codart);

                    clsArticulo objArticulo = f_BuscarArticulo(codart);
                    f_ActualizarCampos(objArticulo);
                    txtBuscar.Focus();
                }                
            }
        }

        protected void f_CantidadTotal()
        {
            DataTable dt = (DataTable)Session["gdt_Articulos"];
            int cantot = 0;
            foreach (DataRow row in dt.Rows)
            {
                cantot += row["cantid"] != null ? Convert.ToInt32(row["cantid"]) : 0;
            }

            txtNumtar.Text = cantot.ToString();
        }
        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            if (Session["gs_ContVal"].ToString().Equals("0")) //para que al aplastar muchas veces solo se haga 1
            {
                Session["gs_ContVal"] = "1";
                if (String.IsNullOrEmpty(txtCodpro.Text))
                {
                    Session["gs_ContVal"] = "0";
                    lblError.Text = "Debe seleccionar un proveedor antes de guardar.";
                }
                else
                {
                    f_ActualizarDatable();
                    if (Session["gs_Existe"] != null)
                    {
                        f_actualizarArticulos();
                    }
                    else
                    {
                        f_insertarArticulos(); //est002=1 ...para q wladi sepa si fue parcial o completo
                    }

                    //PARA ACTUALIZAR COLORES DE GRVENCABEZADOPEDPRO (VERDE)
                    //if (lblError.Text.Trim() == "") //control de errores
                    //{
                    //    f_actualizarColoresGrv(Session["gs_CodEmp"].ToString(), Session["gs_Numfac"].ToString(),
                    //        "1", "1", Session["gs_CodUs1"].ToString());
                    //}

                    //redireccion despues de guardar
                    if (lblError.Text.Trim() == "") //control de errores
                    {
                        lblExito.Text = "✔️ Guardado Exitoso.";
                        Session["gs_ContVal"] = "0";
                        Session["gs_Existe"] = "SI";
                        //Response.Redirect("w_TomaManual.aspx");
                    }
                }
            }
        }

        protected void lkgrvBorrar_Click(object sender, EventArgs e)
        {
            //antes de borrar se actualizar el CurrentTable con grvRenglones
            //int rowIndex = 0;

            //if (Session["gdt_Articulos"] != null)
            //{
            //    DataTable dtCurrentTable = (DataTable)Session["gdt_Articulos"];
            //    DataRow drCurrentRow = null;
            //    if (dtCurrentTable.Rows.Count > 0)
            //    {
            //        for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
            //        {
            //            TextBox cantid = (TextBox)grvRenglonesPuntosVenta.Rows[rowIndex].Cells[4].FindControl("txtgrvCantid");

            //            drCurrentRow = dtCurrentTable.NewRow();
            //            drCurrentRow["N°"] = i + 1;

            //            dtCurrentTable.Rows[i - 1]["Código"] = codart.Text;
            //            dtCurrentTable.Rows[i - 1]["Descripción"] = nomart.Text;
            //            dtCurrentTable.Rows[i - 1]["Cantidad"] = cantid.Text;
            //            dtCurrentTable.Rows[i - 1]["PVP"] = preuni.Text;
            //            dtCurrentTable.Rows[i - 1]["IVA"] = ivacal.Text;
            //            dtCurrentTable.Rows[i - 1]["%"] = proiva.Text;
            //            dtCurrentTable.Rows[i - 1]["Dsco"] = desren.Text;
            //            dtCurrentTable.Rows[i - 1]["% Des."] = coddes.Text;
            //            dtCurrentTable.Rows[i - 1]["Total"] = totren.Text;

            //            rowIndex++;
            //        }
            //        ViewState["CurrentTable"] = dtCurrentTable;
            //    }
            //}

            LinkButton lb = (LinkButton)sender;
            GridViewRow gvRow = (GridViewRow)lb.NamingContainer;
            int rowID = gvRow.RowIndex;
            if (Session["gdt_Articulos"] != null)
            {
                DataTable dt = (DataTable)Session["gdt_Articulos"];
                if (dt.Rows.Count > 0) //si selecciona una fila que NO sea la primera
                {
                    //Remove the Selected Row data
                    dt.Rows.Remove(dt.Rows[rowID]);
                    //if (!(gvRow.RowIndex == 0 && dt.Rows.Count == 1)) //no permite borrar la primera fila
                    //{
                        
                    //}
                }
                //else //si selecciona la primera fila
                //{
                //    if (gvRow.RowIndex == 0 && dt.Rows.Count == 1) //para borrar los datos de la primera fila
                //    {
                //        dt.Rows[0]["codart"] = "";
                //        dt.Rows[0]["nomart"] = "";
                //        dt.Rows[0]["cantid"] = "";
                //        dt.Rows[0]["preuni"] = "";
                //        dt.Rows[0]["coduni"] = "";
                //    }
                //}

                //Store the current data in ViewState for future reference
                Session["gdt_Articulos"] = dt;
                //Re bind the GridView for the updated data
                grvRenglonesIng.DataSource = dt;
                grvRenglonesIng.DataBind();
            }
        }

        public clsArticulo f_BuscarArticulo(string s_codart)
        {
            //OJO se quito desart por q en base DBOND no tienen
            DataTable dt = (DataTable)Session["gdt_Articulos"];
            clsArticulo objArticulo = new clsArticulo();

            try
            {
                if (dt.Rows.Count > 0)
                {
                    //DA 13 JUL 21 AL PISTOLEAR SIEMPRE SE INSERTA NUEVA FILA
                    //foreach (DataRow row in dt.Rows)
                    //{
                    //    //if (row.Field<string>("codart").Trim().Equals(s_codart.Trim()) || row.Field<string>("desart").Trim().Equals(s_codart.Trim()))
                    //    if (row.Field<string>("codart").Trim().Equals(s_codart.Trim()))
                    //    {
                    //        objArticulo.Codart = s_codart;
                    //        string s_nomart = row.Field<string>("nomart");
                    //        objArticulo.Nomart = s_nomart;
                    //        int i_cantid = (int)row.Field<decimal>("cantid") + 1;
                    //        //actualiza cantid del dt
                    //        row["cantid"] = i_cantid;
                    //        objArticulo.Cantid = i_cantid;
                    //        objArticulo.Error = "CORRECTO";

                    //        Session["gdt_Articulos"] = dt;
                    //        grvRenglonesIng.DataSource = dt;
                    //        grvRenglonesIng.DataBind();
                    //        f_CantidadTotal();
                    //        return objArticulo;
                    //    }
                    //}

                    //si aun no hace return es que no hay el articulo en la lista
                    objArticulo = f_selectArticulo(Session["gs_CodEmp"].ToString(), s_codart);
                    if (!String.IsNullOrEmpty(objArticulo.Nomart))
                    {
                        DataRow dr = dt.NewRow();
                        dr[0] = objArticulo.Codart;
                        dr[1] = objArticulo.Nomart;
                        dr[2] = objArticulo.Cantid;
                        dr[3] = objArticulo.Preuni;
                        dr[4] = objArticulo.Coduni;
                        dt.Rows.Add(dr);

                        Session["gdt_Articulos"] = dt;
                        grvRenglonesIng.DataSource = dt;
                        grvRenglonesIng.DataBind();
                        f_CantidadTotal();
                        return objArticulo;
                    }
                }
                else
                {
                    objArticulo = f_selectArticulo(Session["gs_CodEmp"].ToString(), s_codart);
                    if (!String.IsNullOrEmpty(objArticulo.Nomart))
                    {
                        DataRow dr = dt.NewRow();
                        dr[0] = objArticulo.Codart;
                        dr[1] = objArticulo.Nomart;
                        dr[2] = objArticulo.Cantid;
                        dr[3] = objArticulo.Preuni;
                        dr[4] = objArticulo.Coduni;
                        dt.Rows.Add(dr);

                        Session["gdt_Articulos"] = dt;
                        grvRenglonesIng.DataSource = dt;
                        grvRenglonesIng.DataBind();
                        f_CantidadTotal();
                        return objArticulo;
                    }
                }
            }
            catch (Exception ex)
            {
                objArticulo.Codart = "-";
                objArticulo.Nomart = "-";
                objArticulo.Cantid = 0;
                objArticulo.Error = "*02. " + DateTime.Now + " " + ex.Message;
                return objArticulo;
            }

            objArticulo.Codart = "-";
            objArticulo.Nomart = "-";
            objArticulo.Cantid = 0;
            objArticulo.Error = " *03. No se encontró artículo.";
            return objArticulo;
        }

        public clsArticulo f_selectArticulo(string s_codemp, string s_codart)
        {
            clsArticulo objArticulo = new clsArticulo();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_S_INGRESOS_ARTICULO]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CODEMP", s_codemp);
            cmd.Parameters.AddWithValue("@CODART", s_codart);

            cmd.Parameters.Add("@s_Nomart", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@r_Preuni", SqlDbType.Decimal, 20).Direction = ParameterDirection.Output;
            cmd.Parameters["@r_Preuni"].Precision = 20;
            cmd.Parameters["@r_Preuni"].Scale = 2;
            cmd.Parameters.Add("@s_Coduni", SqlDbType.Char, 3).Direction = ParameterDirection.Output;
            try
            {
                //SqlDataAdapter sda = new SqlDataAdapter(cmd);
                //sda.Fill(dt);
                cmd.ExecuteNonQuery();
                objArticulo.Codart = s_codart;
                objArticulo.Nomart = cmd.Parameters["@s_Nomart"].Value.ToString().Trim();
                if (!String.IsNullOrEmpty(objArticulo.Nomart))
                {
                    objArticulo.Cantid = 1;
                    objArticulo.Preuni = !String.IsNullOrEmpty(cmd.Parameters["@r_Preuni"].Value.ToString().Trim()) ? Convert.ToDecimal(cmd.Parameters["@r_Preuni"].Value.ToString().Trim()) : 0;
                    objArticulo.Coduni = cmd.Parameters["@s_Coduni"].Value.ToString().Trim();
                    objArticulo.Error = "CORRECTO";
                }
            }
            catch (Exception ex)
            {
                lblError.Text = "*07. " + DateTime.Now + " " + ex.Message;
                //throw ex;
            }
            conn.Close();
            return objArticulo;
        }

        public void f_pintarGridview(string s_codart, int i_cantid) //pinta y llena el chkConfirmar
        {
            foreach (GridViewRow row in grvRenglonesIng.Rows)
            {

                //if ((int)decimal.Parse(row.Cells[2].Text.Trim()) == 0) //llena el chkConfirmar
                //{
                //    CheckBox rBoton = (CheckBox)row.Cells[4].FindControl("chkConfirmado");
                //    rBoton.Checked = true;
                //}
                string codart1 = row.Cells[2].Text.Trim();
                //string alterno = row.Cells[5].Text.Trim();

                if (row.Cells[2].Text.Trim().Equals(s_codart.Trim())) //pinta la fila con codart
                {
                    if (i_cantid > 0)
                        row.BackColor = Color.LightGreen;
                    else
                        row.BackColor = Color.Khaki;
                }
            }
        }

        protected void btnDespachar_Click(object sender, EventArgs e)
        {
            clsArticulo objArticulo = new clsArticulo();
            try
            {
                objArticulo.Codart = txtCodart.Text;
                objArticulo.Nomart = txtNomart.Text;
                objArticulo.Cantid = (int)decimal.Parse(txtSaldo.Text);
                objArticulo.Error = "";
                DataTable dt = (DataTable)Session["gdt_Articulos"];
                string s_codart = txtCodart.Text.Trim();

                int i_cantidDesapachar = 1;
                if (!txtCantid.Text.Equals("")) //si deja vacio txtCantid se asume que es 1
                {
                    i_cantidDesapachar = int.Parse(txtCantid.Text.ToString());
                }

                foreach (DataRow row in dt.Rows)
                {
                    if (row.Field<string>("codart").Trim().Equals(s_codart.Trim()))
                    {
                        //if ((int)row.Field<decimal>("cantid") < i_cantidDesapachar) //si ingresa cantidad mayor que saldo existente
                        //{
                        //    txtCantid.Text = "1";
                        //    objArticulo.Error = "*10. Cantidad ingresada es superior a Saldo. Despacho no realizado.";
                        //    f_ActualizarCampos(objArticulo);
                        //}
                        //else //cantidad correcta
                        //{

                        //}

                        objArticulo.Codart = s_codart;
                        string s_nomart = row.Field<string>("nomart");
                        objArticulo.Nomart = s_nomart;
                        int i_cantid = (int)row.Field<decimal>("cantid") + i_cantidDesapachar;

                        //actualiza el dt
                        row["cantid"] = i_cantid;

                        objArticulo.Cantid = i_cantid;
                        objArticulo.Error = "CORRECTO";

                        Session["gdt_Articulos"] = dt;
                        //grvRenglonespedpro.Columns[5].Visible = true; //para q f_MostrarCodAlterno() funcione
                        grvRenglonesIng.DataSource = dt;
                        grvRenglonesIng.DataBind();
                        f_CantidadTotal();

                        //f_MostrarCodAlterno(); //PARA MOSTRAR CODIGO ALTERNO
                        //f_pintarGridview(s_codart, i_cantid, s_desart);
                        //grvRenglonespedpro.Columns[5].Visible = false; //ocultar columna extra...DEBE IR AQUI PARA QUE COLORES DE GRV FUNCIONEN
                        f_ActualizarCampos(objArticulo);
                    }
                }
                //aqui poner objArticulo.Codart = "-"; .....
            }
            catch (Exception ex)
            {
                objArticulo.Codart = "-";
                objArticulo.Nomart = "-";
                objArticulo.Cantid = 0;
                objArticulo.Error = "*11. " + DateTime.Now + " " + ex.Message;
                f_ActualizarCampos(objArticulo);
            }
        }

        public void f_ActualizarCampos(clsArticulo objArticulo)
        {
            txtCodart.Text = objArticulo.Codart;
            txtNomart.Text = objArticulo.Nomart;
            txtSaldo.Text = objArticulo.Cantid.ToString();
            if (!objArticulo.Error.Equals("CORRECTO"))
            {
                lblError.Text = "*01. " + DateTime.Now + objArticulo.Error;
            }
            else
            {
                lblError.Text = "";
            }
            txtBuscar.Text = "";
        }

        public void f_ActualizarDatable()
        {
            DataTable dtArticulos = (DataTable)Session["gdt_Articulos"];

            int count = 0;
            foreach (GridViewRow row in grvRenglonesIng.Rows)
            {
                TextBox txtgrvCantid = (TextBox)row.FindControl("txtgrvCantid");
                dtArticulos.Rows[count]["cantid"] = txtgrvCantid.Text;
                count++;
            }
            Session["gdt_Articulos"] = dtArticulos;
        }

        //***i PARA LLENAR INGRESOS**********************************************************
        public void f_insertarArticulos()
        {
            lblError.Text = "";
            string codemp = Session["gs_CodEmp"].ToString();
            string numfac = "";

            //numfac = f_CalcularSecuencia().Trim();
            objSecuencia = objSecuencia.f_CalcularSecuencia(Session["gs_CodEmp"].ToString(), Session["gs_SerSec"].ToString(), "CP_ING");
            if (String.IsNullOrEmpty(objSecuencia.Error.Mensaje))
            {
                numfac = objSecuencia.Numfac.Trim();
                if (String.IsNullOrEmpty(numfac))
                {
                    lblError.Text = DateTime.Now +" *08. Error al calcular Secuencia.";
                }
            }
            else
                lblError.Text = objSecuencia.Error.f_ErrorNuevo("*16.", objSecuencia.Error);

            string codpro = txtCodpro.Text;
            string codalm = ddlCodalm.SelectedValue != "%" ? ddlCodalm.SelectedValue : Session["gs_CodAlm"].ToString();
            string codusu = Session["gs_CodUs1"].ToString(); //se inserta tambien en usuing
            string tiptra = ""; //Session["gs_Tiptar"].ToString(); //???
            string numtra = ""; //Session["gs_Numtar"].ToString(); //???
            //string codcli = Session["gs_Codcli"].ToString();
            //string nomcli = Session["gs_Nomcli"].ToString();            
            //string codven = Session["gs_CodVen"].ToString();            
            string sersec = Session["gs_SerSec"].ToString();
            string codsuc = Session["gs_CodSuc"].ToString();
            string observ = txtObserv.Text;
            clsError objError = new clsError();
            if (String.IsNullOrEmpty(lblError.Text))
            {
                objError = objIngreso.f_llenarEncabezadoIngresos(codemp, numfac, codpro, codalm, codusu,
                tiptra, numtra, sersec, codsuc, codusu, observ); //se llama al SP

                if (String.IsNullOrEmpty(objError.Mensaje)) //si no hubo error al insertar en el encabezado
                {
                    DataTable dtArticulos = (DataTable)Session["gdt_Articulos"];
                    long numren1 = 0;
                    long numite1 = 0;
                    foreach (DataRow row in dtArticulos.Rows)
                    {
                        //Session.Add("gs_Codart", row.Field<string>("codart"));
                        string codart1 = row.Field<string>("codart");
                        numren1 += 1;
                        numite1 += 1;
                        string nomart1 = row.Field<string>("nomart");
                        string coduni1 = row.Field<string>("coduni");
                        decimal preuni1 = row.Field<decimal>("preuni");
                        //string ubifis1 = row.Field<string>("ubifis");
                        decimal cantid1 = row.Field<decimal>("cantid");

                        if (lblError.Text.Trim() == "") //control de errores
                        {
                            objError = objIngreso.f_llenarRenglonesIngresos(codart1, codemp, numfac, numren1, numite1, nomart1, coduni1,
                            preuni1, cantid1, codalm);
                            if (!String.IsNullOrEmpty(objError.Mensaje))
                                lblError.Text = objError.f_ErrorNuevo("*05.", objError);
                        }
                    }
                }
                else
                {
                    lblError.Text = objError.f_ErrorNuevo("*10.", objError);
                }
            }
        }

        public void f_actualizarArticulos()
        {
            lblError.Text = "";
            string codemp = Session["gs_CodEmp"].ToString();
            string numfac = Session["gs_Numfac"].ToString();
            string codpro = txtCodpro.Text;
            string codalm = ddlCodalm.SelectedValue != "%" ? ddlCodalm.SelectedValue : Session["gs_CodAlm"].ToString();
            string codusu = Session["gs_CodUs1"].ToString(); //se inserta tambien en usuing
            clsError objError = new clsError();
            DataTable dtArticulos = (DataTable)Session["gdt_Articulos"];
            long numren1 = 0;
            long numite1 = 0;

            objIngreso.f_deleteRenglonesIngresos(codemp, numfac);
            foreach (DataRow row in dtArticulos.Rows)
            {
                //Session.Add("gs_Codart", row.Field<string>("codart"));
                string codart1 = row.Field<string>("codart");
                numren1 += 1;
                numite1 += 1;
                string nomart1 = row.Field<string>("nomart");
                string coduni1 = row.Field<string>("coduni");
                decimal preuni1 = row.Field<decimal>("preuni");
                //string ubifis1 = row.Field<string>("ubifis");
                decimal cantid1 = row.Field<decimal>("cantid");

                if (lblError.Text.Trim() == "") //control de errores
                {
                    objError = objIngreso.f_llenarRenglonesIngresos(codart1, codemp, numfac, numren1, numite1, nomart1, coduni1,
                    preuni1, cantid1, codalm);
                    if (!String.IsNullOrEmpty(objError.Mensaje))
                        lblError.Text = objError.f_ErrorNuevo("*05.", objError);
                }
            }
        }
        //***f PARA LLENAR INGRESOS**********************************************************


        //***i PARA PROVEEDORES**********************************************************
        protected void grvProveedores_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow fila = grvProveedores.SelectedRow;
            string s_codpro = fila.Cells[1].Text.Trim();
            string s_nompro = fila.Cells[2].Text.Trim();
            txtCodpro.Text = s_codpro;
            txtNompro.Text = s_nompro;
            lblError.Text = "";

            ScriptManager.RegisterStartupScript(this, this.GetType(), "ModalView", "<script>$(function() { $('#modalProveedores').modal('hide'); });</script>", false);
        }

        protected void btnBuscarNompro_Click(object sender, EventArgs e)
        {
            Session["gs_Nompro"] = txtBuscarNompro.Text.Equals("") ? "%" : txtBuscarNompro.Text.Trim();
            ClientScript.RegisterStartupScript(this.GetType(), "Popup", "$('#modalProveedores').modal('show')", true);
        }

        protected void btnProveedores_Click(object sender, EventArgs e)
        {
            Session.Add("gs_Nompro", "%");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ModalView", "<script>$(function() { $('#modalProveedores').modal('show'); });</script>", false);
        }

        //***f PARA PROVEEDORES**********************************************************
    }
}