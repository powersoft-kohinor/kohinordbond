﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace kdbs_prjDBOND
{
    public class clsIngreso
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
        public clsError Error { get; set; }

        public clsError f_actualizarColoresGrv(string codemp, string numfac, string est001,
           string est002, string usu001)
        {
            //para actualizar los colores del ingresos_enc
            clsError objError = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_M_COLOR_INGRESOS_ENC]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codemp", codemp);
            cmd.Parameters.AddWithValue("@numfac", numfac);
            cmd.Parameters.AddWithValue("@est001", est001);
            cmd.Parameters.AddWithValue("@est002", est002);
            cmd.Parameters.AddWithValue("@usu001", usu001);
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
                //lblError.Text = "*" + DateTime.Now + " " + ex.Message;
            }
            conn.Close();
            return objError;
        }

        public clsError f_actualizarRenglonesIngresos(string codart1, string codemp, string numfac, decimal cantid1,
            decimal canfac, string observ)
        {
            clsError objError = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_M_INGRESOS_REN]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codart", codart1);
            cmd.Parameters.AddWithValue("@codemp", codemp);
            cmd.Parameters.AddWithValue("@numfac", numfac);
            cmd.Parameters.AddWithValue("@cantid", cantid1);
            cmd.Parameters.AddWithValue("@canfac", canfac);
            cmd.Parameters.AddWithValue("@observ", observ);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
                //lblError.Text = "*04. " + DateTime.Now + " " + ex.Message;
            }
            conn.Close();
            return objError;
        }

        public clsError f_actualizarObsev(string codart1, string codemp, string numfac, string observ)
        {
            clsError objError = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_ACTUALIZAR_INGRESOS_REN]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codemp", codemp);
            cmd.Parameters.AddWithValue("@numfac", numfac);
            cmd.Parameters.AddWithValue("@codart", codart1);
            cmd.Parameters.AddWithValue("@observ", observ);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
                //lblError.Text = "*14. " + DateTime.Now + " " + ex.Message;
            }
            conn.Close();
            return objError;
        }

        public clsError f_actualizarRenglonesIngPar(string codart1, string codemp, string numfac,
            decimal canfac, decimal candes)
        {
            clsError objError = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_M_GRABAR_PENDIENTE_ING]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codemp", codemp);
            cmd.Parameters.AddWithValue("@numfac", numfac);
            cmd.Parameters.AddWithValue("@codart", codart1);
            cmd.Parameters.AddWithValue("@canfac", canfac);
            cmd.Parameters.AddWithValue("@candes", candes);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
                //lblError.Text = "*13. " + DateTime.Now + " " + ex.Message;
            }
            conn.Close();
            return objError;
        }

        public clsError f_llenarEncabezadoIngresos(string codemp, string numfac, string codpro,
            string codalm, string codusu, string tiptra, string numtra, string sersec,
            string codsuc, string usuing, string observ)
        {
            clsSecuencia objSecuencia = new clsSecuencia();
            clsError objError = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_M_INGRESOS_ENC]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codemp", codemp);
            cmd.Parameters.AddWithValue("@numfac", numfac);
            cmd.Parameters.AddWithValue("@codpro", codpro);
            cmd.Parameters.AddWithValue("@codalm", codalm);
            cmd.Parameters.AddWithValue("@codusu", codusu);
            cmd.Parameters.AddWithValue("@tiptra", tiptra);
            cmd.Parameters.AddWithValue("@numtra", numtra);
            cmd.Parameters.AddWithValue("@sersec", sersec);
            cmd.Parameters.AddWithValue("@codsuc", codsuc);
            cmd.Parameters.AddWithValue("@usuing", usuing);
            cmd.Parameters.AddWithValue("@observ", observ);
            try
            {
                cmd.ExecuteNonQuery();
                conn.Close();
                //actualiza la secuencia
                objSecuencia.Error = objSecuencia.f_ActualizarSecuencia(codemp, sersec, "CP_ING");
                if (!String.IsNullOrEmpty(objSecuencia.Error.Mensaje))
                {
                    objError.Mensaje = objSecuencia.Error.f_ErrorNuevo("*12.", objSecuencia.Error);
                }
            }
            catch (Exception ex)
            {
                conn.Close();
                objError = objError.f_ErrorControlado(ex);
                //lblError.Text = "*04. " + DateTime.Now + ex.Message;
                //return "*04. " + DateTime.Now + " " + ex.Message;
            }
            return objError; //se inserto correctamente
        }

        public clsError f_llenarRenglonesIngresos(string codart1, string codemp, string numfac, long numren1,
            long numite1, string nomart1, string coduni1, decimal preuni1, decimal cantid1, string codalm)
        {
            clsError objError = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_M_INGRESOS_REN_INSERT]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codart", codart1);
            cmd.Parameters.AddWithValue("@codemp", codemp);
            cmd.Parameters.AddWithValue("@numfac", numfac);
            cmd.Parameters.AddWithValue("@numren", numren1);
            cmd.Parameters.AddWithValue("@numite", numite1);
            cmd.Parameters.AddWithValue("@nomart", nomart1);
            cmd.Parameters.AddWithValue("@coduni", coduni1);
            cmd.Parameters.AddWithValue("@cantid", cantid1);
            cmd.Parameters.AddWithValue("@preuni", preuni1);
            //cmd.Parameters.AddWithValue("@ubifis", ubifis1);
            cmd.Parameters.AddWithValue("@codalm", codalm);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
                //lblError.Text = "*05. " + DateTime.Now + ex.Message;
                //throw ex;
            }
            conn.Close();
            return objError;
        }

        //metodo SOLO para TomaManual
        //ya no se usa despues de actualizacion de agregar nuevo renglon por cada pitada
        public clsError f_actualizarRenglonesIngresos2(string codart1, string codemp, string numfac, long numren1,
            long numite1, string nomart1, string coduni1, decimal preuni1, decimal cantid1, string codalm) 
        {
            clsError objError = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_M_INGRESOS_REN_UPDATE]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codart", codart1);
            cmd.Parameters.AddWithValue("@codemp", codemp);
            cmd.Parameters.AddWithValue("@numfac", numfac);
            cmd.Parameters.AddWithValue("@numren", numren1);
            cmd.Parameters.AddWithValue("@numite", numite1);
            cmd.Parameters.AddWithValue("@nomart", nomart1);
            cmd.Parameters.AddWithValue("@coduni", coduni1);
            cmd.Parameters.AddWithValue("@cantid", cantid1);
            cmd.Parameters.AddWithValue("@preuni", preuni1);
            //cmd.Parameters.AddWithValue("@ubifis", ubifis1);
            cmd.Parameters.AddWithValue("@codalm", codalm);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
                //lblError.Text = "*05. " + DateTime.Now + ex.Message;
                //throw ex;
            }
            conn.Close();
            return objError;
        }


        public clsError f_deleteRenglonesIngresos(string codemp, string numfac)
        {
            clsError objError = new clsError();
            conn.Open();
            string query = "DELETE FROM renglonesingresos WHERE codemp = @codemp AND numfac = @numfac";
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@codemp", codemp);
            cmd.Parameters.AddWithValue("@numfac", numfac);
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
            }
            conn.Close();
            return objError;
        }
    }
}