﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace kdbs_prjDBOND
{
    public class clsArticulo
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
        public string Codart { get; set; }
        public string Codcla { get; set; }
        public string Codiva { get; set; }
        public string Coduni { get; set; }
        public string Nomart { get; set; }
        public string Desart { get; set; }
        public string Prec01 { get; set; }
        public string Codmod { get; set; }
        public int Cantid { get; set; } //representa el saldo existente (txtSaldo)
        public decimal Preuni { get; set; }
        
        public string Error { get; set; }

        public DataTable dtArticulo { get; set; }
        public clsError ErrorReal { get; set; }
        public clsArticulo f_Articulo_Buscar(string codemp, string codart,  string codalt, string desart, string nomart)
        {
            clsArticulo objArticulo = new clsArticulo();
            objArticulo.ErrorReal = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_M_ARTICULOS]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the select task to get all data
            cmd.Parameters.AddWithValue("@Action", "Select");
            cmd.Parameters.AddWithValue("@Codemp", codemp);
            cmd.Parameters.AddWithValue("@Codart", codart);
            cmd.Parameters.AddWithValue("@Nomart", nomart);
            cmd.Parameters.AddWithValue("@Codalt", codalt);
            cmd.Parameters.AddWithValue("@Desart", desart);

            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                objArticulo.dtArticulo = dt;
            }
            catch (Exception ex)
            {
                objArticulo.ErrorReal = objArticulo.ErrorReal.f_ErrorControlado(ex);
            }

            conn.Close();
            return objArticulo;
        }

        public clsArticulo f_Articulo_Almacen_Buscar(string codemp, string codart)
        {
            clsArticulo objArticulo = new clsArticulo();
            objArticulo.ErrorReal = new clsError();
            conn.Open();
            SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_M_ARTICULOS]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            // call the select task to get all data
            cmd.Parameters.AddWithValue("@Action", "Almacen");
            cmd.Parameters.AddWithValue("@Codemp", codemp);
            cmd.Parameters.AddWithValue("@Codart", codart);

            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                objArticulo.dtArticulo = dt;
            }
            catch (Exception ex)
            {
                objArticulo.ErrorReal = objArticulo.ErrorReal.f_ErrorControlado(ex);
            }

            conn.Close();
            return objArticulo;
        }
    }
}