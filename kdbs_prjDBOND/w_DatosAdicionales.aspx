﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="w_DatosAdicionales.aspx.cs" Inherits="kdbs_prjDBOND.w_DatosAdicionales" %>


<%@ Register Src="~/WebMenuUserControl.ascx" TagPrefix="uc1" TagName="WebMenuUserControl" %>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Kohinor Móvil - DBOND</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">
<form id="form1" runat="server">
  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
      <uc1:WebMenuUserControl runat="server" id="WebMenuUserControl1" />
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button onclick="return false;" id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Search -->
          <div class="d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
              <div class="row">
                  <div class="col">
                      <h6 class="m-0 font-weight-bold text-primary">Existencia Almacenes</h6>
                  </div>
              </div>
            <%--<div class="input-group">
              <input type="text" class="form-control bg-light border-0 small" placeholder="Buscar..." aria-label="Search" aria-describedby="basic-addon2">
              <div class="input-group-append">
                <button class="btn btn-primary" type="button">
                  <i class="fas fa-search fa-sm"></i>
                </button>
              </div>
            </div>--%>
          </div>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <%--<!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>--%>

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">
                    <asp:Label ID="lblUsuario" runat="server"></asp:Label>
                </span>
                <img class="img-profile rounded-circle" src="Imagenes/catUser.png">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">                     
                  <asp:HyperLink ID="hpkUsuario" class="dropdown-item" runat="server">                      
                      <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                      <asp:Label ID="lblUsuarioDrop" runat="server"></asp:Label>
                  </asp:HyperLink>
                
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid" style="display:none">
          <!-- BAJO CONSTRUCCION Error Text -->
          <div class="text-center">
            <div class="error mx-auto" data-text="000">! ! !</div>
            <p class="lead text-gray-800 mb-5">Página En Construcción</p>
            <p class="text-gray-500 mb-0">Parece que la página que busca no esta lista...</p>
          </div>
        </div>


        <div class="container-fluid" >
          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between">
             <asp:LinkButton ID="lkbtnVolver" runat="server" class="btn btn-primary shadow-sm btn-sm" PostBackUrl="~/w_ListadoProductos.aspx">
                <i class="fas fa-arrow-left"></i> Volver</asp:LinkButton>
              <asp:Label ID="lblError" runat="server" Text="" ForeColor="Maroon"></asp:Label>
            <%--<h1 class="h3 mb-0 text-gray-800"><strong>Despacho</strong></h1>--%>
            <%--<a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>--%>
          </div>
            <div class="card shadow mb-4">
                <div class="card-body">
                    <label style="color: black">
                        <strong>
                            <asp:Label ID="lblNomart" runat="server" Text='<%# Eval("nomart") %>'></asp:Label></strong></label>

                </div>
            </div>
            <div class="row">

                        <!-- Earnings (Monthly) Card Example -->
                        <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card border-left-primary shadow">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                Compras</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">
                                                <label style="color: black">
                                                <asp:Label ID="lblCompras" runat="server"></asp:Label></label>
                                            </div>
                                        </div>
                                        <div class="col  mr-2">
                                            <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                                        </div>
                                         <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                                Ventas</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">
                                                <label style="color: black">
                                                <asp:Label ID="lblVentas" runat="server"></asp:Label></label>
                                            </div>
                                        </div>
                                        <div class="col  mr-2">
                                           <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Earnings (Monthly) Card Example -->
                    <%--    <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card border-left-success shadow">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                                Ventas</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">
                                                <label style="color: black">
                                                <asp:Label ID="lblVentas" runat="server"></asp:Label></label>
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>--%>

                        <!-- Earnings (Monthly) Card Example -->
                       <!-- Earnings (Monthly) Card Example -->
                        <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card border-left-success shadow">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                                Saldo</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">
                                                <label style="color: black">
                                                <asp:Label ID="lblSaldo" runat="server" Text='<%# Eval("saldo") %>'></asp:Label></label>
                                            </div>
                                        </div>
                                        <div class="col  mr-2">
                                            <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                                        </div>
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                PVP.</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">
                                                <label style="color: black">
                                                <asp:Label ID="lblPrec01" runat="server" Text='<%# Eval("prec01") %>'></asp:Label></label>
                                            </div>
                                        </div>
                                        <div class="col mr-2">
                                            <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Pending Requests Card Example -->
                       <%--  <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card border-left-primary shadow">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                PVP.</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">
                                                <label style="color: black">
                                                <asp:Label ID="lblPrec01" runat="server" Text='<%# Eval("prec01") %>'></asp:Label></label>
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>--%>
                    </div>

              <div class="card shadow mb-4">
                <div class="card-body">
                  <div class="row">
                    <div class="col-xl-2">
                           
                                 <img src="Imagenes/SacoABMariela748.png" class="img-thumbnail img-fluid"/>
                      </div>

                      <div class="col-xl-3">
                           <div class="row">
                                <div class="col-6">
                                     <h3> Datos </h3>
                                          
                                </div>
                            </div>
                          <br />

                            <div class="row">
                                <div class="col-4">
                                     <h6> Código </h6>
                                            
                                </div>
                                <div class="col-8">
                                    <label style="color: black">
                                                <asp:Label ID="lblCodart" runat="server" Text='<%# Eval("codart") %>'></asp:Label></label>
                                </div>
                            </div>
                          <div class="row">
                                <div class="col-4">
                                     <h6> SKU </h6>
                                       
                                </div>
                              <div class="col-8">
                                   <label style="color: black">
                                                <asp:Label ID="lblDesart" runat="server" Text='<%# Eval("desart") %>'></asp:Label></label>
                              </div>

                            </div>
                          <div class="row">
                                <div class="col-4">
                                     <h6> Clase </h6>
                                       
                                </div>
                              <div class="col-8">
                                   <label style="color: black">
                                                <asp:Label ID="lblNomcla" runat="server" Text='<%# Eval("nomcla") %>'></asp:Label></label>
                              
                              </div>

                            </div>
                          <div class="row">
                                <div class="col-4">
                                     <h6> Familia </h6>
                                           
                                </div>
                              <div class="col-8">
                               <label style="color: black">
                                                <asp:Label ID="lblNomfam" runat="server" Text='<%# Eval("nomfam") %>'></asp:Label></label>

                                </div>
                            </div>
                          <div class="row">
                                <div class="col-4">
                                     <h6> Modelo </h6>
                                        
                                </div>
                               <div class="col-8">
                              <label style="color: black">
                                                <asp:Label ID="lblModbas" runat="server" Text='<%# Eval("modbas") %>'></asp:Label></label>
                               </div>

                            </div>
                                 
                                 
                      </div>

                      <div class="col-xl-7">
                           <div class="row">
                                <div class="col-6">
                                     <h4> Datos Técnicos </h4>
                                        
                                </div>
                            </div>
                            <br />

                            <div class="row">
                                <div class="col-4">
                                     <h6> Referencia </h6>
                                     
                                </div>

                                <div class="col-8">
                                    <label style="color: black"><strong>
                                                <asp:Label ID="lblCodalt" runat="server" Text='<%# Eval("codalt") %>'></asp:Label></strong></label>
                                </div>
                                


                            </div>
                          <div class="row">
                                <div class="col-3">
                                     <h6> Galga </h6>
                                           
                                </div>
                               <div class="col-3">
                                    <label style="color: black">
                                                <asp:Label ID="lblGalmat" runat="server" Text='<%# Eval("galmat") %>'></asp:Label></label>
                                   </div>

                              <div class="col-3">
                                     <h6> Hebras </h6>
                                            
                              </div>
                             <div class="col-3">
                                  <label style="color: black">
                                                <asp:Label ID="lblNumheb" runat="server" Text='<%# Eval("numheb") %>'></asp:Label></label>
                                 </div>
                               
                            </div>
                          
                          <div class="row">
                                <div class="col-4">
                                     <h6> Materiales </h6>
                                        
                                </div>
                                  <div class="col-8">
                                      <label style="color: black">
                                                <asp:Label ID="lblDesmat" runat="server" Text='<%# Eval("desmat") %>'></asp:Label></label>
                                </div>
                            </div>
                          <br />
                          <div class="row">
                                <div class="col-4">
                                     <h6> Desc. </h6>
                                           
                                </div>
                                  <div class="col-8">
                                       <label style="color: black">
                                                <asp:Label ID="lblDescri" runat="server" Text='<%# Eval("descri") %>'></asp:Label></label>
                                </div>
                            </div>
                                 
                                 
                      </div>
                      
                   </div>
                  

               
          
          </div>
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; KOHINOR 2021 Todos Los Derechos Reservados</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">¿Desea salir?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Seleccionar "Logout" abajo para salir de la sesión actual.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
          <a class="btn btn-primary" href="w_Login.aspx">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/chart-area-demo.js"></script>
  <script src="js/demo/chart-pie-demo.js"></script>

  <!--i SQL DATASOURCES-->
  <asp:SqlDataSource ID="sqldsDASHBOARD_DETALLE" runat="server" ConnectionString="<%$ ConnectionStrings:kdbs_EsperanzaConnectionString %>" SelectCommand="PVT_S_DASHBOARD_EXISTENCIA_IMG" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="CODEMP" SessionField="gs_CodEmp" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="codart" SessionField="gs_Codart" Type="String" />
            <asp:SessionParameter DefaultValue="0.0" Name="exiact" SessionField="gs_Exiact" Type="Decimal" />
        </SelectParameters>
    </asp:SqlDataSource>
  <!--f SQL DATASOURCES-->

</form>
</body>

</html>


