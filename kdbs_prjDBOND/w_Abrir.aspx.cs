﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace kdbs_prjDBOND
{
    public partial class w_Abrir : System.Web.UI.Page
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
        protected clsIngreso objIngreso = new clsIngreso();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DateTime d_fechaFin = DateTime.Now;  //fehca del dia
                string s_fechaFin = d_fechaFin.ToString("yyyy-MM-dd");
                Session.Add("gs_FecFin", s_fechaFin);
                txtFecFin.Text = s_fechaFin;

                DateTime d_fechaInicio = DateTime.Now;
                string s_fechaInicio = d_fechaInicio.ToString("yyyy-MM-dd");
                Session.Add("gs_FecIni", s_fechaInicio);
                txtFecIni.Text = s_fechaInicio;

                Session["gs_Numfac"] = null; //para resetear el ingreso y q puedan seleccionar otro nuevo
                Session["gs_Observ"] = null;
                Session["gs_Existe"] = null;
                if (Session["gs_CodUs1"] != null)
                {
                    lblUsuario.Text = Session["gs_CodUs1"].ToString();
                    lblUsuarioDrop.Text = Session["gs_CodUs1"].ToString();
                }
                else
                {
                    Response.Redirect("w_Login.aspx");
                }
            }
            if (IsPostBack)
            {
                Session["gs_FecIni"] = txtFecIni.Text;
                Session["gs_FecFin"] = txtFecFin.Text;
            }
        }

        protected void grvCrmTarea_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow fila = grvCrmTarea.SelectedRow;
            Label lblgrvNumfac = fila.FindControl("lblgrvNumfac") as Label;
            Label lblgrvObserv = fila.FindControl("lblgrvObserv") as Label;
            //string s_numfac = fila.Cells[2].Text.Trim();
            if (lblgrvNumfac != null)
            {
                string s_numfac = lblgrvNumfac.Text;

                Session.Add("gs_Numfac", s_numfac.Trim()); //variable para que al seleccionar uno en el grv solo se añada ese a la dt
                if (lblgrvObserv !=null)
                {
                    Session.Add("gs_Observ", lblgrvObserv.Text);
                }
                Session.Add("gs_Existe", "SI");
                DataSourceSelectArguments args = new DataSourceSelectArguments(); //para pasar del SqlDataSource1 a una DataTable
                DataView view = (DataView)sqldsCrmTareaFull.Select(args); //para pasar del SqlDataSource1 a una DataTable
                DataTable dt = view.ToTable(); //en esta dataTable esta el pedido seleccionado... (con los datos de este se llenara el encabezadoegresos)

                string codpro = dt.Rows[0].Field<string>("codpro");
                Session.Add("gs_Codpro", codpro.Trim());
                string nompro = dt.Rows[0].Field<string>("nompro");
                Session.Add("gs_Nompro", nompro.Trim());
            }
            
            //string tittar = dt.Rows[0].Field<string>("tittar");
            //Session.Add("gs_Tittar", tittar.Trim());
            //DateTime fectra = dt.Rows[0].Field<DateTime>("fectar");
            //Session.Add("gs_Fectar", fectra);
            //string numfac = dt.Rows[0].Field<string>("numfac");
            //Session.Add("gs_Numfac", numfac.Trim());

            //string est002 = "";
            //if (dt.Rows[0].Field<string>("est002") != null) //para saber si observ es NULL
            //{
            //    est002 = dt.Rows[0].Field<string>("est002");
            //}
            //Session.Add("gs_Est002", est002.Trim());


            ////PARA ACTUALIZAR COLORES DE GRVENCABEZADOPEDPRO (AMARILLO)
            //clsError objError = objIngreso.f_actualizarColoresGrv(Session["gs_CodEmp"].ToString(), numfac,
            //    "1", "", Session["gs_CodUs1"].ToString());
            //if (String.IsNullOrEmpty(objError.Mensaje))
            //    Response.Redirect("w_TomaFisica.aspx");
            //else
            //    lblError.Text = objError.f_ErrorNuevo("*01.", objError);

            Response.Redirect("w_TomaManual.aspx");
        }

        //protected void grvCrmTarea_RowDataBound(object sender, GridViewRowEventArgs e)
        //{

        //    //para actualizar el color de las filas de grvEncabezadorpedpro
        //    foreach (GridViewRow row in grvCrmTarea.Rows)
        //    {
        //        if (row.Cells[6].Text.Trim().Equals("0")) //rojo
        //        {
        //            row.BackColor = Color.LightCoral;
        //        }
        //        else
        //        {
        //            row.BackColor = Color.Khaki; //amarillo
        //            if (row.Cells[7].Text.Trim().Equals("1")) //verde
        //            {
        //                row.BackColor = Color.LightGreen;
        //            }
        //            else
        //            {
        //                if (row.Cells[7].Text.Trim().Equals("2")) //lila
        //                {
        //                    row.BackColor = Color.Plum;
        //                }
        //                else
        //                {
        //                    if (row.Cells[7].Text.Trim().Equals("3")) //azul
        //                    {
        //                        row.BackColor = Color.DodgerBlue;
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    e.Row.Cells[6].Visible = false;
        //    e.Row.Cells[7].Visible = false;
        //}
    }
}