﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace kdbs_prjDBOND
{
    public partial class w_DespachoListado : System.Web.UI.Page
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
        protected clsPedpro objPedpro = new clsPedpro();
        protected void Page_Load(object sender, EventArgs e)
        {
            //OJO TODOS LOS PRECEDIMIENTOS DE ESTA APLICACION ESTAN GUARDADOS COMO W_JEP_AAA
            //SE UTILIZA LA BASE KDBS_ESPERANZA
            if (!IsPostBack)
            {
                if (Session["gs_CodUs1"]!=null)
                {
                    lblUsuario.Text = Session["gs_CodUs1"].ToString();
                    lblUsuarioDrop.Text = Session["gs_CodUs1"].ToString();
                }
                else
                {
                    Response.Redirect("w_Login.aspx");
                }
            }
        }        

        protected void grvEncabezadopedpro_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow fila = grvEncabezadopedpro.SelectedRow;
            string s_numtra = fila.Cells[2].Text.Trim();
            Session.Add("gs_Numtra", s_numtra); //variable para que al seleccionar uno en el grv solo se añada ese a la dt

            DataSourceSelectArguments args = new DataSourceSelectArguments(); //para pasar del SqlDataSource1 a una DataTable
            DataView view = (DataView)sqldsEncPedproFull.Select(args);            //para pasar del SqlDataSource1 a una DataTable
            DataTable dt = view.ToTable(); //en esta dataTable esta el pedido seleccionado... (con los datos de este se llenara el encabezadoegresos)

            string codcli = dt.Rows[0].Field<string>("codcli");
            Session.Add("gs_Codcli", codcli.Trim());
            string nomcli = dt.Rows[0].Field<string>("nomcli");
            Session.Add("gs_Nomcli", nomcli.Trim());
            string codusu = dt.Rows[0].Field<string>("codusu");
            Session.Add("gs_Codusu", codusu.Trim());
            string tiptra = dt.Rows[0].Field<string>("tiptra");
            Session.Add("gs_Tiptra", tiptra.Trim());
            string codven = dt.Rows[0].Field<string>("codven");       
            Session.Add("gs_CodVen", codven.Trim());
            string observ = "";
            if (dt.Rows[0].Field<string>("observ") != null) //para saber si observ es NULL
            {
                observ = dt.Rows[0].Field<string>("observ");             
            }
            Session.Add("gs_ObservEncPed", observ.Trim());
            string est002 = "";
            if (dt.Rows[0].Field<string>("est002") != null) //para saber si observ es NULL
            {
                est002 = dt.Rows[0].Field<string>("est002");
            }
            Session.Add("gs_Est002", est002.Trim());


            //PARA ACTUALIZAR COLORES DE GRVENCABEZADOPEDPRO (AMARILLO)
            clsError objError = objPedpro.f_actualizarColoresGrv(Session["gs_CodEmp"].ToString(), s_numtra, 
                "1","",Session["gs_CodUs1"].ToString());
            if (String.IsNullOrEmpty(objError.Mensaje))
                Response.Redirect("w_Despacho.aspx");               
            else
                lblError.Text = objError.f_ErrorNuevo("*01.",objError);
        }

        protected void grvEncabezadopedpro_RowDataBound(object sender, GridViewRowEventArgs e)
        {            
            //para actualizar el color de las filas de grvEncabezadorpedpro
            foreach (GridViewRow row in grvEncabezadopedpro.Rows)
            {
                if (row.Cells[5].Text.Trim().Equals("0")) //rojo
                {
                    row.BackColor = Color.LightCoral;
                }
                else 
                {
                    row.BackColor = Color.Khaki; //amarillo
                    if (row.Cells[6].Text.Trim().Equals("1")) //verde
                    {
                        row.BackColor = Color.LightGreen;
                    }
                    else
                    {
                        if (row.Cells[6].Text.Trim().Equals("2")) //lila
                        {
                            row.BackColor = Color.Plum;
                        }
                        else
                        {
                            if (row.Cells[6].Text.Trim().Equals("3")) //azul
                            {
                                row.BackColor = Color.DodgerBlue;
                            }
                        }
                    }
                }
            }
            e.Row.Cells[5].Visible = false;
            e.Row.Cells[6].Visible = false;
        }
    }
}