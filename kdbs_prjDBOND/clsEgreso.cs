﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace kdbs_prjDBOND
{
    public class clsEgreso
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
        public clsError Error { get; set; }
        public clsError f_llenarEncabezadoEgresos(string codemp, string numfac, string codcli,
            string codalm, string codven, string codusu, string nomcli, string tiptra, string numtra,
            string sersec, string codsuc, string usuing, string observ, string est002)
        {
            clsSecuencia objSecuencia = new clsSecuencia();
            clsError objError = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_M_EGRESOS_ENC]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codemp", codemp);
            cmd.Parameters.AddWithValue("@numfac", numfac);
            cmd.Parameters.AddWithValue("@codcli", codcli);
            cmd.Parameters.AddWithValue("@codalm", codalm);
            cmd.Parameters.AddWithValue("@codven", codven);
            cmd.Parameters.AddWithValue("@codusu", codusu);
            cmd.Parameters.AddWithValue("@nomcli", nomcli);
            cmd.Parameters.AddWithValue("@tiptra", tiptra);
            cmd.Parameters.AddWithValue("@numtra", numtra);
            cmd.Parameters.AddWithValue("@sersec", sersec);
            cmd.Parameters.AddWithValue("@codsuc", codsuc);
            cmd.Parameters.AddWithValue("@usuing", usuing);
            cmd.Parameters.AddWithValue("@observ", observ);
            cmd.Parameters.AddWithValue("@est002", est002);

            try
            {
                cmd.ExecuteNonQuery();
                conn.Close();
                //actualiza la secuencia
                objSecuencia.f_ActualizarSecuencia(codemp, sersec, "VC_EGR");
                if (!String.IsNullOrEmpty(objSecuencia.Error.Mensaje))
                {
                    objError.Mensaje = objSecuencia.Error.f_ErrorNuevo("*12.", objSecuencia.Error);
                }                
            }
            catch (Exception ex)
            {
                conn.Close();
                objError = objError.f_ErrorControlado(ex);
                //lblError.Text = "*03. " + DateTime.Now + " " + ex.Message;
            }
            
            return objError; //se inserto correctamente
        }

        public clsError f_llenarRenglonesEgresos(string codart1, string codemp, string numfac, long numren1,
            long numite1, string nomart1, string coduni1, decimal preuni1, decimal cantid1,
            decimal canfac, string observ, string codalm, string numtra)
        {
            clsError objError = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_M_EGRESOS_REN]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codart", codart1);
            cmd.Parameters.AddWithValue("@codemp", codemp);
            cmd.Parameters.AddWithValue("@numfac", numfac);
            cmd.Parameters.AddWithValue("@numren", numren1);
            cmd.Parameters.AddWithValue("@numite", numite1);
            cmd.Parameters.AddWithValue("@nomart", nomart1);
            cmd.Parameters.AddWithValue("@coduni", coduni1);
            cmd.Parameters.AddWithValue("@cantid", cantid1);
            cmd.Parameters.AddWithValue("@preuni", preuni1);
            //cmd.Parameters.AddWithValue("@ubifis", ubifis1);
            cmd.Parameters.AddWithValue("@codalm", codalm);
            cmd.Parameters.AddWithValue("@numtra", numtra);
            cmd.Parameters.AddWithValue("@canfac", canfac);
            cmd.Parameters.AddWithValue("@observ", observ);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
                //lblError.Text = "*04. " + DateTime.Now + " " + ex.Message;
            }
            conn.Close();
            return objError;
        }

        public clsError f_llenarKardex(string codemp1, string numfac, string codart1, long numren1, string coduni1, string codalm,
             decimal cantid1, string codcli, string codven, string codusu, string nomcli, string tiptra, string numtra,
             string sersec, string codsuc, decimal totven, decimal preuni1)
        {
            clsError objError = new clsError();
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_M_KARDEX]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codemp", codemp1);
            cmd.Parameters.AddWithValue("@numdoc", numfac);
            cmd.Parameters.AddWithValue("@codart", codart1);
            cmd.Parameters.AddWithValue("@numren", numren1);
            cmd.Parameters.AddWithValue("@coduni", coduni1);
            cmd.Parameters.AddWithValue("@codalm", codalm);
            cmd.Parameters.AddWithValue("@cantid", cantid1);
            cmd.Parameters.AddWithValue("@codcli", codcli);
            cmd.Parameters.AddWithValue("@codven", codven);
            cmd.Parameters.AddWithValue("@codusu", codusu);
            cmd.Parameters.AddWithValue("@nomcli", nomcli);
            cmd.Parameters.AddWithValue("@tiptra", tiptra);
            cmd.Parameters.AddWithValue("@numtra", numtra);
            cmd.Parameters.AddWithValue("@sersec", sersec);
            cmd.Parameters.AddWithValue("@codsuc", codsuc);
            cmd.Parameters.AddWithValue("@totven", totven);
            cmd.Parameters.AddWithValue("@preuni", preuni1);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                objError = objError.f_ErrorControlado(ex);
                //lblError.Text = "*05. " + DateTime.Now + " " + ex.Message;
            }
            conn.Close();
            return objError;
        }
    }
}