﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


//LAST ERROR CODE: *03.

namespace kdbs_prjDBOND
{
    public partial class w_TomaFisica1 : System.Web.UI.Page
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
        protected clsIngreso objIngreso = new clsIngreso();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["gs_CodUs1"] != null)
                {
                    lblUsuario.Text = Session["gs_CodUs1"].ToString();
                    lblUsuarioDrop.Text = Session["gs_CodUs1"].ToString();
                }
                else
                {
                    Response.Redirect("w_Login.aspx");
                }

                Session.Add("gs_ContVal", "0"); //para que al aplastar muchas veces solo se haga 1
                txtFecegr.Text = DateTime.Now.ToString("yyyy-MM-dd");
                ////SECUENCIA N° EGR
                //txtNumegr.Text = f_CalcularSecuencia();
                txtNumegr.Text = Session["gs_Numfac"].ToString();
                //txtTittar.Text = Session["gs_Tittar"].ToString();
                txtNumtar.Text = Session["gs_Numtar"].ToString();

                //PARA LLENAR grvRenglonespedpro
                DataSourceSelectArguments args = new DataSourceSelectArguments(); //para pasar del SqlDataSource1 a una DataTable
                DataView view = (DataView)sqldsRenglonesIng.Select(args);            //para pasar del SqlDataSource1 a una DataTable
                DataTable dt = view.ToTable(); //en esta dataTable esta el pedido seleccionado... (con los datos de este se llenara el encabezadoegresos)
                //para agregar saldo (cantid original)
                dt.Columns.Add("saldo", typeof(decimal));
                foreach (DataRow row in dt.Rows)
                {
                    row["saldo"] = row.Field<decimal>("cantid"); //guarda el total de cada articulo en columna saldo

                    if (!DBNull.Value.Equals(row["canfac"])) //para saber si canfac es NULL
                    {
                        row["cantid"] = row.Field<decimal>("canfac"); //si han hecho despacho parcial el valor de canfac debe guardarse en cantid
                        row["saldo"] = row["cantid"];
                    }
                }

                Session.Add("gdt_Articulos", dt);
                grvRenglonesIng.DataSource = dt;
                grvRenglonesIng.DataBind();

                //la cantid puede ser 0 cuando hayan realizado un despacho parcial
                //para esto se llena el chk y se pinta de verde apenaz ingrese a Despacho.aspx
                int rowIndex = 0;
                foreach (GridViewRow row in grvRenglonesIng.Rows)
                {
                    if ((int)decimal.Parse(row.Cells[2].Text.Trim()) > 0) //llena el chkConfirmar
                    {
                        //CheckBox rBoton = (CheckBox)row.Cells[4].FindControl("chkConfirmado");
                        //rBoton.Checked = true;
                        row.BackColor = Color.LightGreen;
                    }

                    //para poner color azul en caso de que haya sido grabado pendiente
                    string estdes = "";
                    if (!DBNull.Value.Equals(dt.Rows[rowIndex]["estdes"])) //si estdes no es NULL
                    {
                        estdes = dt.Rows[rowIndex].Field<string>("estdes");
                    }

                    if (estdes.Equals("1"))
                    {
                        row.BackColor = Color.DodgerBlue;
                    }

                    //PARA MOSTRAR EL CODIGO ALTERNO EN LUGAR DE CODART....SOLO SI NO ES NULL
                    //if (row.Cells[5].Text != "&nbsp;") //para saber si desart es NULL
                    //{
                    //    string s_aux = row.Cells[1].Text; //PARA PASAR desart A Cells[1] y codart  A Cells[5]
                    //    row.Cells[1].Text = row.Cells[5].Text; //Muestra desart en Código
                    //    row.Cells[5].Text = s_aux;
                    //}
                    rowIndex++;
                }
                //grvRenglonespedpro.Columns[5].Visible = false; //esconde la columna de desart
                
            }

            if (IsPostBack)
            {
                if (!txtBuscar.Text.Equals(""))
                {
                    string codart = txtBuscar.Text.Trim();
                    Session.Add("gs_Codart", codart);
                    if (!codart.Equals(""))
                    {
                        clsArticulo objArticulo = f_BuscarArticulo(codart);
                        f_ActualizarCampos(objArticulo);
                        txtBuscar.Focus();
                    }
                }
            }
        }

        public int f_CanTot() //PARA CALCULAR CANTID TOTAL DE grvRenglonespedpro...(PARA VALIDACION DE btnGuardar)
        {
            //1 -->PARCIAL (ALGUNO TIENE CANTID 0)
            //0 -->COMPLETO(NINGUNO ESTA CON CANTID 0)
            int i_cantot = 0;
            foreach (GridViewRow row in grvRenglonesIng.Rows)
            {
                if (decimal.Parse(row.Cells[2].Text.Trim())==0)
                {
                    i_cantot = 1;
                }
                //i_cantot = i_cantot + (int)decimal.Parse(row.Cells[2].Text.Trim());
            }
            return i_cantot;
        }


        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            if (f_CanTot() == 0) //verifica si todos los cantid estan en 0....REALIZO PARCIAL PERO TODOS ESTABAN EN CERO....SE PONE EN VERDE
            {
                if (Session["gs_ContVal"].ToString().Equals("0")) //para que al aplastar muchas veces solo se haga 1
                {
                    Session["gs_ContVal"] = "1";
                    f_insertarArticulos("btnGuardar", "1"); //est002=1 ...para q wladi sepa si fue parcial o completo

                    //PARA ACTUALIZAR COLORES DE GRVENCABEZADOPEDPRO (VERDE)
                    clsError objError = new clsError();
                    if (lblError.Text.Trim() == "") //control de errores
                    {
                        objError = objIngreso.f_actualizarColoresGrv(Session["gs_CodEmp"].ToString(), Session["gs_Numfac"].ToString(),
                            "1", "1", Session["gs_CodUs1"].ToString());
                        if (!String.IsNullOrEmpty(objError.Mensaje))
                            lblError.Text = objError.f_ErrorNuevo("*07.", objError);
                    }

                    //redireccion despues de guardar
                    if (lblError.Text.Trim() == "") //control de errores
                    {
                        Response.Redirect("w_TomaFisicaListado.aspx");
                    }
                }
            }
            else //GRABADO PARCIAL NORMAL
            {
                btnGuardarParcial_Click();
            }
        }

        protected void btnAceptar_Click(object sender, EventArgs e) //para guardarParcial
        {
            if (Session["gs_ContVal"].ToString().Equals("0")) //para que al aplastar muchas veces solo se haga 1
            {
                Session["gs_ContVal"] = "1";
                f_insertarArticulos("btnAceptar", "2"); //est002=2 ...para q wladi sepa si fue parcial o completo

                //PARA ACTUALIZAR COLORES DE GRVENCABEZADOPEDPRO (LILA)
                clsError objError = new clsError();
                if (lblError.Text.Trim() == "") //control de errores
                {
                    objError = objIngreso.f_actualizarColoresGrv(Session["gs_CodEmp"].ToString(), Session["gs_Numfac"].ToString(),
                        "1", "2", Session["gs_CodUs1"].ToString());
                    if (!String.IsNullOrEmpty(objError.Mensaje))
                        lblError.Text = objError.f_ErrorNuevo("*07.", objError);
                }

                //redireccion despues de guardar
                if (lblError.Text.Trim() == "") //control de errores
                {
                    Response.Redirect("w_TomaFisicaListado.aspx");
                }
            }            
        }

        protected void btnGuardarParcial_Click()
        {
            lblAlertaModal.Text = "";
            int rowIndex = 0;
            DataTable dtObservacionArticulos = (DataTable)Session["gdt_Articulos"];

            //PARA MOSTRAR ROW DE LOS ARTICULOS QUE NO SE VEN A INSERTAR (SU CANTID NO ES CERO)
            //for (int i = 0; i < dtObservacionArticulos.Rows.Count; i++)
            //{

            //    if (cantidadDes == 0) //si sobran articulos los muestra (si su cantidadDes es 0)
            //    {
            //        dtObservacionArticulos.Rows.RemoveAt(i);
            //    }
            //}

            grvObservacionArticulos.DataSource = dtObservacionArticulos;
            grvObservacionArticulos.DataBind();

            foreach (GridViewRow row in grvObservacionArticulos.Rows)
            {

                //PARA MOSTRAR ROW DE LOS ARTICULOS QUE NO SE VEN A INSERTAR (SU CANTID ES CERO)
                decimal cantidadDes = dtObservacionArticulos.Rows[rowIndex].Field<decimal>("cantid"); //cantidad de total de articulos menos los q se despacho (canfac)
                //decimal saldo = dt.Rows[rowIndex].Field<decimal>("saldo"); //cantidad total de articulo con el que se empezo
                //decimal cantid1 = saldo - cantidadDes; //articulos despachados

                if (cantidadDes == 0) // muestra (si su cantidadDes  es 0)
                {
                    grvObservacionArticulos.Rows[rowIndex].Visible = true;
                }
                else
                {
                    grvObservacionArticulos.Rows[rowIndex].Visible = false;
                }
                rowIndex++;
            }

            ClientScript.RegisterStartupScript(this.GetType(), "Popup", "$('#modalObservacion').modal('show')", true);
            //de aqui va al btnAceptar
        }

        public void f_insertarArticulos(string s_tipoGuargar, string est002) //metodo para insertar enc y ren egresos
        {
            lblError.Text = "";
            string codemp = Session["gs_CodEmp"].ToString();
            string numfac = Session["gs_Numfac"].ToString();
            string usuing = Session["gs_CodUs1"].ToString();

            string s_enc = "";

            if (s_enc.Equals("")) //si no hubo error al insertar en el encabezado
            {
                DataTable dtArticulos = (DataTable)Session["gdt_Articulos"];
                int rowIndex = 0; //para enconctrar ddl en grvObservacion   
                clsError objError = new clsError();
                foreach (DataRow row in dtArticulos.Rows)
                {
                    Session.Add("gs_Codart", row.Field<string>("codart"));
                    string codart1 = row.Field<string>("codart");
                    //string nomart1 = row.Field<string>("nomart");
                    string observ1 = "";
                    string estdes1 = "";
                    if (!DBNull.Value.Equals(row["estdes"])) //para saber si estdes es NULL
                    {
                        estdes1 = row.Field<string>("estdes");
                    }

                    decimal cantidadDes = row.Field<decimal>("cantid"); //cantidad de total de articulos menos los q se despacho (canfac)
                    decimal saldo = row.Field<decimal>("saldo"); //cantidad total de articulo con el que se empezo
                    //decimal cantid1 = saldo - cantidadDes; //articulos despachados
                    decimal cantid1 = cantidadDes; //articulos despachados

                    decimal candes1 = 0;
                    if (!DBNull.Value.Equals(row["candes"]))
                    {
                        candes1 = row.Field<decimal>("candes");//cantidad guardada en campo candes al hacer grabar pendiente
                    }

                    //***para obtener observ de grvObservacion al hacer clic en btnAceptar
                    if (s_tipoGuargar.Equals("btnAceptar")) //si se hizo clic en btnAceptar
                    {
                        DropDownList ddlgrvObserv = (DropDownList)grvObservacionArticulos.Rows[rowIndex].Cells[3].FindControl("ddlgrvObservacion");
                        if (cantidadDes == 0) //muestra (si su cantidadDes  es 0)
                        {
                            observ1 = ddlgrvObserv.SelectedValue.ToString().Trim();
                        }
                    }
                        

                    if (cantid1 != 0) //si no toco al articulo, no lo inserta en la tabla (si su cantid1 es 0)
                    {
                        //if (candes1 != 0) //si hace despacho parcial en un articulo que estaba con grabar pendiente
                        //{
                        //    cantid1 = cantid1 + candes1;
                        //}
                        if (lblError.Text.Trim() == "") //control de errores
                        {
                            objError = objIngreso.f_actualizarRenglonesIngresos(codart1, codemp, numfac, cantid1, cantidadDes, observ1);
                            if (!String.IsNullOrEmpty(objError.Mensaje))
                                lblError.Text = objError.f_ErrorNuevo("*04.", objError);
                        }
                    }
                    else
                    {
                        if (estdes1.Equals("1") && candes1 != 0) // si fue grabado pendiente
                        {
                            if (lblError.Text.Trim() == "") //control de errores
                            {
                                objError = objIngreso.f_actualizarRenglonesIngresos(codart1, codemp, numfac, candes1, cantidadDes, observ1);
                                if (!String.IsNullOrEmpty(objError.Mensaje))
                                    lblError.Text = objError.f_ErrorNuevo("*04.", objError);
                            }
                        }
                        else //actualizar la observacion
                        {
                            if (lblError.Text.Trim() == "" && !(observ1.Equals(""))) //control de errores
                            {
                                objError = objIngreso.f_actualizarObsev(codart1, codemp, numfac, observ1); //para actualizar las observ
                                if (!String.IsNullOrEmpty(objError.Mensaje))
                                    lblError.Text = objError.f_ErrorNuevo("*14.", objError);
                            }
                        }
                    }
                    rowIndex++;

                    //if (lblError.Text.Trim() == "" && !(observ1.Equals(""))) //control de errores
                    //{
                    //    f_actualizarObsev(codart1, codemp, numtra, observ1); //para actualizar las observ
                    //}
                }
            }
        }        

        
        public clsArticulo f_BuscarArticulo(string s_codart)
        {
            clsArticulo objArticulo = new clsArticulo();
            DataTable dt = (DataTable)Session["gdt_Articulos"];

            try
            {
                foreach (DataRow row in dt.Rows)
                {
                    if (row.Field<string>("codart").Trim().Equals(s_codart.Trim()))
                    {
                        objArticulo.Codart = s_codart;
                        string s_nomart = row.Field<string>("nomart");
                        objArticulo.Nomart = s_nomart;
                        int i_cantid = (int)row.Field<decimal>("cantid") + 1;

                        //actualiza el dt
                        row["cantid"] = i_cantid;

                        objArticulo.Cantid = i_cantid;
                        objArticulo.Error = "CORRECTO";

                        Session["gdt_Articulos"] = dt;
                        //grvRenglonespedpro.Columns[5].Visible = true; //para q f_MostrarCodAlterno() funcione
                        grvRenglonesIng.DataSource = dt;
                        grvRenglonesIng.DataBind();

                        //f_MostrarCodAlterno(); //PARA MOSTRAR CODIGO ALTERNO
                        f_pintarGridview(s_codart, i_cantid);
                        //grvRenglonespedpro.Columns[5].Visible = false; //ocultar columna extra...DEBE IR AQUI PARA QUE COLORES DE GRV FUNCIONEN
                        Session["gs_ArtDespachado"] = "1"; //para saber que ha despachado por lo menos 1 articulo..se usa en f_insertarArticulos
                        return objArticulo;
                    }
                }
            }
            catch (Exception ex)
            {
                objArticulo.Codart = "-";
                objArticulo.Nomart = "-";
                objArticulo.Cantid = 0;
                objArticulo.Error = "*02. " + DateTime.Now + " " + ex.Message;
                return objArticulo;
            }

            //if (s_codart.Any(char.IsDigit)) //si contiene por lo menos 1 numero (pistoleo el codigo....todo se hace automático)
            //{
                
            //}
            //else // si busco por nombre en txtBuscar
            //{
            //    Session.Add("gs_Nomart", s_codart.Trim());
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ModalView", "<script>$(function() { $('#modalBuscar').modal('show'); });</script>", false);
            //    objArticulo.Codart = "-";
            //    objArticulo.Nomart = "-";
            //    objArticulo.Cantid = 0;
            //    objArticulo.Error = "";
            //    return objArticulo;
            //}

            objArticulo.Codart = "-";
            objArticulo.Nomart = "-";
            objArticulo.Cantid = 0;
            objArticulo.Error = "*03. " + DateTime.Now + " " + " No se encontró artículo.";
            return objArticulo;
        }

        public void f_pintarGridview(string s_codart, int i_cantid) //pinta y llena el chkConfirmar
        {
            foreach (GridViewRow row in grvRenglonesIng.Rows)
            {

                //if ((int)decimal.Parse(row.Cells[2].Text.Trim()) == 0) //llena el chkConfirmar
                //{
                //    CheckBox rBoton = (CheckBox)row.Cells[4].FindControl("chkConfirmado");
                //    rBoton.Checked = true;
                //}
                string codart1 = row.Cells[1].Text.Trim();
                //string alterno = row.Cells[5].Text.Trim();

                if (row.Cells[1].Text.Trim().Equals(s_codart.Trim())) //pinta la fila con codart
                {
                    if (i_cantid > 0)
                        row.BackColor = Color.LightGreen;
                    else
                        row.BackColor = Color.Khaki;
                }
            }
        }

        protected void btnDespachar_Click(object sender, EventArgs e)
        {
            clsArticulo objArticulo = new clsArticulo();
            try
            {
                objArticulo.Codart = txtCodart.Text;
                objArticulo.Nomart = txtNomart.Text;
                objArticulo.Cantid = (int)decimal.Parse(txtSaldo.Text);
                objArticulo.Error = "";
                DataTable dt = (DataTable)Session["gdt_Articulos"];
                string s_codart = txtCodart.Text.Trim();

                int i_cantidDesapachar = 1;
                if (!txtCantid.Text.Equals("")) //si deja vacio txtCantid se asume que es 1
                {
                    i_cantidDesapachar = int.Parse(txtCantid.Text.ToString());
                }

                foreach (DataRow row in dt.Rows)
                {
                    if (row.Field<string>("codart").Trim().Equals(s_codart.Trim()))
                    {
                        //if ((int)row.Field<decimal>("cantid") < i_cantidDesapachar) //si ingresa cantidad mayor que saldo existente
                        //{
                        //    txtCantid.Text = "1";
                        //    objArticulo.Error = "*10. Cantidad ingresada es superior a Saldo. Despacho no realizado.";
                        //    f_ActualizarCampos(objArticulo);
                        //}
                        //else //cantidad correcta
                        //{
                            
                        //}

                        objArticulo.Codart = s_codart;
                        string s_nomart = row.Field<string>("nomart");
                        objArticulo.Nomart = s_nomart;
                        int i_cantid = (int)row.Field<decimal>("cantid") + i_cantidDesapachar;

                        //actualiza el dt
                        row["cantid"] = i_cantid;

                        objArticulo.Cantid = i_cantid;
                        objArticulo.Error = "CORRECTO";

                        Session["gdt_Articulos"] = dt;
                        //grvRenglonespedpro.Columns[5].Visible = true; //para q f_MostrarCodAlterno() funcione
                        grvRenglonesIng.DataSource = dt;
                        grvRenglonesIng.DataBind();

                        //f_MostrarCodAlterno(); //PARA MOSTRAR CODIGO ALTERNO
                        //f_pintarGridview(s_codart, i_cantid, s_desart);
                        //grvRenglonespedpro.Columns[5].Visible = false; //ocultar columna extra...DEBE IR AQUI PARA QUE COLORES DE GRV FUNCIONEN
                        f_ActualizarCampos(objArticulo);
                    }
                }
                //aqui poner objArticulo.Codart = "-"; .....
            }
            catch (Exception ex)
            {
                objArticulo.Codart = "-";
                objArticulo.Nomart = "-";
                objArticulo.Cantid = 0;
                objArticulo.Error = "*11. " + DateTime.Now + " " + ex.Message;
                f_ActualizarCampos(objArticulo);
            }
        }

        public void f_ActualizarCampos(clsArticulo objArticulo)
        {
            txtCodart.Text = objArticulo.Codart;
            txtNomart.Text = objArticulo.Nomart;
            txtSaldo.Text = objArticulo.Cantid.ToString();
            if (!objArticulo.Error.Equals("CORRECTO"))
            {
                lblError.Text = "*01. " + DateTime.Now + objArticulo.Error;
            }
            else
            {
                lblError.Text = "";
            }
            txtBuscar.Text = "";
        }

        protected void btnPendiente_Click(object sender, EventArgs e) //para que despachen cierta cantidad y se guarde al volver a ingresar (NO INSERTA EN TBLS...solo hace update)
        {
            string codemp = Session["gs_CodEmp"].ToString();
            string numfac = Session["gs_Numfac"].ToString();

            //PARA ACTUALIZAR COLORES DE GRVENCABEZADOPEDPRO (AZUL)
            clsError objError = new clsError();
            if (lblError.Text.Trim() == "") //control de errores
            {
                objError = objIngreso.f_actualizarColoresGrv(codemp, numfac,
                    "1", "3", Session["gs_CodUs1"].ToString());
                if (!String.IsNullOrEmpty(objError.Mensaje))
                    lblError.Text = objError.f_ErrorNuevo("*07.", objError);
            }

            DataTable dtArticulos = (DataTable)Session["gdt_Articulos"];
            foreach (DataRow row in dtArticulos.Rows)
            {
                Session.Add("gs_Codart", row.Field<string>("codart"));
                string codart1 = row.Field<string>("codart");
                //string nomart1 = row.Field<string>("nomart");
                //string coduni1 = row.Field<string>("coduni");
                //decimal preuni1 = row.Field<decimal>("preuni");  

                decimal cantidadDes = row.Field<decimal>("cantid"); //cantidad de total de articulos menos los q se despacho (canfac)
                decimal saldo = row.Field<decimal>("saldo"); //cantidad total de articulo con el que se empezo
                //decimal cantid1 = saldo + cantidadDes; //articulos despachados
                decimal cantid1 = cantidadDes; //articulos despachados

                //NUEVO CODIGO 20200217 PARA QUE AL GRABAR PENDIENTE 2 VECES EL MISMO ARTICULO SUME LAS CANDES
                decimal candes1 = 0;
                if (!DBNull.Value.Equals(row["candes"]))
                {
                    candes1 = row.Field<decimal>("candes");//cantidad guardada en campo candes al hacer grabar pendiente
                }

                if (cantid1 != 0) //si no toco al articulo, no le modifica su estdes ni su canfac
                {
                    //if (candes1 != 0) //si hace grabar pendiente en un articulo que estaba con grabar pendiente
                    //{
                    //    cantid1 = cantid1 + candes1;
                    //}

                    if (lblError.Text.Trim() == "") //control de errores
                    {
                        objError = objIngreso.f_actualizarRenglonesIngPar(codart1, codemp, numfac, cantidadDes, cantid1);
                        if (!String.IsNullOrEmpty(objError.Mensaje))
                            lblError.Text = objError.f_ErrorNuevo("*13.", objError);
                    }
                }
            }

            if (lblError.Text.Trim() == "") //control de errores
            {
                Response.Redirect("w_TomaFisicaListado.aspx"); //ojo manejar error
            }
        }        
    }
}