﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="w_TomaManual.aspx.cs" Inherits="kdbs_prjDBOND.w_TomaManual" %>

<%@ Register Src="~/WebMenuUserControl.ascx" TagPrefix="uc1" TagName="WebMenuUserControl" %>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <title>Kohinor Móvil - DBOND</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">
    <link href="css/StyleSheet1.css" rel="stylesheet" />
    <script src="js/jquery-3.4.1.min.js"></script>

    <script type="text/javascript">        
        $(document).ready(function () {
            if (window.innerWidth <= 640) {
                smallControls();
            }
        });

        $(window).resize(function () {
            if (window.innerWidth <= 640) {
                smallControls();
            } else {
                largeControls();
            }
        });

        function smallControls() {
            $("#<%=txtObserv.ClientID%>").addClass('form-control-sm');
            $("#<%=txtCodart.ClientID%>").addClass('form-control-sm');
            $("#<%=txtNomart.ClientID%>").addClass('form-control-sm');
            $('#input-cantid').addClass('input-group-sm');
            $('#input-saldo').addClass('input-group-sm');
            $("#<%=ddlCodalm.ClientID%>").addClass('form-control-sm');
            $('#input-numegr').addClass('input-group-sm');
            $('#input-fecegr').addClass('input-group-sm');
            $('#input-numtar').addClass('input-group-sm');
            $("#<%=txtCodpro.ClientID%>").addClass('form-control-sm');
            $("#<%=txtNompro.ClientID%>").addClass('form-control-sm');
            $("#<%=btnProveedores.ClientID%>").addClass('btn-sm');
        }

        function largeControls() {            
            $("#<%=txtObserv.ClientID%>").removeClass('form-control-sm');
            $("#<%=txtCodart.ClientID%>").removeClass('form-control-sm');
            $("#<%=txtNomart.ClientID%>").removeClass('form-control-sm');
            $('#input-cantid').removeClass('input-group-sm');
            $('#input-saldo').removeClass('input-group-sm');
            $("#<%=ddlCodalm.ClientID%>").removeClass('form-control-sm');
            $('#input-numegr').removeClass('input-group-sm');
            $('#input-fecegr').removeClass('input-group-sm');
            $('#input-numtar').removeClass('input-group-sm');
            $("#<%=txtCodpro.ClientID%>").removeClass('form-control-sm');
            $("#<%=txtNompro.ClientID%>").removeClass('form-control-sm');
            $("#<%=btnProveedores.ClientID%>").removeClass('btn-sm');
        }

    </script>
</head>

<body id="page-top">
    <form id="form1" runat="server">
        <%--SCRIPTS BODY--%>

        <%--SCRIPTS BODY--%>



        <!-- Page Wrapper -->
        <div id="wrapper">

            <!-- Sidebar -->
            <uc1:WebMenuUserControl runat="server" ID="WebMenuUserControl" />
            <!-- End of Sidebar -->

            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">

                <!-- Main Content -->
                <div id="content">

                    <!-- Topbar -->
                    <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                        <!-- Sidebar Toggle (Topbar) -->
                        <button onclick="return false;" id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                            <i class="fa fa-bars"></i>
                        </button>

                        <!-- Topbar Search -->

                        <div class="d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                            <div class="input-group">
                                <%--<input type="text" id="txtBuscar" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2" runat="server" AutoPostBack="True">--%>
                                <asp:TextBox ID="txtBuscar" runat="server" class="form-control bg-light border-0 small" placeholder="Buscar..." AutoPostBack="True"></asp:TextBox>
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="button">
                                        <i class="fas fa-search fa-sm"></i>
                                    </button>
                                </div>
                            </div>
                        </div>

                        <!-- Topbar Navbar -->
                        <ul class="navbar-nav ml-auto">

                            <div class="topbar-divider d-none d-sm-block"></div>

                            <!-- Nav Item - User Information -->
                            <li class="nav-item dropdown no-arrow">
                                <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="mr-2 d-none d-lg-inline text-gray-600 small">
                                        <asp:Label ID="lblUsuario" runat="server" Text=""></asp:Label>
                                    </span>
                                    <img class="img-profile rounded-circle" src="Imagenes/catUser.png">
                                </a>
                                <!-- Dropdown - User Information -->
                                <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                                    <asp:HyperLink ID="hpkUsuario" class="dropdown-item" runat="server">
                                        <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                        <asp:Label ID="lblUsuarioDrop" runat="server"></asp:Label>
                                    </asp:HyperLink>

                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                        <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                        Logout
                                    </a>
                                </div>
                            </li>

                        </ul>

                    </nav>
                    <!-- End of Topbar -->

                    <!-- Begin Page Content -->
                    <div class="container-fluid" style="display: none">

                        <!-- BAJO CONSTRUCCION Error Text -->
                        <div class="text-center">
                            <div class="error mx-auto" data-text="000">! ! !</div>
                            <p class="lead text-gray-800 mb-5">Página En Construcción</p>
                            <p class="text-gray-500 mb-0">Parece que la página que busca no esta lista...</p>
                        </div>
                    </div>


                    <div class="container-fluid">

                        <!-- Page Heading -->
                        <div class="d-sm-flex align-items-center justify-content-between page-heading-bottom">
                            <asp:Label ID="lblExito" runat="server" Text="" ForeColor="Green"></asp:Label>
                            <asp:Label ID="lblError" runat="server" Text="" ForeColor="Maroon"></asp:Label>
                            <%--<h1 class="h3 mb-0 text-gray-800"><strong>Toma Física</strong></h1>--%>
                            <%--<a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>--%>
                            <%--<asp:Button ID="btnGuardar" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" runat="server" Text="Guardar" OnClick="btnGuardar_Click" />--%>
                        </div>


                        <%--Seccion Textbox Toma Fisica--%>
                        <div class="row">

                            <!-- Area Chart -->
                            <div class="col">
                                <div class="card shadow mb-4">
                                    <!-- Card Header - Dropdown -->
                                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between card-header-xs">
                                        <h6 class="m-0 font-weight-bold text-primary">Toma Física Manual</h6>
                                        <div class="right">
                                            <button id="btnGuardar" class="btn btn-primary btn-sm btn-icon-split" runat="server" onserverclick="btnGuardar_Click">
                                                <span class="icon text-white-50">
                                                    <i class="fas fa-save"></i>
                                                </span>
                                                <span class="text">Guardar</span>
                                            </button>
                                        </div>
                                        <%--<div class="dropdown no-arrow">
                                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-ellipsis-v fa-sm fa-fw text-black-50"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                                                <div class="dropdown-header">Acciones:</div>
                                                <asp:Button ID="btnDespachar" class="dropdown-item" runat="server" Text="&#9997; Despachar" OnClick="btnDespachar_Click" />
                                                <div class="dropdown-divider"></div>
                                                <button id="btnGuardar" class="dropdown-item" runat="server" onserverclick="btnGuardar_Click"><i class="fas fa-save"></i> Guardar</button>
                                            </div>
                                        </div>--%>
                                    </div>
                                    <!-- Card Body -->
                                    <div class="card-body">
                                        <div style="width: 100%; overflow: hidden">
                                            <div id="alertExiste" class="alert alert-primary alert-dismissible fade show" runat="server" role="alert">
                                                    <i class="fas fa-info-circle"></i> <span id="alertIngresoText" runat="server"></span>
                                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                      <span aria-hidden="true">&times;</span>
                                                  </button>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg">
                                                    <b>
                                                        <center>Observación</center>
                                                    </b>
                                                    <asp:TextBox ID="txtObserv" class="form-control form-control-user" runat="server" Width="100%" TextMode="MultiLine"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg">
                                                    <b>
                                                        <center>-- Artículo --</center>
                                                    </b>
                                                    <asp:TextBox ID="txtCodart" class="form-control form-control-user" runat="server" placeholder="Código"></asp:TextBox>
                                                    <asp:TextBox ID="txtNomart" class="form-control form-control-user" runat="server" placeholder="Nombre"></asp:TextBox>
                                                    <div class="row" style="display:none;">
                                                        <div class="col-6">
                                                            <div class="input-group mb-3" id="input-cantid">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><strong>Cantidad</strong></span>
                                                                </div>
                                                                <asp:TextBox ID="txtCantid" class="form-control form-control-user" runat="server" TextMode="Number" placeholder="Cantidad"></asp:TextBox>
                                                            </div>

                                                        </div>
                                                        <div class="col-6">
                                                            <div class="input-group mb-3" id="input-saldo">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><strong>Existe</strong></span>
                                                                </div>
                                                                <asp:TextBox ID="txtSaldo" class="form-control form-control-user" runat="server" TextMode="Number" placeholder="Saldo" ReadOnly="true"></asp:TextBox>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <b>
                                                        <center>-- Almacén --</center>
                                                    </b>
                                                    <asp:DropDownList ID="ddlCodalm" runat="server" CssClass="form-control mb-2" DataTextField="nomalm" DataValueField="codalm"></asp:DropDownList>
                                                </div>
                                                <div class="col-lg">
                                                    <b>
                                                        <center>-- N° Ingreso --</center>
                                                    </b>
                                                    <div class="input-group" id="input-numegr">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><strong>N° Egreso</strong></span>
                                                        </div>
                                                        <asp:TextBox ID="txtNumegr" class="form-control form-control-user" runat="server" placeholder="N° Egreso" ReadOnly="true"></asp:TextBox>
                                                    </div>                                                    
                                                    <div class="input-group" id="input-fecegr">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><strong>Fecha</strong></span>
                                                        </div>
                                                        <asp:TextBox ID="txtFecegr" class="form-control form-control-user" runat="server" ReadOnly="true"></asp:TextBox>
                                                    </div>
                                                    <div class="input-group" id="input-numtar">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><strong>Total</strong></span>
                                                        </div>
                                                        <asp:TextBox ID="txtNumtar" class="form-control form-control-user" runat="server" placeholder="Numtar" ReadOnly="true"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="col-lg">
                                                    <b>
                                                        <center>-- Proveedor --</center>
                                                    </b>
                                                    <div class="row">
                                                        <div class="col-8" style="padding-right: 0rem;">
                                                            <asp:TextBox ID="txtCodpro" class="form-control form-control-user" runat="server" placeholder="Codpro" ReadOnly="true"></asp:TextBox>
                                                    <asp:TextBox ID="txtNompro" class="form-control form-control-user" runat="server" placeholder="Nompro" ReadOnly="true"></asp:TextBox>
                                                        </div>
                                                        <div class="col-4" style="padding-left: 0rem;">
                                                            <button ID="btnProveedores" class="btn btn-primary" style="width: 100%;height: 100%;" runat="server" onserverclick="btnProveedores_Click" >Buscar Proveedor</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <%--Seccion Gridview Toma Fisica--%>
                        <div class="row">

                            <!-- Area Chart -->
                            <div class="col">
                                <div class="card shadow mb-4">
                                    <!-- Card Header - Dropdown -->
                                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between card-header-xs">
                                        <h6 class="m-0 font-weight-bold text-primary">Listado Artículos</h6>

                                    </div>
                                    <!-- Card Body -->
                                    <style type="text/css">
                                        .table td, .table th {
                                            vertical-align: middle;
                                        }
                                    </style>
                                    <div class="card-body body-table-xs">
                                        <div style="width: 100%; overflow: scroll">
                                            <asp:GridView ID="grvRenglonesIng" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="True" class="table table-sm table-bordered table-active table-hover table-striped">

                                                <Columns>
                                                    <asp:TemplateField ItemStyle-Width="1%">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lkgrvBorrar" runat="server" class="text-primary bg-transparent small" OnClick="lkgrvBorrar_Click">
                                                                    <i class="fas fa-backspace fa-2x"></i>
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="N°" ItemStyle-Width="1%">
                                                        <ItemTemplate>
                                                            <%# Container.DataItemIndex + 1 %>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="1%"></ItemStyle>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Artículo">
                                                        <ItemTemplate>
                                                            <div class="row">
                                                                <div class="col">
                                                                    <span><strong><%# Eval("codart") %></strong></span>
                                                                </div>
                                                                <div class="col">
                                                                    <asp:TextBox ID="txtgrvCantid" class="form-control form-control-sm" style="float:right; width:70%;" runat="server" TextMode="Number" Text='<%# Eval("cantid","{0:n0}") %>'></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col">
                                                                    <span><small><%# Eval("nomart") %></small></span>
                                                                </div>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle BackColor="#5A5C69" Font-Bold="True" ForeColor="White" />
                                                <RowStyle BackColor="#D3E4F2" ForeColor="#333333" />                                                
                                                <AlternatingRowStyle BackColor="#F8FAFA" ForeColor="#284775" />
                                            </asp:GridView>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                    <!-- /.container-fluid -->

                </div>
                <!-- End of Main Content -->

                <!-- Footer -->
                <footer class="sticky-footer bg-white">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                            <span>Copyright &copy; KOHINOR 2021 Todos Los Derechos Reservados</span>
                        </div>
                    </div>
                </footer>
                <!-- End of Footer -->

            </div>
            <!-- End of Content Wrapper -->

        </div>
        <!-- End of Page Wrapper -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>

        <!-- Bootstrap core JavaScript-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Custom scripts for all pages-->
        <script src="js/sb-admin-2.min.js"></script>

        <!-- Page level plugins -->
        <script src="vendor/chart.js/Chart.min.js"></script>

        <!-- Page level custom scripts -->
        <script src="js/demo/chart-area-demo.js"></script>
        <script src="js/demo/chart-pie-demo.js"></script>

        <!--i MODALS -->

        <!--i Modal Proveedores -->
        <div id="modalProveedores" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Selección de Proveedor
                        </h4>
                        <button type="button" class="close" data-dismiss="modal">
                            &times;</button>
                    </div>
                    <div class="modal-body">
                        <!-- CONTENIDO-->

                        <div class="form-row">
                            <div class="input-group mb-3 input-group-sm">
                                <asp:TextBox ID="txtBuscarNompro" runat="server" class="form-control" placeholder="Nombre"></asp:TextBox>
                                <div class="input-group-append">
                                    <asp:Button ID="btnBuscarNompro" runat="server" Text="Buscar" class="btn btn-primary" OnClick="btnBuscarNompro_Click" />
                                </div>
                            </div>
                        </div>

                        <br />
                        <div style="width: 100%; height: 350px; overflow: scroll;">

                            <asp:GridView ID="grvProveedores" runat="server" class="table table-sm table-bordered table-active table-hover table-striped" AutoGenerateColumns="False" DataSourceID="sqlProveedores" OnSelectedIndexChanged="grvProveedores_SelectedIndexChanged">

                                <AlternatingRowStyle BackColor="#F8FAFA" ForeColor="#284775" />

                                <Columns>
                                    <asp:CommandField ButtonType="Image" SelectImageUrl="~/Imagenes/Select-Hand.png" ShowSelectButton="True">
                                        <ControlStyle Height="30px" />
                                        <FooterStyle Height="30px" />
                                        <HeaderStyle Height="30px" />
                                        <ItemStyle Height="30px" />
                                    </asp:CommandField>

                                    <asp:BoundField DataField="codpro" HeaderText="Código" SortExpression="codpro" />
                                    <asp:BoundField DataField="nompro" HeaderText="Proveedor" SortExpression="nompro" />
                                    <asp:BoundField DataField="tipind" HeaderText="Identificación" SortExpression="tipind" />

                                </Columns>

                                <HeaderStyle BackColor="#00445e" Font-Bold="True" ForeColor="White" />
                                <RowStyle BackColor="#D3E4F2" ForeColor="#333333" />
                                <AlternatingRowStyle BackColor="#F8FAFA" ForeColor="#284775" />
                            </asp:GridView>

                        </div>
                    </div>
                </div>
            </div>

        </div>

        <!--f Modal Proveedores -->

        <!--i Logout Modal-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">¿Desea salir?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Seleccionar "Logout" abajo para salir de la sesión actual.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                        <a class="btn btn-primary" href="w_Login.aspx">Logout</a>
                    </div>
                </div>
            </div>
        </div>
        <!--f Logout Modal-->

        <!--i Modal Observacion Popup -->
        <%--<div id="modalObservacion" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" >
                    <h4 class="modal-title" >
                        Observación Toma Física
                    </h4>
                    <button type="button" class="close" data-dismiss="modal">
                        &times;</button>                
                </div>
                <div class="modal-body">
                    <!-- CONTENIDO-->                  
                    <div class="form-row">
                        <div style="width: 100%; overflow: scroll">
                            <asp:GridView ID="grvObservacionArticulos" runat="server" class="table table-sm table-dark" AutoGenerateColumns="False">
                                <AlternatingRowStyle BackColor="#F8FAFA" ForeColor="#284775" />
                                <Columns>
                                    <asp:TemplateField HeaderText="N°" ItemStyle-Width="1%">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                        <ItemStyle Width="1%"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="codart" HeaderText="Código" SortExpression="codart" />
                                    <asp:BoundField DataField="cantid" HeaderText="Cantid" SortExpression="cantid" DataFormatString="{0:n0}"/>
                                    <asp:TemplateField HeaderText="Observación">
                                        <ItemTemplate>
                                            <div class="form-row">
                                                <asp:DropDownList ID="ddlgrvObservacion" CssClass="form-control" runat="server" DataSourceID="sqldsllenarddlObserv" DataTextField="nomcat" DataValueField="codcat"></asp:DropDownList>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>

                                <EditRowStyle BackColor="#999999" />
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#00445e" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                <RowStyle BackColor="#BBD9EF" ForeColor="#333333" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                <SortedDescendingHeaderStyle BackColor="#6F8DAE" />

                            </asp:GridView>
                            *Indicar por que no se agregó los artículos de la lista.
                            <asp:Label ID="lblAlertaModal" runat="server" Text="" ForeColor="Maroon"></asp:Label>
                        </div>                        
                    </div>
                </div> 
                <div class="modal-footer">
                  <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>                 
                  <asp:Button ID="btnAceptar" runat="server" Text="Aceptar" class="btn btn-primary" OnClick="btnAceptar_Click"/>
                </div>
            </div>
        </div>
    </div>--%>
        <!--f Modal Observacion Popup -->


        <!--f MODALS -->

        <!--i SQLDATASOURCES-->
        <%--<asp:SqlDataSource ID="sqldsRenglonesIng" runat="server" ConnectionString="<%$ ConnectionStrings:kdbs_EsperanzaConnectionString %>" 
        SelectCommand="SELECT [codart], [nomart], [canfis] as cantid, [canpar] as canfac, [estdes], [candes] 
        FROM [renglonesingresos] 
        WHERE ([numfac] = @numfac)">
        <SelectParameters>
            <asp:SessionParameter Name="numfac" SessionField="gs_Numfac" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>--%>

        <%--<asp:SqlDataSource ID="sqldsllenarddlObserv" runat="server" ConnectionString="<%$ ConnectionStrings:kdbs_EsperanzaConnectionString %>" SelectCommand="W_CRM_S_DDLOBSERVACION" SelectCommandType="StoredProcedure"></asp:SqlDataSource>--%>

        <asp:SqlDataSource ID="sqlProveedores" runat="server" ConnectionString="<%$ ConnectionStrings:kdbs_EsperanzaConnectionString %>" SelectCommand="SELECT TOP 50 [nompro], [tipind], [codpro] FROM [proveedores] WHERE (([nompro] LIKE '%' + @nompro + '%'))">
            <SelectParameters>
                <asp:SessionParameter Name="nompro" SessionField="gs_Nompro" Type="String" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="sqldsRenglonesIng" runat="server" ConnectionString="<%$ ConnectionStrings:kdbs_EsperanzaConnectionString %>"
            SelectCommand="SELECT [codart], [nomart], [cantid], [preuni], [coduni] 
        FROM [renglonesingresos] 
        WHERE ([numfac] = @numfac)">
            <SelectParameters>
                <asp:SessionParameter Name="numfac" SessionField="gs_Numfac" Type="String" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="sqldsAlmacen" runat="server" ConnectionString="<%$ ConnectionStrings:ReportesBI %>" SelectCommand="SELECT * FROM [vw_BI_Almacenes] WHERE [codalm] != 'A1'"></asp:SqlDataSource>
        <!--f SQLDATASOURCES-->
    </form>
</body>

</html>

