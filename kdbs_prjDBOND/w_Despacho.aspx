﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="w_Despacho.aspx.cs" Inherits="kdbs_prjDBOND.w_TomaFisica" %>
<%@ Register Src="~/WebMenuUserControl.ascx" TagPrefix="uc1" TagName="WebMenuUserControl" %>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Kohinor Móvil - DBOND</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

	<%--SCRIPTS HEAD--%>
	<%--<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>--%>
	<%--<script type="text/javascript">
		$(document).ready(function () {
			$("#<%=txtBuscar.ClientID%>").blur(function () {
				getValues();
			});
		});
	</script>--%>
	<%--SCRIPTS HEAD--%>
</head>

<body id="page-top">
<form id="form1" runat="server">
	<%--SCRIPTS BODY--%>
	<%--<asp:ScriptManager ID="sm1" runat="server" EnablePageMethods="true"></asp:ScriptManager>--%>        
	<%--<script type="text/javascript">
		//validar y obtener cedula de web api sri
		function getValues() {
			var text1 = $("#<%=txtBuscar.ClientID%>").val();
			PageMethods.f_Validar(text1, getValues_Success, getValues_Fail);
		}
		function getValues_Success(data) {
			$("#<%=txtCodart.ClientID%>").val(data.Codart);
			$("#<%=txtNomart.ClientID%>").val(data.Nomart);
			$("#<%=txtCantid.ClientID%>").val(data.Cantid);            
		}
		function getValues_Fail() {
			alert("ERROR : Método getValues_Success(data) fallo.");
		}
	</script>--%>
	<%--SCRIPTS BODY--%>



  <!-- Page Wrapper -->
  <div id="wrapper">

	<!-- Sidebar -->
	<uc1:WebMenuUserControl runat="server" id="WebMenuUserControl" />
	<!-- End of Sidebar -->

	<!-- Content Wrapper -->
	<div id="content-wrapper" class="d-flex flex-column">

	  <!-- Main Content -->
	  <div id="content">

		<!-- Topbar -->
		<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

		  <!-- Sidebar Toggle (Topbar) -->
		  <button onclick="return false;" id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
			<i class="fa fa-bars"></i>
		  </button>

		  <!-- Topbar Search -->            
		  <div class="d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
			<div class="input-group">
			  <%--<input type="text" id="txtBuscar" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2" runat="server" AutoPostBack="True">--%>
			  <asp:TextBox ID="txtBuscar" runat="server" class="form-control bg-light border-0 small" placeholder="Buscar..." AutoPostBack="True"></asp:TextBox>
			  <div class="input-group-append">
				<button class="btn btn-primary" type="button">
				  <i class="fas fa-search fa-sm"></i>
				</button>
			  </div>
			</div>
		  </div>

		  <!-- Topbar Navbar -->
		  <ul class="navbar-nav ml-auto">
			  <!-- Static Topbar Search for Mobile-->
			  <%--<li class="nav-item dropdown no-arrow d-sm-none">
				<div class="form-inline mr-auto w-100 navbar-search">
				  <div class="input-group">                    
					<asp:TextBox ID="txtBuscarMobile" runat="server" class="form-control" placeholder="Search for..." AutoPostBack="True"></asp:TextBox>
					<div class="input-group-append">
					  <button class="btn btn-primary" type="button">
						<i class="fas fa-search fa-sm"></i>
					  </button>
					</div>
				  </div>
				</div> 
			   </li>--%>
			  <div class="topbar-divider d-none d-sm-block"></div>

			<!-- Nav Item - User Information -->
			<li class="nav-item dropdown no-arrow">
			  <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<span class="mr-2 d-none d-lg-inline text-gray-600 small">
					<asp:Label ID="lblUsuario" runat="server" Text=""></asp:Label>
				</span>
				<img class="img-profile rounded-circle" src="Imagenes/catUser.png">
			  </a>
			  <!-- Dropdown - User Information -->
			  <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">                     
				  <asp:HyperLink ID="hpkUsuario" class="dropdown-item" runat="server">                      
					  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
					  <asp:Label ID="lblUsuarioDrop" runat="server"></asp:Label>
				  </asp:HyperLink>
				
				<div class="dropdown-divider"></div>
				<a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
				  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
				  Logout
				</a>
			  </div>
			</li>

		  </ul>

		</nav>
		<!-- End of Topbar -->

		<!-- Begin Page Content -->
		<div class="container-fluid">

		  <!-- Page Heading -->
		  <div class="d-sm-flex align-items-center justify-content-between mb-4">
			  <asp:Label ID="lblError" runat="server" Text="" ForeColor="Maroon"></asp:Label>
			<%--<h1 class="h3 mb-0 text-gray-800"><strong>Toma Física</strong></h1>--%>
			<%--<a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>--%>
			  
		  </div>

		  
			<%--Seccion Textbox Toma Fisica--%>
		  <div class="row">

			<!-- Area Chart -->
			<div class="col">
			  <div class="card shadow mb-4">
				<!-- Card Header - Dropdown -->
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
				  <h6 class="m-0 font-weight-bold text-primary">Despacho Manual</h6>
				  <div class="dropdown no-arrow">
					<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					  <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
					</a>
					<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
					  <div class="dropdown-header">Acciones:</div>
						<%--<asp:Button ID="btnDespachar" class="d-sm-inline-block btn btn-sm btn-primary shadow-sm" runat="server" Text="Despachar" OnClick="btnDespachar_Click" />--%>
						<asp:Button ID="btnDespachar" class="dropdown-item" runat="server" Text="&#9997; Despachar" OnClick="btnDespachar_Click" />
						<asp:Button ID="btnPendiente" class="dropdown-item" runat="server" Text="💾 Grabar Pendiente" OnClick="btnPendiente_Click"/>
						<div class="dropdown-divider"></div>
						<asp:Button ID="btnGuardarParcial" class="dropdown-item" runat="server" Text= "&#9898; Guardar Parcial" OnClick="btnGuardarParcial_Click" />
						<asp:Button ID="btnGuardar" class="dropdown-item" runat="server" Text= "&#9899; Guardar" OnClick="btnGuardar_Click" />                      
						<%--<div class="dropdown-divider"></div>--%>
						<%--<asp:Label ID="lblNota" runat="server" Text="*Todas las cantidades deben estar en cero para poder Guardar*"></asp:Label>--%>
					  <%--<a class="dropdown-item" href="#" style="width: 10px">**</a>--%>
					</div>
				  </div>
				</div>
				<!-- Card Body -->
				<div class="card-body">        
					<div style="width: 100%; overflow: scroll">
						<div class="row">
							<div class="col">
								<asp:TextBox ID="txtCodart" class="form-control form-control-user" runat="server" placeholder="Código" Width="100%"></asp:TextBox>
								<asp:TextBox ID="txtNomart" class="form-control form-control-user" runat="server" placeholder="Nombre" Width="100%"></asp:TextBox>
								<asp:TextBox ID="txtCantid" class="form-control form-control-user" runat="server" TextMode="Number" placeholder="Cantidad" Width="100%"></asp:TextBox>                            
							</div>
							<div class="col">
								<asp:TextBox ID="txtNumegr" class="form-control form-control-user" runat="server" placeholder="N° Egreso" ReadOnly="true" Width="100%"></asp:TextBox>
								<asp:TextBox ID="txtFecegr" class="form-control form-control-user" runat="server" ReadOnly="true" Width="100%"></asp:TextBox>                                                                       
								<%--<span class="mr-2 d-none d-lg-inline text-gray-600 small">
									<asp:Label ID="lblSaldo" runat="server" Text="Saldo"></asp:Label>
								</span>--%>
								<asp:TextBox ID="txtSaldo" class="form-control form-control-user" runat="server" TextMode="Number" placeholder="Saldo" ReadOnly="true" Width="100%"></asp:TextBox>            
							</div>
						</div>                           
					</div>
				  </div>
			  </div>
			</div>

		  </div>
			<%--Seccion Gridview Toma Fisica--%>
		  <div class="row">

			<!-- Area Chart -->
			<div class="col">
			  <div class="card shadow mb-4">
				<!-- Card Header - Dropdown -->
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">                  
				  <h6 class="m-0 font-weight-bold text-primary">Listado Artículos</h6>
				  <%--<div class="dropdown no-arrow">
					<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					  <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
					</a>
					<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
					  <div class="dropdown-header">Dropdown Header:</div>
					  <a class="dropdown-item" href="#">Action</a>
					  <a class="dropdown-item" href="#">Another action</a>
					  <div class="dropdown-divider"></div>
					  <a class="dropdown-item" href="#">Something else here</a>
					</div>
				  </div>--%>
				</div>
				<!-- Card Body -->
				<div class="card-body">       
					<div style="width: 100%; overflow: scroll">
					<asp:GridView ID="grvRenglonespedpro" runat="server"  AutoGenerateColumns="False" ShowHeaderWhenEmpty="True" class="table table-sm table-dark">
									
											<Columns>
												<asp:TemplateField HeaderText="N°" ItemStyle-Width="1%">
													<ItemTemplate>
													<%# Container.DataItemIndex + 1 %>
													</ItemTemplate>
													<ItemStyle Width="1%"></ItemStyle>
												</asp:TemplateField>
												<asp:BoundField DataField="codart" HeaderText="Código" SortExpression="codart" />
												<asp:BoundField DataField="cantid" HeaderText="Cantidad" SortExpression="cantid" />
												<asp:BoundField DataField="nomart" HeaderText="Nombre" SortExpression="nomart" />
												<asp:TemplateField>
													<ItemTemplate>
														<asp:CheckBox ID="chkConfirmado" runat="server" Enabled="False" />
													</ItemTemplate>
												</asp:TemplateField>
												<asp:BoundField DataField="desart" HeaderText="Desart" SortExpression="desart" />
												
											</Columns>
									
											<EditRowStyle BackColor="#999999" />
											<FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
											  
											<PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
											<RowStyle BackColor="#BBD9EF" ForeColor="#333333" />
											<SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
											<SortedAscendingCellStyle BackColor="#E9E7E2" />
											<SortedAscendingHeaderStyle BackColor="#506C8C" />
											 <SortedDescendingCellStyle BackColor="#FFFDF8" />
											<SortedDescendingHeaderStyle BackColor="#6F8DAE" />
											<AlternatingRowStyle BackColor="#F8FAFA" ForeColor="#284775" /> 
					</asp:GridView>
						
					 </div>
				  </div>
			  </div>
			</div>

		  </div>

		</div>
		<!-- /.container-fluid -->

	  </div>
	  <!-- End of Main Content -->

	  <!-- Footer -->
	  <footer class="sticky-footer bg-white">
		<div class="container my-auto">
		  <div class="copyright text-center my-auto">
			<span>Copyright &copy; KOHINOR 2021 Todos Los Derechos Reservados</span>
		  </div>
		</div>
	  </footer>
	  <!-- End of Footer -->

	</div>
	<!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
	<i class="fas fa-angle-up"></i>
  </a>

  

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/chart-area-demo.js"></script>
  <script src="js/demo/chart-pie-demo.js"></script>

	<!--i MODALS -->

	<!--i Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
	  <div class="modal-content">
		<div class="modal-header">
		  <h5 class="modal-title" id="exampleModalLabel">¿Desea salir?</h5>
		  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">×</span>
		  </button>
		</div>
		<div class="modal-body">Seleccionar "Logout" abajo para salir de la sesión actual.</div>
		<div class="modal-footer">
		  <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
		  <a class="btn btn-primary" href="w_Login.aspx">Logout</a>
		</div>
	  </div>
	</div>
  </div>
	<!--f Logout Modal-->

	<!--i Modal Articulos Popup -->
<div id="modalBuscar" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header" >
				<h4 class="modal-title" >
					Seleccione Artículos
				</h4>
				<button type="button" class="close" data-dismiss="modal">
					&times;</button>                
			</div>
			<div class="modal-body">
				<!-- CONTENIDO-->  
				
				<div class="form-row">
					<asp:Label ID="Label1" runat="server" Text="Nombre"></asp:Label>
					&nbsp;&nbsp;
					<asp:TextBox ID="txtBuscarNomart" runat="server" class="form-control form-control-sm" Width="20%"></asp:TextBox>
					<asp:Button ID="btnBuscarNomart" runat="server" Text="Buscar" class="btn btn-primary" OnClick="btnBuscarNomart_Click" />
				</div>

				<br />

				<div style = "width:100%;overflow: scroll;">
					
				  <asp:GridView ID="grvArticulos" runat="server" DataSourceID="sqldsRenglonespedproBuscar" class="table table-sm table-dark" AutoGenerateColumns="False" OnSelectedIndexChanged="grvArticulos_SelectedIndexChanged">
				   
						<AlternatingRowStyle BackColor="#F8FAFA" ForeColor="#284775" /> 

						<Columns>
							<asp:CommandField ButtonType="Image" SelectImageUrl="~/Imagenes/Select-Hand.png" ShowSelectButton="True">
							<ControlStyle Width="30px" />
							</asp:CommandField>
							<asp:BoundField DataField="codart" HeaderText="Código" SortExpression="codart" />
							<asp:BoundField DataField="cantid" HeaderText="Cantidad" SortExpression="cantid" />
							<asp:BoundField DataField="nomart" HeaderText="Nombre" SortExpression="nomart" />
						</Columns>

						<EditRowStyle BackColor="#999999" />
						<FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
						<HeaderStyle BackColor="#00445e" Font-Bold="True" ForeColor="White" />
						<PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
						<RowStyle BackColor="#BBD9EF" ForeColor="#333333" />
						<SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
						<SortedAscendingCellStyle BackColor="#E9E7E2" />
						<SortedAscendingHeaderStyle BackColor="#506C8C" />
						<SortedDescendingCellStyle BackColor="#FFFDF8" />
						<SortedDescendingHeaderStyle BackColor="#6F8DAE" />

					</asp:GridView>              
				</div>
				</div>
		  
			</div>
		</div>
	</div>       
				
	<!--f Modal Articulos Popup -->

	<!-- Modal Observacion Popup -->
	<div id="modalObservacion" class="modal fade" role="dialog">
		<div class="modal-dialog modal-lg">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header" >
					<h4 class="modal-title" >
						Observación Despacho Parcial
					</h4>
					<button type="button" class="close" data-dismiss="modal">
						&times;</button>                
				</div>
				<div class="modal-body">
					<!-- CONTENIDO-->                  
					<div class="form-row">
						<div style="width: 100%; overflow: scroll">
							<%--<asp:TextBox ID="txtObservacion" runat="server" class="form-control form-control-sm" Width="100%" TextMode="MultiLine" Height="200" placeholder="*Descripción / razón del despacho. (Obligatorio llenar para poder guardar. Hasta 160 carácteres)"></asp:TextBox>--%>
							<asp:GridView ID="grvObservacionArticulos" runat="server" class="table table-sm table-dark" AutoGenerateColumns="False">
								<AlternatingRowStyle BackColor="#F8FAFA" ForeColor="#284775" />
								<Columns>
									<asp:TemplateField HeaderText="N°" ItemStyle-Width="1%">
										<ItemTemplate>
											<%# Container.DataItemIndex + 1 %>
										</ItemTemplate>
										<ItemStyle Width="1%"></ItemStyle>
									</asp:TemplateField>
									<asp:BoundField DataField="codart" HeaderText="Código" SortExpression="codart" />
									<asp:BoundField DataField="desart" HeaderText="Desart" SortExpression="desart" />
									<asp:BoundField DataField="cantid" HeaderText="Cantid" SortExpression="cantid" DataFormatString="{0:n2}"/>
									<asp:TemplateField HeaderText="Observación">
										<ItemTemplate>
											<div class="form-row">
												<asp:DropDownList ID="ddlgrvObservacion" CssClass="form-control" runat="server" DataSourceID="sqldsllenarddlObserv" DataTextField="nomcat" DataValueField="codcat"></asp:DropDownList>
											</div>
										</ItemTemplate>
									</asp:TemplateField>
								</Columns>

								<EditRowStyle BackColor="#999999" />
								<FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
								<HeaderStyle BackColor="#00445e" Font-Bold="True" ForeColor="White" />
								<PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
								<RowStyle BackColor="#BBD9EF" ForeColor="#333333" />
								<SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
								<SortedAscendingCellStyle BackColor="#E9E7E2" />
								<SortedAscendingHeaderStyle BackColor="#506C8C" />
								<SortedDescendingCellStyle BackColor="#FFFDF8" />
								<SortedDescendingHeaderStyle BackColor="#6F8DAE" />

							</asp:GridView>
							*La cantidad mostrada son los artículos sobrantes, no los despachados.
							<asp:Label ID="lblAlertaModal" runat="server" Text="" ForeColor="Maroon"></asp:Label>
						</div>                        
					</div>
				</div> 
				<div class="modal-footer">
				  <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>                 
				  <asp:Button ID="btnAceptar" runat="server" Text="Aceptar" class="btn btn-primary" OnClick="btnAceptar_Click"/>
				</div>
			</div>
		</div>
	</div>  


	<!--f MODALS -->

	<!--i SQLDATASOURCES-->
	<asp:SqlDataSource ID="sqldsRenglonespedpro" runat="server" ConnectionString="<%$ ConnectionStrings:kdbs_EsperanzaConnectionString %>" 
		SelectCommand="SELECT R.[codart], A.[desart], R.[cantid], R.[canpar] as canfac, R.[nomart], R.[coduni], R.[preuni], R.[estdes], R.[candes] 
		FROM [renglonespedpro] R,[articulos] A  
		WHERE ([numtra] = @numtra AND
		A.[codart] = R.[codart] AND
		A.codemp = R.codemp
		) UNION
		SELECT R.[codart], R.[desart], R.[cantid], 0 as canfac, R.[nomart], R.[coduni], R.[preuni], '1', 1
		FROM [renglonespedpro] R 
		WHERE ([numtra] = '001001-18000023' AND
		[codart] LIKE '/%')">
	   <SelectParameters>
		  <asp:SessionParameter Name="numtra" SessionField="gs_Numtra" Type="String" />
	   </SelectParameters>
	</asp:SqlDataSource> 

	<asp:SqlDataSource ID="sqldsRenglonespedproBuscar" runat="server" ConnectionString="<%$ ConnectionStrings:kdbs_EsperanzaConnectionString %>" SelectCommand="SELECT [codart], [cantid], [nomart] FROM [renglonespedpro] WHERE (([numtra] = @numtra) AND ([nomart] LIKE '%' + @nomart + '%'))">
	   <SelectParameters>
		  <asp:SessionParameter Name="numtra" SessionField="gs_Numtra" Type="String" />
		   <asp:SessionParameter Name="nomart" SessionField="gs_Nomart" Type="String" />
	   </SelectParameters>
	</asp:SqlDataSource>

	<asp:SqlDataSource ID="sqldsllenarddlObserv" runat="server" ConnectionString="<%$ ConnectionStrings:kdbs_EsperanzaConnectionString %>" SelectCommand="W_CRM_S_DDLOBSERVACION" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
	
	<!--f SQLDATASOURCES-->
</form>
</body>

</html>
