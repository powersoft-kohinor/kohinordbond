﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WebMenuUserControl.ascx.cs" Inherits="kdbs_prjDBOND.WebMenuUserControl" %>
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion toggled" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
        <div class="sidebar-brand-icon">
            <img src="Imagenes/Logo-Kohinor2.png" width="50px" />
            <%--<i class="fas fa-laugh-wink"></i>--%>
        </div>
        <div class="sidebar-brand-text mx-2">KOHINOR <sup>V3.1</sup></div>
    </a>

    <!-- Divider -->
    <%--<hr class="sidebar-divider my-0">--%>

    <!-- Nav Item - Pages Collapse Menu -->
<%--    <li class="nav-item active">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw far fa-tasks"></i>
            <span>Despacho</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Componentes:</h6>

                <asp:LinkButton ID="lnbtnDespachoListado" class="collapse-item" runat="server" OnClick="lnbtnMenuRedirect_Click" CommandArgument="1-1" CommandName="w_DespachoListado"> 
              <span>Listado Despacho</span></asp:LinkButton>
                <asp:LinkButton ID="lnbtnDespacho" class="collapse-item" runat="server" OnClick="lnbtnMenuRedirect_Click" CommandArgument="1-2-1" CommandName="w_Despacho">
              <span>Despacho Artículos</span></asp:LinkButton>
            </div>
        </div>
    </li>--%>

    <!-- Divider -->
    <%--<hr class="sidebar-divider my-0">--%>

    <!-- Nav Item - Toma Física -->
<%--    <li class="nav-item active">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo1" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fal fa-edit"></i>
            <span>Toma Física</span>
        </a>
        <div id="collapseTwo1" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Componentes:</h6>

                <asp:LinkButton ID="lnbtnListado" class="collapse-item" runat="server" OnClick="lnbtnMenuRedirect_Click" CommandArgument="2-1" CommandName="w_TomaFisicaListado">         
              <span>Listado Toma F.</span></asp:LinkButton>
                <asp:LinkButton ID="lnbtnTomaFisica" class="collapse-item" runat="server" OnClick="lnbtnMenuRedirect_Click" CommandArgument="2-2" CommandName="w_TomaFisica">
              <span>Toma Física </span></asp:LinkButton>
            </div>

        </div>
    </li>--%>

        <!-- Divider -->
        <%--<hr class="sidebar-divider my-0">--%>

        <!-- Nav Item - Importaciones -->
<%--        <li class="nav-item active">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo2" aria-expanded="true" aria-controls="collapseTwo">
                <i class="fas fa-envelope-open-text"></i>
                <span>Importaciones</span>
            </a>
            <div id="collapseTwo2" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Componentes:</h6>

                    <asp:LinkButton ID="lnbtnImportListado" class="collapse-item" runat="server" OnClick="lnbtnMenuRedirect_Click" CommandArgument="4-1" CommandName="w_ImportListado">
              <span>Listado Import.</span></asp:LinkButton>
                    <asp:LinkButton ID="lnbtnImportaciones" class="collapse-item" runat="server" OnClick="lnbtnMenuRedirect_Click" CommandArgument="4-2" CommandName="w_Importaciones">
              <span>Importaciones </span></asp:LinkButton>
                </div>

            </div>
        </li>--%>

    <!-- Divider -->
    <%--<hr class="sidebar-divider my-0">--%>

    <!-- Nav Item - Transferencias -->
<%--    <li class="nav-item active">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo3" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-people-carry"></i>
            <span>Transferencias</span>
        </a>
        <div id="collapseTwo3" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Componentes:</h6>

                <asp:LinkButton ID="lnbtnTransferListado" class="collapse-item" runat="server" OnClick="lnbtnMenuRedirect_Click" CommandArgument="5-1" CommandName="w_TransferListado">
              <span>Listado Transfer.</span></asp:LinkButton>

                <asp:LinkButton ID="lnbtnTransferencias" class="collapse-item" runat="server" OnClick="lnbtnMenuRedirect_Click" CommandArgument="5-2" CommandName="w_Transferencias">
              <span>Transferencias </span></asp:LinkButton>
            </div>

        </div>
    </li>--%>

        <!-- Divider -->
      <hr class="sidebar-divider my-0">

        <!-- Nav Item - Abrir -->
      <li class="nav-item active">
        <asp:LinkButton ID="lkbtnAbrir" class="nav-link" runat="server" OnClick="lnbtnMenuRedirect_Click" CommandArgument="8-1" CommandName="w_Abrir">
           <i class="fas fa-folder-open"></i>
           <span>Abrir</span></asp:LinkButton>
      </li>

        <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Toma Manual -->
    <li class="nav-item active">
        <asp:LinkButton ID="lnbtnTomaManual" class="nav-link" runat="server" OnClick="lnbtnMenuRedirect_Click" CommandArgument="3-1" CommandName="w_TomaManual">
           <i class="fas fa-fw fal fa-user-edit"></i>
          <span>Toma Manual</span>
        </asp:LinkButton>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Artículos -->
    <li class="nav-item active">
        <asp:LinkButton ID="lnbtnArticulos" class="nav-link" runat="server" OnClick="lnbtnMenuRedirect_Click" CommandArgument="7-1" CommandName="w_Articulos">
           <i class="fas fa-box-open"></i>
           <span>Artículos</span></asp:LinkButton>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Listado Productos -->
    <li class="nav-item active bg-gradient-primary">
        <asp:LinkButton ID="lnbtnProductos" class="nav-link" runat="server" OnClick="lnbtnMenuRedirect_Click" CommandArgument="10-1" CommandName="w_ListadoProductos">
           <i class="fas fa-box-open"></i>
           <span>Listado Productos</span></asp:LinkButton>
    </li>

    <!-- Divider -->
      <hr class="sidebar-divider my-0">       

        <!-- Nav Item - Informe Venta Descuento -->
      <li class="nav-item active bg-gradient-primary">
        <asp:LinkButton ID="lkbtnInformeVentaDesc" class="nav-link" runat="server" OnClick="lnbtnMenuRedirect_Click" CommandArgument="9-1" CommandName="w_VentaDescuento">
           <i class="fas fa-file-invoice-dollar"></i>
           <span>Ventas Desc</span></asp:LinkButton>
      </li>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Salida -->
    <li class="nav-item active bg-gradient-dark">
        <asp:LinkButton ID="lnbtnSalida" class="nav-link" runat="server" OnClick="lnbtnMenuRedirect_Click" CommandArgument="6-1" CommandName="w_Login">
           <i class="fas fa-fw far fa-sign-out-alt"></i>
           <span>Salida</span></asp:LinkButton>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button onclick="return false;" class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
