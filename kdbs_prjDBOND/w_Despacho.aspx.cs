﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

//LAST ERROR CODE: *16.

namespace kdbs_prjDBOND
{
    public partial class w_TomaFisica : System.Web.UI.Page
    {
        protected SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
        protected clsSecuencia objSecuencia = new clsSecuencia();
        protected clsEgreso objEgreso = new clsEgreso();
        protected clsPedpro objPedpro = new clsPedpro();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["gs_CodUs1"] != null)
                {
                    lblUsuario.Text = Session["gs_CodUs1"].ToString();
                    lblUsuarioDrop.Text = Session["gs_CodUs1"].ToString();
                }
                else
                {
                    Response.Redirect("w_Login.aspx");
                }

                //PARA LLENAR grvRenglonespedpro
                DataSourceSelectArguments args = new DataSourceSelectArguments(); //para pasar del SqlDataSource1 a una DataTable
                DataView view = (DataView)sqldsRenglonespedpro.Select(args);            //para pasar del SqlDataSource1 a una DataTable
                DataTable dt = view.ToTable(); //en esta dataTable esta el pedido seleccionado... (con los datos de este se llenara el encabezadoegresos)
                //para agregar saldo (cantid original)
                dt.Columns.Add("saldo", typeof(decimal));
                foreach (DataRow row in dt.Rows)
                {
                    row["saldo"] = row.Field<decimal>("cantid"); //guarda el total de cada articulo en columna saldo

                    if (!DBNull.Value.Equals(row["canfac"])) //para saber si canfac es NULL
                    {
                        row["cantid"] = row.Field<decimal>("canfac"); //si han hecho despacho parcial el valor de canfac debe guardarse en cantid
                        row["saldo"] = row["cantid"];
                    }
                }

                Session.Add("gdt_Articulos", dt);
                grvRenglonespedpro.DataSource = dt;
                grvRenglonespedpro.DataBind();

                //la cantid puede ser 0 cuando hayan realizado un despacho parcial
                //para esto se llena el chk y se pinta de verde apenaz ingrese a Despacho.aspx
                int rowIndex = 0;
                foreach (GridViewRow row in grvRenglonespedpro.Rows)
                {
                    if ((int)decimal.Parse(row.Cells[2].Text.Trim()) == 0) //llena el chkConfirmar
                    {
                        CheckBox rBoton = (CheckBox)row.Cells[4].FindControl("chkConfirmado");
                        rBoton.Checked = true;
                        row.BackColor = Color.LightGreen;
                    }

                    //para poner color azul en caso de que haya sido grabado pendiente
                    string estdes = "";
                    if (!DBNull.Value.Equals(dt.Rows[rowIndex]["estdes"])) //si estdes no es NULL
                    {
                        estdes = dt.Rows[rowIndex].Field<string>("estdes");
                    }

                    if (estdes.Equals("1"))
                    {
                        row.BackColor = Color.DodgerBlue;
                    }

                    //PARA MOSTRAR EL CODIGO ALTERNO EN LUGAR DE CODART....SOLO SI NO ES NULL
                    if (row.Cells[5].Text != "&nbsp;") //para saber si desart es NULL
                    {
                        string s_aux = row.Cells[1].Text; //PARA PASAR desart A Cells[1] y codart  A Cells[5]
                        row.Cells[1].Text = row.Cells[5].Text; //Muestra desart en Código
                        row.Cells[5].Text = s_aux;
                    }
                    rowIndex++;
                }
                grvRenglonespedpro.Columns[5].Visible = false; //esconde la columna de desart

                txtFecegr.Text = DateTime.Now.ToString("yyyy-MM-dd");
                Session.Add("gs_ArtDespachado", "");
                Session.Add("gs_ContVal", "0"); //para que al aplastar muchas veces solo se haga 1

                //PARA CALCULAR CANTID TOTAL ...(PARA VALIDACION DE btnGuardar)                
                Session.Add("gs_CanTot", f_CanTot()); //variable que guarda la suma de las cantidades de grvRenglonespedpro

                //SECUENCIA N° EGR                
                objSecuencia = objSecuencia.f_CalcularSecuencia(Session["gs_CodEmp"].ToString(), Session["gs_SerSec"].ToString(), "VC_EGR");
                if (String.IsNullOrEmpty(objSecuencia.Error.Mensaje))
                {
                    Session.Add("gs_NumFac", objSecuencia.Numfac);
                    txtNumegr.Text = objSecuencia.Numfac;
                }
                else
                {
                    lblError.Text = objSecuencia.Error.f_ErrorNuevo("*16.", objSecuencia.Error);
                }
            }

            if (IsPostBack) //PARA BUSCAR AL HACER TAB EN TXTBUSCAR
            {
                if (!txtBuscar.Text.Equals(""))
                {
                    //validacion para borrar 001 al final de codart...en caso de que tenga 001
                    string s_result = txtBuscar.Text;
                    try
                    {
                        string s_tresdigitos = txtBuscar.Text.Substring(txtBuscar.Text.Length - 4);
                        if (s_tresdigitos.Equals(" 001") || s_tresdigitos.Equals("0001"))
                            s_result = s_result.Substring(0, s_result.Length - 4);
                        else
                            s_result = txtBuscar.Text;
                    }
                    catch (Exception ex)
                    {
                        s_result = txtBuscar.Text;
                    }

                    clsArticulo objArticulo = f_BuscarArticulo(s_result);
                    f_ActualizarCampos(objArticulo);
                    txtBuscar.Focus();
                }
            }
        }

        public void f_MostrarCodAlterno()
        {
            //grvRenglonespedpro.Columns[5].Visible = true;
            //PARA MOSTRAR EL CODIGO ALTERNO EN LUGAR DE CODART....SOLO SI NO ES NULL
            foreach (GridViewRow row in grvRenglonespedpro.Rows)
            {
                if (row.Cells[5].Text != "&nbsp;") //para saber si desart es NULL
                {
                    string s_aux = row.Cells[1].Text; //PARA PASAR desart A Cells[1] y codart  A Cells[5]
                    row.Cells[1].Text = row.Cells[5].Text; //Muestra desart en Código
                    row.Cells[5].Text = s_aux;
                }
            }
        }

        public int f_CanTot() //PARA CALCULAR CANTID TOTAL DE grvRenglonespedpro...(PARA VALIDACION DE btnGuardar)
        {
            int i_cantot = 0;
            foreach (GridViewRow row in grvRenglonespedpro.Rows)
            {
                i_cantot = i_cantot + (int)decimal.Parse(row.Cells[2].Text.Trim());
            }
            return i_cantot;
        }

        public clsArticulo f_BuscarArticulo(string s_codart)
        {
            clsArticulo objArticulo = new clsArticulo();
            DataTable dt = (DataTable)Session["gdt_Articulos"];

            if (s_codart.Any(char.IsDigit)) //si contiene por lo menos 1 numero (pistoleo el codigo....todo se hace automático)
            {
                try
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        string s_desart = "ZZZ"; //no poner vacio, ZZZ para que el al buscar nada en txtBuscar no haya problemas
                        if (!DBNull.Value.Equals(row["desart"])) //para saber si desart es NULL
                        {
                            s_desart = row.Field<string>("desart").Trim();
                        }

                        if ((row.Field<string>("codart").Trim().Equals(s_codart.Trim()) || s_desart.Equals(s_codart.Trim())) && (int)row.Field<decimal>("cantid") > 0)
                        {
                            objArticulo.Codart = s_codart;
                            string s_nomart = row.Field<string>("nomart");
                            objArticulo.Nomart = s_nomart;
                            int i_cantid = (int)row.Field<decimal>("cantid") - 1;

                            //actualiza el dt
                            row["cantid"] = i_cantid;

                            objArticulo.Cantid = i_cantid;
                            objArticulo.Error = "CORRECTO";

                            Session["gdt_Articulos"] = dt;
                            grvRenglonespedpro.Columns[5].Visible = true; //para q f_MostrarCodAlterno() funcione
                            grvRenglonespedpro.DataSource = dt;
                            grvRenglonespedpro.DataBind();

                            f_MostrarCodAlterno(); //PARA MOSTRAR CODIGO ALTERNO
                            f_pintarGridview(s_codart, i_cantid, s_desart);
                            grvRenglonespedpro.Columns[5].Visible = false; //ocultar columna extra...DEBE IR AQUI PARA QUE COLORES DE GRV FUNCIONEN
                            Session["gs_ArtDespachado"] = "1"; //para saber que ha despachado por lo menos 1 articulo..se usa en f_insertarArticulos
                            return objArticulo;
                        }
                    }
                }
                catch (Exception ex)
                {
                    objArticulo.Codart = "-";
                    objArticulo.Nomart = "-";
                    objArticulo.Cantid = 0;
                    objArticulo.Error = "*08. " + DateTime.Now + " " + ex.Message;
                    return objArticulo;
                }
            }
            else // si busco por nombre en txtBuscar
            {
                Session.Add("gs_Nomart", s_codart.Trim());
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ModalView", "<script>$(function() { $('#modalBuscar').modal('show'); });</script>", false);
                objArticulo.Codart = "-";
                objArticulo.Nomart = "-";
                objArticulo.Cantid = 0;
                objArticulo.Error = "";
                return objArticulo;
            }

            objArticulo.Codart = "-";
            objArticulo.Nomart = "-";
            objArticulo.Cantid = 0;
            objArticulo.Error = "*09. No se encontró artículo.";
            return objArticulo;
        }
        
        public void f_pintarGridview(string s_codart, int i_cantid, string s_desart) //pinta y llena el chkConfirmar
        {
            foreach (GridViewRow row in grvRenglonespedpro.Rows)
            {

                if ((int)decimal.Parse(row.Cells[2].Text.Trim()) == 0) //llena el chkConfirmar
                {
                    CheckBox rBoton = (CheckBox)row.Cells[4].FindControl("chkConfirmado");
                    rBoton.Checked = true;
                }
                string codart1 = row.Cells[1].Text.Trim();
                string alterno = row.Cells[5].Text.Trim();

                if (row.Cells[1].Text.Trim().Equals(s_codart.Trim()) || row.Cells[5].Text.Trim().Equals(s_codart.Trim())) //pinta la fila con codart
                {
                    if (i_cantid == 0)
                        row.BackColor = Color.LightGreen;
                    else
                        row.BackColor = Color.Khaki;
                }
            }
        }

        protected void grvArticulos_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow fila = grvArticulos.SelectedRow;
            string s_codart = fila.Cells[1].Text.Trim();
            string s_cantid = Server.HtmlDecode(fila.Cells[2].Text.Trim()); //para que acepte caracteres especiales (Ñ)
            string s_nomart = fila.Cells[3].Text.Trim();
            txtCodart.Text = s_codart;
            txtCantid.Text = "1";
            txtSaldo.Text = s_cantid;
            txtNomart.Text = s_nomart;
            lblError.Text = "";

            ScriptManager.RegisterStartupScript(this, this.GetType(), "ModalView", "<script>$(function() { $('#modalBuscar').modal('hide'); });</script>", false);
        }

        protected void btnBuscarNomart_Click(object sender, EventArgs e)
        {
            if (txtBuscarNomart.Text.Equals(""))
                Session["gs_Nomart"] = "%";
            else
                Session["gs_Nomart"] = txtBuscarNomart.Text.Trim();

            ClientScript.RegisterStartupScript(this.GetType(), "Popup", "$('#modalBuscar').modal('show')", true);
        }

        protected void btnDespachar_Click(object sender, EventArgs e)
        {
            clsArticulo objArticulo = new clsArticulo();
            try
            {
                objArticulo.Codart = txtCodart.Text;
                objArticulo.Nomart = txtNomart.Text;
                objArticulo.Cantid = (int)decimal.Parse(txtSaldo.Text);
                objArticulo.Error = "";
                DataTable dt = (DataTable)Session["gdt_Articulos"];
                string s_codart = txtCodart.Text.Trim();

                int i_cantidDesapachar = 1;
                if (!txtCantid.Text.Equals("")) //si deja vacio txtCantid se asume que es 1
                {
                    i_cantidDesapachar = int.Parse(txtCantid.Text.ToString());
                }

                foreach (DataRow row in dt.Rows)
                {
                    string s_desart = "ZZZ"; //no poner vacio, ZZZ para que el al buscar nada en txtBuscar no haya problemas
                    if (!DBNull.Value.Equals(row["desart"])) //para saber si desart es NULL
                    {
                        s_desart = row.Field<string>("desart").Trim();
                    }

                    if ((row.Field<string>("codart").Trim().Equals(s_codart.Trim()) || s_desart.Equals(s_codart.Trim())) && (int)row.Field<decimal>("cantid") > 0)
                    {
                        if ((int)row.Field<decimal>("cantid") < i_cantidDesapachar) //si ingresa cantidad mayor que saldo existente
                        {
                            txtCantid.Text = "1";
                            objArticulo.Error = "*10. Cantidad ingresada es superior a Saldo. Despacho no realizado.";
                            f_ActualizarCampos(objArticulo);
                        }
                        else //cantidad correcta
                        {
                            objArticulo.Codart = s_codart;
                            string s_nomart = row.Field<string>("nomart");
                            objArticulo.Nomart = s_nomart;
                            int i_cantid = (int)row.Field<decimal>("cantid") - i_cantidDesapachar;

                            //actualiza el dt
                            row["cantid"] = i_cantid;

                            objArticulo.Cantid = i_cantid;
                            objArticulo.Error = "CORRECTO";

                            Session["gdt_Articulos"] = dt;
                            grvRenglonespedpro.Columns[5].Visible = true; //para q f_MostrarCodAlterno() funcione
                            grvRenglonespedpro.DataSource = dt;
                            grvRenglonespedpro.DataBind();

                            f_MostrarCodAlterno(); //PARA MOSTRAR CODIGO ALTERNO
                            f_pintarGridview(s_codart, i_cantid, s_desart);
                            grvRenglonespedpro.Columns[5].Visible = false; //ocultar columna extra...DEBE IR AQUI PARA QUE COLORES DE GRV FUNCIONEN
                            f_ActualizarCampos(objArticulo);
                        }
                    }
                }
                //aqui poner objArticulo.Codart = "-"; .....
            }
            catch (Exception ex)
            {
                objArticulo.Codart = "-";
                objArticulo.Nomart = "-";
                objArticulo.Cantid = 0;
                objArticulo.Error = "*11. " + DateTime.Now + " " + ex.Message;
                f_ActualizarCampos(objArticulo);
            }
        }

        public void f_ActualizarCampos(clsArticulo objArticulo)
        {
            txtCodart.Text = objArticulo.Codart;
            txtNomart.Text = objArticulo.Nomart;
            txtSaldo.Text = objArticulo.Cantid.ToString();
            if (!objArticulo.Error.Equals("CORRECTO"))
            {
                lblError.Text = "*01. " + DateTime.Now + " " + objArticulo.Error;
            }
            else
            {
                lblError.Text = "";
            }
            txtBuscar.Text = "";
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            if (f_CanTot() == 0) //verifica si todos los cantid estan en 0
            {
                if (Session["gs_ContVal"].ToString().Equals("0")) //para que al aplastar muchas veces solo se haga 1
                {
                    Session["gs_ContVal"] = "1";
                    f_insertarArticulos("btnGuardar", "1"); //est002=1 ...para q wladi sepa si fue parcial o completo

                    clsError objError = new clsError();
                    if (lblError.Text.Trim() == "") //control de errores
                    {
                        objError = objPedpro.f_actualizarEncabezadopedpro(Session["gs_CodEmp"].ToString(),
                            Session["gs_Numtra"].ToString(), txtNumegr.Text);
                        if (!String.IsNullOrEmpty(objError.Mensaje))
                            lblError.Text = objError.f_ErrorNuevo("*06.", objError);
                    }

                    //PARA ACTUALIZAR COLORES DE GRVENCABEZADOPEDPRO (VERDE)
                    if (lblError.Text.Trim() == "") //control de errores
                    {
                        objError = objPedpro.f_actualizarColoresGrv(Session["gs_CodEmp"].ToString(), Session["gs_Numtra"].ToString(),
                            "1", "1", Session["gs_CodUs1"].ToString());
                        if (!String.IsNullOrEmpty(objError.Mensaje))
                            lblError.Text = objError.f_ErrorNuevo("*07.", objError);
                    }

                    //redireccion despues de guardar
                    if (lblError.Text.Trim() == "") //control de errores
                    {
                        Response.Redirect("w_Inicio.aspx");
                        //Response.Write("<script>alert('Todos los totales deben estar llenos');</script>"); //OJO NO VALE PONER RESPONSE .REDIRECT Y RESPONSE.WRITE JUNTOS....EL ALTERT NO SE ACTIVA
                    }
                }
            }
            else
            {
                lblError.Text = "*02. "+ DateTime.Now + " " + "Debe despachar todos los artículos antes de Guardar Completo.";
            }
        }

        protected void btnGuardarParcial_Click(object sender, EventArgs e)
        {
            lblAlertaModal.Text = "";
            int rowIndex = 0;
            DataTable dtObservacionArticulos = (DataTable)Session["gdt_Articulos"];

            //PARA MOSTRAR ROW DE LOS ARTICULOS QUE NO SE VEN A INSERTAR (SU CANTID NO ES CERO)
            //for (int i = 0; i < dtObservacionArticulos.Rows.Count; i++)
            //{

            //    if (cantidadDes == 0) //si sobran articulos los muestra (si su cantidadDes es 0)
            //    {
            //        dtObservacionArticulos.Rows.RemoveAt(i);
            //    }
            //}

            grvObservacionArticulos.Columns[2].Visible = true;
            grvObservacionArticulos.DataSource = dtObservacionArticulos;
            grvObservacionArticulos.DataBind();

            foreach (GridViewRow row in grvObservacionArticulos.Rows)
            {
                //PARA MOSTRAR EL CODIGO ALTERNO EN LUGAR DE CODART....SOLO SI NO ES NULL
                if (row.Cells[2].Text != "&nbsp;") //para saber si desart es NULL
                {
                    string s_aux = row.Cells[1].Text; //PARA PASAR desart A Cells[1] y codart  A Cells[5]
                    row.Cells[1].Text = row.Cells[2].Text; //Muestra desart en Código
                    row.Cells[2].Text = s_aux;
                }

                //PARA MOSTRAR ROW DE LOS ARTICULOS QUE NO SE VEN A INSERTAR (SU CANTID NO ES CERO)
                decimal cantidadDes = dtObservacionArticulos.Rows[rowIndex].Field<decimal>("cantid"); //cantidad de total de articulos menos los q se despacho (canfac)
                //decimal saldo = dt.Rows[rowIndex].Field<decimal>("saldo"); //cantidad total de articulo con el que se empezo
                //decimal cantid1 = saldo - cantidadDes; //articulos despachados

                if (cantidadDes != 0) //si sobran articulos los muestra (si su cantidadDes no es 0)
                {
                    grvObservacionArticulos.Rows[rowIndex].Visible = true;
                }
                else
                {
                    grvObservacionArticulos.Rows[rowIndex].Visible = false;
                }
                rowIndex++;
            }

            grvObservacionArticulos.Columns[2].Visible = false; //esconde la columna de desart
            ClientScript.RegisterStartupScript(this.GetType(), "Popup", "$('#modalObservacion').modal('show')", true);
            //de aqui va al btnAceptar
        }

        public void f_insertarArticulos(string s_tipoGuargar, string est002) //metodo para insertar enc y ren egresos
        {
            lblError.Text = "";
            string numfac = "";

            //numfac = f_CalcularSecuencia().Trim();
            objSecuencia = objSecuencia.f_CalcularSecuencia(Session["gs_CodEmp"].ToString(), Session["gs_SerSec"].ToString(), "VC_EGR");
            if (String.IsNullOrEmpty(objSecuencia.Error.Mensaje))
                numfac = objSecuencia.Numfac.Trim();
            else
                lblError.Text = objSecuencia.Error.f_ErrorNuevo("*16.", objSecuencia.Error);

            string codemp = Session["gs_CodEmp"].ToString();
            string codcli = Session["gs_Codcli"].ToString();
            string nomcli = Session["gs_Nomcli"].ToString();
            string codalm = Session["gs_CodAlm"].ToString();
            string codven = Session["gs_CodVen"].ToString();
            string codusu = Session["gs_CodUsu"].ToString();
            string sersec = Session["gs_SerSec"].ToString();
            string codsuc = Session["gs_CodSuc"].ToString();
            string tiptra = Session["gs_Tiptra"].ToString();
            string numtra = Session["gs_Numtra"].ToString();
            string usuing = Session["gs_CodUs1"].ToString();
            string observ = "";
            if (Session["gs_ObservEncPed"] != null)
                observ = Session["gs_ObservEncPed"].ToString();

            clsError objError = new clsError();
            if (Session["gs_ArtDespachado"].ToString().Equals("1")) //inserta si despacho por lo menos 1 articulo
            {
                objError = objEgreso.f_llenarEncabezadoEgresos(codemp, numfac, codcli, codalm,
                codven, codusu, nomcli, tiptra, numtra, sersec, codsuc, usuing, observ, est002); //se llama al SP
            }
            else
            {
                if (Session["gs_Est002"].ToString().Equals("3")) //inserta si haybia hecho desp parcial o grabar pendiente
                {
                    objError = objEgreso.f_llenarEncabezadoEgresos(codemp, numfac, codcli, codalm,
                    codven, codusu, nomcli, tiptra, numtra, sersec, codsuc, usuing, observ, est002); //se llama al SP
                }
                else
                {
                    lblError.Text = "*15. " + DateTime.Now + "Debe despachar por lo menos 1 artículo para poder guardar.";
                }
            }

            if (String.IsNullOrEmpty(objError.Mensaje)) //si no hubo error al insertar en el encabezado
            {
                DataTable dtArticulos = (DataTable)Session["gdt_Articulos"];
                int rowIndex = 0; //para enconctrar ddl en grvObservacion                
                long numren1 = 0;
                long numite1 = 0;
                foreach (DataRow row in dtArticulos.Rows)
                {                   
                    Session.Add("gs_Codart", row.Field<string>("codart"));
                    string codart1 = row.Field<string>("codart");
                    numren1 += 1;
                    numite1 += 1;
                    string nomart1 = row.Field<string>("nomart");
                    string coduni1 = row.Field<string>("coduni");
                    decimal preuni1 = row.Field<decimal>("preuni");
                    string observ1 = "";
                    string estdes1 = "";
                    if (!DBNull.Value.Equals(row["estdes"])) //para saber si estdes es NULL
                    {
                        estdes1 = row.Field<string>("estdes");
                    }

                    decimal cantidadDes = row.Field<decimal>("cantid"); //cantidad de total de articulos menos los q se despacho (canfac)
                    decimal saldo = row.Field<decimal>("saldo"); //cantidad total de articulo con el que se empezo
                    decimal cantid1 = saldo - cantidadDes; //articulos despachados

                    decimal candes1 = 0;
                    if (!DBNull.Value.Equals(row["candes"]))
                    {
                        candes1 = row.Field<decimal>("candes");//cantidad guardada en campo candes al hacer grabar pendiente
                    }

                    //***para obtener observ de grvObservacion al hacer clic en btnAceptar
                    if (s_tipoGuargar.Equals("btnAceptar")) //si se hizo clic en btnAceptar
                    {
                        DropDownList ddlgrvObserv = (DropDownList)grvObservacionArticulos.Rows[rowIndex].Cells[3].FindControl("ddlgrvObservacion");
                        if (cantidadDes != 0) //si sobran articulos los muestra (si su cantidadDes no es 0)
                        {
                            observ1 = ddlgrvObserv.SelectedValue.ToString().Trim();
                        }                                               
                    }

                    if (cantid1 != 0) //si no toco al articulo, no lo inserta en la tabla (si su cantid1 es 0)
                    {
                        if (candes1 != 0) //si hace despacho parcial en un articulo que estaba con grabar pendiente
                        {
                            cantid1 = cantid1 + candes1;
                        }
                        if (lblError.Text.Trim() == "") //control de errores
                        {
                            objError = objEgreso.f_llenarRenglonesEgresos(codart1, codemp, numfac, numren1, numite1, nomart1, coduni1,
                                preuni1, cantid1, cantidadDes, observ1, Session["gs_CodAlm"].ToString(), Session["gs_Numtra"].ToString());
                            if (!String.IsNullOrEmpty(objError.Mensaje))
                                lblError.Text = objError.f_ErrorNuevo("*04.", objError);
                            
                        }
                        decimal totven = preuni1 * cantid1;
                        totven = Math.Round(totven, 2); //para 2 decimales
                        if (lblError.Text.Trim() == "") //control de errores
                        {
                            objError = objEgreso.f_llenarKardex(codemp, numfac, codart1, numren1, coduni1, codalm, cantid1, codcli, codven,
                                codusu, nomcli, tiptra, numtra, sersec, codsuc, totven, preuni1); //OJO totven ya no se usa para INSERT
                            if (!String.IsNullOrEmpty(objError.Mensaje))
                                lblError.Text = objError.f_ErrorNuevo("*05.", objError);
                        }
                    }
                    else
                    {                       
                        if (estdes1.Equals("1") && candes1 != 0) // si fue grabado pendiente
                        {
                            if (lblError.Text.Trim() == "") //control de errores
                            {
                                objError = objEgreso.f_llenarRenglonesEgresos(codart1, codemp, numfac, numren1, numite1, nomart1, coduni1,
                                    preuni1, candes1, cantidadDes, observ1, Session["gs_CodAlm"].ToString(), Session["gs_Numtra"].ToString());
                                if (!String.IsNullOrEmpty(objError.Mensaje))
                                    lblError.Text = objError.f_ErrorNuevo("*04.", objError);
                            }
                            decimal totven = preuni1 * cantid1;
                            totven = Math.Round(totven, 2); //para 2 decimales
                            if (lblError.Text.Trim() == "" && !(codart1[0].ToString().Equals("/"))) //control de errores... si es servicio de transporte (/V400) no lo inserta
                            {
                                objError = objEgreso.f_llenarKardex(codemp, numfac, codart1, numren1, coduni1, codalm, candes1, codcli, codven,
                                    codusu, nomcli, tiptra, numtra, sersec, codsuc, totven, preuni1); //OJO totven ya no se usa para INSERT
                                if (!String.IsNullOrEmpty(objError.Mensaje))
                                    lblError.Text = objError.f_ErrorNuevo("*05.", objError);
                            }
                        }
                    }
                    rowIndex++;
                    
                    if (lblError.Text.Trim() == "" && !(observ1.Equals(""))) //control de errores
                    {
                        objError = objPedpro.f_actualizarObsev(codart1,codemp,numtra,observ1); //para actualizar las observ
                        if (!String.IsNullOrEmpty(objError.Mensaje))
                            lblError.Text = objError.f_ErrorNuevo("*14.", objError);
                    }
                }
            }
            else
            {
                lblError.Text = objError.f_ErrorNuevo("*03.", objError);
            }
        }     
        
        protected void btnAceptar_Click(object sender, EventArgs e) //para guardarParcial
        {
            if (f_CanTot() == 0) //verifica si todos los cantid estan en 0....REALIZO PARCIAL PERO TODOS ESTABAN EN CERO....SE PONE EN VERDE
            {
                if (Session["gs_ContVal"].ToString().Equals("0")) //para que al aplastar muchas veces solo se haga 1
                {
                    Session["gs_ContVal"] = "1";
                    f_insertarArticulos("btnAceptar", "1"); //est002=1 ...para q wladi sepa si fue parcial o completo

                    clsError objError = new clsError();
                    if (lblError.Text.Trim() == "") //control de errores
                    {
                        objError = objPedpro.f_actualizarEncabezadopedpro(Session["gs_CodEmp"].ToString(),
                            Session["gs_Numtra"].ToString(), txtNumegr.Text);
                        if (!String.IsNullOrEmpty(objError.Mensaje))
                            lblError.Text = objError.f_ErrorNuevo("*06.", objError);
                    }

                    //PARA ACTUALIZAR COLORES DE GRVENCABEZADOPEDPRO (VERDE)
                    if (lblError.Text.Trim() == "") //control de errores
                    {
                        objError = objPedpro.f_actualizarColoresGrv(Session["gs_CodEmp"].ToString(), Session["gs_Numtra"].ToString(),
                            "1", "1", Session["gs_CodUs1"].ToString());
                        if (!String.IsNullOrEmpty(objError.Mensaje))
                            lblError.Text = objError.f_ErrorNuevo("*07.", objError);
                    }

                    //redireccion despues de guardar
                    if (lblError.Text.Trim() == "") //control de errores
                    {
                        Response.Redirect("w_Inicio.aspx");
                    }
                }                
            }
            else //GRABADO PARCIAL NORMAL
            {
                if (Session["gs_ContVal"].ToString().Equals("0")) //para que al aplastar muchas veces solo se haga 1
                {
                    Session["gs_ContVal"] = "1";
                    f_insertarArticulos("btnAceptar", "2"); //est002=2 ...para q wladi sepa si fue parcial o completo

                    clsError objError = new clsError();
                    if (lblError.Text.Trim() == "") //control de errores
                    {
                        objError = objPedpro.f_actualizarEncabezadopedpro(Session["gs_CodEmp"].ToString(),
                            Session["gs_Numtra"].ToString(), txtNumegr.Text);
                        if (!String.IsNullOrEmpty(objError.Mensaje))
                            lblError.Text = objError.f_ErrorNuevo("*06.", objError);

                    }

                    //PARA ACTUALIZAR COLORES DE GRVENCABEZADOPEDPRO (LILA)
                    if (lblError.Text.Trim() == "") //control de errores
                    {
                        objError = objPedpro.f_actualizarColoresGrv(Session["gs_CodEmp"].ToString(), Session["gs_Numtra"].ToString(),
                            "1", "2", Session["gs_CodUs1"].ToString());
                        if (!String.IsNullOrEmpty(objError.Mensaje))
                            lblError.Text = objError.f_ErrorNuevo("*07.", objError);
                    }

                    //redireccion despues de guardar
                    if (lblError.Text.Trim() == "") //control de errores
                    {
                        Response.Redirect("w_Inicio.aspx");
                    }
                }
            }
        }

        protected void btnPendiente_Click(object sender, EventArgs e) //para que despachen cierta cantidad y se guarde al volver a ingresar (NO INSERTA EN TBLS...solo hace update)
        {
            string codemp = Session["gs_CodEmp"].ToString();
            string numtra = Session["gs_Numtra"].ToString();

            //PARA ACTUALIZAR COLORES DE GRVENCABEZADOPEDPRO (AZUL)
            clsError objError = new clsError();
            if (lblError.Text.Trim() == "") //control de errores
            {
                objError = objPedpro.f_actualizarColoresGrv(codemp, numtra,"1", "3", Session["gs_CodUs1"].ToString());
                if (!String.IsNullOrEmpty(objError.Mensaje))
                    lblError.Text = objError.f_ErrorNuevo("*07.", objError);
            }

            DataTable dtArticulos = (DataTable)Session["gdt_Articulos"];
            foreach (DataRow row in dtArticulos.Rows)
            {
                Session.Add("gs_Codart", row.Field<string>("codart"));
                string codart1 = row.Field<string>("codart");
                //string nomart1 = row.Field<string>("nomart");
                //string coduni1 = row.Field<string>("coduni");
                //decimal preuni1 = row.Field<decimal>("preuni");  

                decimal cantidadDes = row.Field<decimal>("cantid"); //cantidad de total de articulos menos los q se despacho (canfac)
                decimal saldo = row.Field<decimal>("saldo"); //cantidad total de articulo con el que se empezo
                decimal cantid1 = saldo - cantidadDes; //articulos despachados

                //NUEVO CODIGO 20200217 PARA QUE AL GRABAR PENDIENTE 2 VECES EL MISMO ARTICULO SUME LAS CANDES
                decimal candes1 = 0;
                if (!DBNull.Value.Equals(row["candes"]))
                {
                    candes1 = row.Field<decimal>("candes");//cantidad guardada en campo candes al hacer grabar pendiente
                }

                if (cantid1 != 0) //si no toco al articulo, no le modifica su estdes ni su canfac
                {
                    if (candes1 != 0) //si hace grabar pendiente en un articulo que estaba con grabar pendiente
                    {
                        cantid1 = cantid1 + candes1;
                    }

                    if (lblError.Text.Trim() == "") //control de errores
                    {
                        objError = objPedpro.f_actualizarRenglonespedpro(codart1, codemp, numtra, cantidadDes, cantid1);
                        if (!String.IsNullOrEmpty(objError.Mensaje))
                            lblError.Text = objError.f_ErrorNuevo("*13.", objError);
                    }
                }
            }

            if (lblError.Text.Trim() == "") //control de errores
            {
                Response.Redirect("w_Inicio.aspx"); //ojo manejar error
            }
        }
    }
}


